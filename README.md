# Farma-Alg 3.0 (frontend)

Farma-Alg é um sistema para fins educacionais de lógica de programação e algorítmos. Seu propósito inicial é dar aos estudantes a possibilidade de executar códigos online com o feedback do resultado da execução de seu programa e, ao mesmo tempo, proporcionar ao professor um ferramental analítico para acompanhamento do aprendizado de suas turmas. A primeira versão deste software está em uso por alguns docentes do curso de Tecnologia em Análise e Desenvolvimento de Sistemas da Universidade Federal do Paraná (UFPR).

O propósito deste projeto é desenvolver uma evolução do Farma-Alg original, mantendo aspectos positivos e mitigando aspectos negativos da versão já em uso, levando em conta práticas atuais de mercado para o desenvolvimento de softwares escaláveis e de fácil manutenção. E, neste momento, o projeto está sendo desenvolvido como Trabalho de Conclusão de Curso e com o escopo um pouco reduzido em comparação ao projeto inicial.

Ver protótipo em construção [nesta página](https://farmaalg.vercel.app/).

Este repositório se concentra no módulo frontend, com as interfaces que os usuários vão interagir. É necessário ter o [Node.js 12+](https://nodejs.org/) para executar o código localmente e gerar os arquivos estáticos para produção.

Com isso, os seguintes comandos estão disponíveis em linha de comando:

```bash
$ npm run dev     # deixa a aplicação disponível em http://localhost:8080
$ npm run build   # gera os arquivos estáticos para implantação
$ npm run prod    # cria um servidor local para visualizar os arquivos estáticos gerados
```
