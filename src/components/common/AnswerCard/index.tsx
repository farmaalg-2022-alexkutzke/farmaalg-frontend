import Chip from '@mui/material/Chip'
import Collapse from '@mui/material/Collapse'
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import Typography from '@mui/material/Typography'
import { useState } from 'react'

import MarkdownRenderer from '@/common/MarkdownRenderer'
import { Resposta } from '~/models'

import { useStyles } from './styles'

namespace AnswerCard {
  export interface Props {
    answer: Resposta
  }
}

function AnswerCard({ answer }: AnswerCard.Props) {
  const classes = useStyles()
  const [open, setOpen] = useState(false)

  return (
    <Paper className={classes.root} onClick={() => setOpen(!open)}>
      <Typography variant="h5">
        {answer.ehFinal ? 'Resposta Final' : `Tentativa #${answer.numeroTentativa}`}
      </Typography>
      <MarkdownRenderer>{`\`\`\`${answer.codigo}\`\`\``}</MarkdownRenderer>

      <Collapse in={open}>
        {answer.casosTeste.map((test) => (
          <Grid
            key={test.id}
            container
            columnSpacing={2}
            className={classes.testCase}
          >
            <Grid item xs={12} className={classes.testCaseHeader}>
              <Typography variant="h6">{test.titulo}</Typography>
              {test.estahCorreto ? (
                <Chip color="success" label="Passou" size="small" />
              ) : (
                <Chip color="error" label="Falhou" size="small" />
              )}
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body2">Saída esperada</Typography>
              <MarkdownRenderer>
                {`\`\`\`\n${test.output}\n\`\`\`\n`}
              </MarkdownRenderer>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body2">Saída obtida</Typography>
              <MarkdownRenderer>
                {`\`\`\`\n${test.outputObtido}\n\`\`\`\n`}
              </MarkdownRenderer>
            </Grid>
          </Grid>
        ))}
      </Collapse>
    </Paper>
  )
}

export default AnswerCard
