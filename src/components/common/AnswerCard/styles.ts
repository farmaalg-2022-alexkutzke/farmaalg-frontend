import { alpha } from '@mui/material/styles'
import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(4),
    backgroundColor: alpha(theme.palette.primary.light, 0.2),
    cursor: 'pointer',

    '& h5': {
      padding: theme.spacing(2, 3, 0, 3),
    },
  },

  testCase: {
    borderTops: `1px solid ${theme.palette.divider}`,
    padding: theme.spacing(1, 3, 0),

    '& .markdown-body': {
      padding: theme.spacing(0, 0, 2),

      '& pre': {
        margin: 0,
      },
    },
  },

  testCaseHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    gap: theme.spacing(2),

    marginBottom: theme.spacing(2),

    '& h6': {
      fontWeight: 700,
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },

    '& button': {
      opacity: 0.6,
      backgroundColor: 'transparent',
      transition: 'color 200ms, opacity 200ms',

      '&:hover': {
        opacity: 1,
        backgroundColor: 'transparent',
        color: theme.palette.secondary.main,
      },
    },
  },
}))
