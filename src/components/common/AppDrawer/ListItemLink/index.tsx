import Avatar from '@mui/material/Avatar'
import ListItem from '@mui/material/ListItem'
import ListItemAvatar from '@mui/material/ListItemAvatar'
import ListItemText from '@mui/material/ListItemText'
import SvgIcon from '@mui/material/SvgIcon'
import MaterialTooltip from '@mui/material/Tooltip'
import clsx from 'clsx'
import { FC, Fragment, ReactElement } from 'react'
import { Link, useMatch, useResolvedPath } from 'react-router-dom'

import { useDrawer } from '~/helpers/hooks'
import { useStyles } from './styles'

namespace ListItemLink {
  export interface Props {
    icon: typeof SvgIcon
    label: string
    to?: string
    onClick?: () => void
  }
}

function ListItemLink({ icon: Icon, label, to, onClick }: ListItemLink.Props) {
  const classes = useStyles()
  const isDrawerOpen = useDrawer().isOpen
  const resolvedPath = useResolvedPath(to ?? '')
  const routeMatch = useMatch({ path: resolvedPath.pathname })

  const mergeClass = (className: string) => {
    return clsx(className, {
      [classes.active]: Boolean(routeMatch),
      [classes.expanded]: isDrawerOpen,
    })
  }

  const ToolTip: FC = isDrawerOpen
    ? Fragment
    : ({ children }) => (
      <MaterialTooltip
        arrow
        title={label}
        placement="right"
        classes={{
          arrow: classes.tooltipWrapper,
          tooltip: classes.tooltipText,
        }}
      >
        {children as ReactElement}
      </MaterialTooltip>
    )

  const routerLinkProps = to
    ? { component: Link, to }
    : undefined

  return (
    <ToolTip>
      <ListItem
        className={mergeClass(classes.link)}
        onClick={onClick}
        {...routerLinkProps}
      >
        <ListItemAvatar>
          <Avatar className={mergeClass(classes.avatar)}>
            <Icon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText className={mergeClass(classes.label)}>
          {label}
        </ListItemText>
      </ListItem>
    </ToolTip>
  )
}

export default ListItemLink
