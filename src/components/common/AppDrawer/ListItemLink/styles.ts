import { makeStyles } from '@mui/styles'

export const DRAWER_WIDTH = 240

export const useStyles = makeStyles((theme) => ({
  link: {
    maxWidth: `calc(100% - ${theme.spacing(2)})`,
    margin: theme.spacing(0, 1),
    padding: theme.spacing(1),

    '& *': {
      ...theme.typography.button,
      fontWeight: 500,
      letterSpacing: 1.2,
    },

    '&$active': {
      borderRadius: '37%',
      backgroundColor: theme.palette.primary.main,

      '&$expanded': {
        borderRadius: theme.shape.borderRadius,
      },
    },
  },

  avatar: {
    borderRadius: '37%',
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.secondary.main,

    '&$active': {
      backgroundColor: 'transparent',
      color: 'white',

      '&$expanded': {
        backgroundColor: theme.palette.primary.light,
      },
    },

    '& svg': {
      fontSize: '1.4rem',
    },
  },

  label: {
    color: 'white',

    '&:active': {
      color: theme.palette.secondary.main,
    },

    '&$active': {
      color: 'white',
    },
  },

  tooltipWrapper: {
    transform: 'translateX(2px)',
    color: theme.palette.grey[800],
  },

  tooltipText: {
    backgroundColor: theme.palette.grey[800],
    padding: theme.spacing(1),

    ...theme.typography.button,
    fontWeight: 500,
    letterSpacing: 1.2,
  },

  active: {},
  expanded: {},
}))
