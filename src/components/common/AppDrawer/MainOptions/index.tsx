import AssignmentIcon from '@mui/icons-material/Assignment'
import CodeIcon from '@mui/icons-material/Code'
import HomeIcon from '@mui/icons-material/Home'
import LocationCityIcon from '@mui/icons-material/LocationCity'
import PersonIcon from '@mui/icons-material/Person'
import SchoolIcon from '@mui/icons-material/School'
import List from '@mui/material/List'

import { useAuth } from '~/helpers/hooks'

import ListItemLink from '../ListItemLink'
import { useStyles } from './styles'

function MainOptions() {
  const classes = useStyles()
  const { user } = useAuth()

  return (
    <List component="nav" className={classes.wrapper}>
      <ListItemLink
        to="/"
        icon={HomeIcon}
        label="Início"
      />
      {user?.isAdmin() && (
        <ListItemLink
          to="/instituicoes"
          icon={LocationCityIcon}
          label="Instituições"
        />
      )}
      {user?.isAdmin() && (
        <ListItemLink
          to="/usuarios"
          icon={PersonIcon}
          label="Usuários"
        />
      )}
      <ListItemLink
        to="/turmas"
        icon={SchoolIcon}
        label="Turmas"
      />
      {(user?.isAdmin() || user?.isTeacher()) && (
        <ListItemLink
          to="/exercicios"
          icon={AssignmentIcon}
          label="Exercícios"
        />
      )}
      {user?.isAdmin() && (
        <ListItemLink
          to="/linguagens"
          icon={CodeIcon}
          label="Linguagens"
        />
      )}
    </List>
  )
}

export default MainOptions
