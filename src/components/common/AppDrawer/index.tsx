import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import Box from '@mui/material/Box'
import Drawer from '@mui/material/Drawer'
import IconButton from '@mui/material/IconButton'
import clsx from 'clsx'

import { useDrawer } from '~/helpers/hooks'

import MainOptions from './MainOptions'
import { DRAWER_WIDTH, useStyles } from './styles'

function AppDrawer() {
  const drawer = useDrawer()
  const classes = useStyles()

  return (
    <Drawer
      variant="permanent"
      open={drawer.isOpen}
      classes={{
        root: clsx(classes.drawerRoot, {
          [classes.drawerOpen]: drawer.isOpen,
          [classes.drawerClosed]: !drawer.isOpen,
        }),
        paper: clsx(classes.drawerPaper, {
          [classes.drawerOpen]: drawer.isOpen,
          [classes.drawerClosed]: !drawer.isOpen,
        }),
      }}
    >
      <Box className={classes.toolbar}>
        <IconButton onClick={drawer.close} className={classes.toolbarBtn} size="large">
          <ChevronLeftIcon />
        </IconButton>
      </Box>
      <MainOptions />
    </Drawer>
  )
}

AppDrawer.WIDTH = DRAWER_WIDTH

export default AppDrawer
