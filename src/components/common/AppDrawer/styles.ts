import { makeStyles } from '@mui/styles'

export const DRAWER_WIDTH = 240

export const useStyles = makeStyles((theme) => ({
  drawerRoot: {
    flexShrink: 0,
    width: DRAWER_WIDTH,
    whiteSpace: 'nowrap',

    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },

  drawerOpen: {
    width: DRAWER_WIDTH,

    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },

  drawerClosed: {
    overflowX: 'hidden',
    width: `calc(${theme.spacing(7)} + 1px)`,

    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),

    [theme.breakpoints.up('sm')]: {
      width: `calc(${theme.spacing(9)} + 1px)`,
    },
  },

  drawerPaper: {
    backgroundColor: theme.palette.grey[900],
    paddingBottom: theme.spacing(2),
    color: theme.palette.grey[100],
  },

  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),

    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },

  toolbarBtn: {
    color: theme.palette.grey[100],
  },
}))
