import Typography from '@mui/material/Typography'

import { useStyles } from './styles'

const CURRENT_YEAR = new Date().getFullYear()

function AppFooter() {
  const classes = useStyles()

  return (
    <footer className={classes.appFooter}>
      <Typography variant="body1">
        Universidade Federal do Paraná
      </Typography>
      <Typography variant="body2" color="textSecondary">
        Farma-Alg &copy; {CURRENT_YEAR} - Todos os direitos reservados
      </Typography>
    </footer>
  )
}

export default AppFooter
