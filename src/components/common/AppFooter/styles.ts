import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  appFooter: {
    marginTop: 'auto',
    backgroundColor: 'white',
    padding: theme.spacing(3, 2),
    textAlign: 'center',
  },
}))
