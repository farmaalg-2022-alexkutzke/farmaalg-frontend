import SettingsIcon from '@mui/icons-material/ManageAccounts'
import MenuIcon from '@mui/icons-material/Menu'
import ExitIcon from '@mui/icons-material/PowerSettingsNew'
import BackIcon from '@mui/icons-material/West'
import AppBar from '@mui/material/AppBar'
import Avatar from '@mui/material/Avatar'
import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import IconButton from '@mui/material/IconButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import Toolbar from '@mui/material/Toolbar'
import Tooltip from '@mui/material/Tooltip'
import clsx from 'clsx'
import { useState } from 'react'
import { Link, useLocation, useNavigate } from 'react-router-dom'

import Button from '@/common/Button'
import FarmaAlgLogo from '@/common/FarmaAlgLogo'
import UserSettings from '@/common/UserSettings'
import { useAuth, useDrawer } from '~/helpers/hooks'

import { useStyles } from './styles'

namespace AppHeader {
  export type Props = {
    backButton?: boolean
    contained?: boolean
    drawer?: boolean
  }
}

function AppHeader({
  backButton = false,
  contained = false,
  drawer: withDrawer = false,
}: AppHeader.Props) {
  const classes = useStyles()
  const drawer = useDrawer()
  const navigate = useNavigate()
  const { pathname } = useLocation()
  const { signOut, user } = useAuth()
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const [userSettingsVisible, setUserSettingsVisible] = useState(false)

  function handleOpenUserSettings() {
    setUserSettingsVisible(true)
    setAnchorEl(null)
  }

  return (
    <AppBar
      position="absolute"
      color="default"
      className={clsx(classes.appHeader, {
        [classes.appHeaderShift]: withDrawer && drawer.isOpen,
      })}
    >
      <Container maxWidth={contained && 'lg'}>
        <Toolbar className={classes.toolbar}>
          <Box>
            {withDrawer && !drawer.isOpen && (
              <IconButton
                edge="start"
                color="inherit"
                aria-label="abrir manu"
                onClick={drawer.open}
                className={clsx(classes.menuButton, {
                  [classes.menuButtonHidden]: withDrawer && drawer.isOpen,
                })}
                size="large"
              >
                <MenuIcon />
              </IconButton>
            )}
            {backButton && (
              <Tooltip title="Voltar">
                <IconButton className={classes.backButton} onClick={() => navigate(-1)}>
                  <BackIcon color="inherit" />
                </IconButton>
              </Tooltip>
            )}
            <Link to="/" className={classes.toolbarTitle}>
              <FarmaAlgLogo />
            </Link>
          </Box>

          <Box component="nav" className={classes.navButtons}>
            {user ? (
              <>
                <Button
                  className={classes.userMenuButton}
                  id="user-settings-button"
                  startIcon={<Avatar src={user.avatar} alt="avatar" />}
                  variant="outlined"
                  onClick={(event) => setAnchorEl(event.currentTarget)}
                  aria-controls="user-settings-menu"
                  aria-expanded={anchorEl ? 'true' : undefined}
                  aria-haspopup="true"
                  aria-label="opções"
                >
                  {user.nome}
                </Button>
                <Menu
                  anchorEl={anchorEl}
                  id="user-settings-menu"
                  MenuListProps={{ 'aria-labelledby': 'user-settings-button' }}
                  open={Boolean(anchorEl)}
                  anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
                  transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                  onClose={() => setAnchorEl(null)}
                >
                  <MenuItem onClick={handleOpenUserSettings}>
                    <ListItemIcon><SettingsIcon /></ListItemIcon>
                    <ListItemText>Configurações</ListItemText>
                  </MenuItem>
                  <MenuItem onClick={() => signOut()}>
                    <ListItemIcon><ExitIcon /></ListItemIcon>
                    <ListItemText>Sair</ListItemText>
                  </MenuItem>
                </Menu>
                <UserSettings
                  open={userSettingsVisible}
                  onClose={() => setUserSettingsVisible(false)}
                />
              </>
            ) : (
              <>
                {pathname !== '/entrar' && (
                  <Button
                    size="large"
                    variant="text"
                    className={classes.toolbarLink}
                    onClick={() => navigate('/entrar')}
                  >
                    Entrar
                  </Button>
                )}
                {pathname !== '/registrar' && (
                  <Button
                    size="large"
                    className={classes.toolbarLink}
                    onClick={() => navigate('/registrar')}
                  >
                    Registrar
                  </Button>
                )}
              </>
            )}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  )
}

export default AppHeader
