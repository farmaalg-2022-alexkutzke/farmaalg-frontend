import { makeStyles } from '@mui/styles'

import Drawer from '@/common/AppDrawer'

export const useStyles = makeStyles((theme) => ({
  appHeader: {
    zIndex: theme.zIndex.drawer + 1,
    borderBottom: `1px solid ${theme.palette.primary.main}`,

    transition: theme.transitions.create(['margin', 'width'], {
      duration: theme.transitions.duration.leavingScreen,
      easing: theme.transitions.easing.sharp,
    }),
  },

  appHeaderShift: {
    marginLeft: Drawer.WIDTH,
    width: `calc(100% - ${Drawer.WIDTH}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      duration: theme.transitions.duration.enteringScreen,
      easing: theme.transitions.easing.sharp,
    }),
  },

  toolbar: {
    justifyContent: 'space-between',
  },

  menuButton: {
    margin: theme.spacing(0, 4, 0, -4),
  },

  menuButtonHidden: {
    display: 'none',
  },

  toolbarTitle: {
    margin: theme.spacing(2, 0),
    textDecoration: 'none',
    cursor: 'pointer',
  },

  toolbarLink: {
    margin: theme.spacing(1, 0),
  },

  backButton: {
    margin: theme.spacing(0, 4, 1, 0),
    color: theme.palette.secondary.main,
  },

  userMenuButton: {
    borderColor: 'transparent',
    padding: theme.spacing(1),
    textTransform: 'none',

    '&:hover': {
      backgroundColor: theme.palette.primary.main,
    },

    '& .MuiAvatar-root': {
      height: theme.spacing(4),
      width: theme.spacing(4),
    },
  },

  navButtons: {
    display: 'flex',
    gap: 16,
  },
}))
