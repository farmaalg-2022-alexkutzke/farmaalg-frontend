import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import InputLabel from '@mui/material/InputLabel'

import FormField from '@/common/FormField'

import { useStyles } from './styles'

function AppSettingsForm() {
  const classes = useStyles()

  return (
    <Box className={classes.root}>
      <Grid container rowSpacing={2}>
        <Grid
          item
          xs={12}
          md={4}
          className={classes.labelContainer}
        >
          <InputLabel htmlFor="lang-setting">Idioma</InputLabel>
        </Grid>
        <Grid item xs={12} md={8}>
          <FormField
            hiddenLabel
            id="lang-setting"
            options={[{ value: 'pt-BR', label: 'Português' }]}
            value="pt-BR"
          />
        </Grid>

        <Grid
          item
          xs={12}
          md={4}
          className={classes.labelContainer}
        >
          <InputLabel htmlFor="lang-setting">Tema</InputLabel>
        </Grid>
        <Grid item xs={12} md={8}>
          <FormField
            hiddenLabel
            id="lang-setting"
            options={[{ value: 'light', label: 'Claro' }]}
            value="light"
          />
        </Grid>
      </Grid>
    </Box>
  )
}

export default AppSettingsForm
