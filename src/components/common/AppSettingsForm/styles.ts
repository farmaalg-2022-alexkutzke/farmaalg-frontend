import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {},

  labelContainer: {
    alignSelf: 'center',
    display: 'flex',
    alignItems: 'center',

    '& label': {
      paddingTop: theme.spacing(2),

      color: theme.palette.primary.main,
      fontWeight: 700,

      [theme.breakpoints.up('md')]: {
        '&::after': {
          content: '":"',
        },
      },
    },
  },
}))
