import ShuffleIcon from '@mui/icons-material/Shuffle'
import Avatar, { AvatarProps as MuiAvatarProps } from '@mui/material/Avatar'
import Box from '@mui/material/Box'
import IconButton, { IconButtonProps } from '@mui/material/IconButton'
import Tooltip from '@mui/material/Tooltip'
import clsx from 'clsx'
import { ChangeEvent, useEffect, useState } from 'react'

import FormField from '@/common/FormField'
import { generateAvatar } from '~/helpers'

import { useStyles } from './/styles'

namespace AvatarField {
  export interface Props<T = any> extends FormField.Props<T> {
    alt?: string
    AvatarProps?: MuiAvatarProps
    ButtonProps?: IconButtonProps
    FormFieldProps?: FormField.Props<T>
    generatorButton?: boolean
    value?: string
  }
}

function AvatarField({
  alt,
  AvatarProps,
  ButtonProps,
  className,
  controller,
  FormFieldProps,
  generatorButton,
  value: initialValue,
  onChange,
  ...props
}: AvatarField.Props) {
  const classes = useStyles()
  const [value, setValue] = useState(initialValue)

  function handleChange(event: ChangeEvent<HTMLInputElement>) {
    setValue(event.target.value)
    onChange?.(event)
  }

  function handleClick() {
    const randomAvatar = generateAvatar()
    controller?.setValue(randomAvatar)
    setValue(randomAvatar)
  }

  useEffect(() => {
    controller && setValue(controller.defaultValue ?? controller.getValue())
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Box className={clsx(classes.root, className)}>
      <Avatar
        className={classes.image}
        src={value}
        {...AvatarProps}
      />
      <FormField
        className={classes.input}
        controller={controller}
        type="text"
        value={value}
        onChange={handleChange}
        endAdornment={generatorButton && (
          <Tooltip title="Gerar Aleatório" placement="top">
            <IconButton
              className={classes.button}
              onClick={handleClick}
              {...ButtonProps}
            >
              <ShuffleIcon />
            </IconButton>
          </Tooltip>
        )}
        {...props}
        {...FormFieldProps}
      />
    </Box>
  )
}

export default AvatarField
