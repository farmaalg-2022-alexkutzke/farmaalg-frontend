import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    gap: theme.spacing(1),

    width: '100%',
  },

  image: {
    width: 48,
    height: 48,
    margin: theme.spacing(1, 0, 1),
  },

  input: {
    flexGrow: 1,
  },

  button: {
    color: theme.palette.primary.main,
  },
}))
