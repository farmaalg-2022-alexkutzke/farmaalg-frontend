/* eslint-disable react/jsx-max-props-per-line */
import Link from '@mui/material/Link'
import SvgIcon from '@mui/material/SvgIcon'
import Typography from '@mui/material/Typography'
import { Fragment, FunctionComponent } from 'react'
import { Link as RouterLink } from 'react-router-dom'

import { useStyles } from './styles'

namespace Crumb {
  export interface Props {
    label: string
    icon?: typeof Fragment | typeof SvgIcon
    to?: string
  }
}

function Crumb({
  label,
  icon: Icon = Fragment,
  to,
}: Crumb.Props) {
  const classes = useStyles()

  const Wrapper: FunctionComponent = to
    ? (props) => <Link className={classes.root} component={RouterLink} to={to} {...props} />
    : (props) => <Typography className={classes.root} {...props} />

  return (
    <Wrapper>
      <Icon /> {label}
    </Wrapper>
  )
}

export default Crumb
