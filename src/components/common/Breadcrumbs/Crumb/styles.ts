import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    color: 'inherit',
    textDecoration: 'none',

    '&:not(:not(a)):hover': {
      color: theme.palette.secondary.main,
      fontWeight: 500,
      textDecoration: 'underline',
    },

    '&:not(a)': {
      fontWeight: 700,
    },

    '& svg': {
      marginRight: theme.spacing(1),
    },
  },
}))
