import MuiBreadcrumbs, { BreadcrumbsProps } from '@mui/material/Breadcrumbs'
import clsx from 'clsx'

import Crumb from './Crumb'
import { useStyles } from './styles'

namespace Breadcrumbs {
  export interface Props extends BreadcrumbsProps {
    crumbs: Crumb.Props[]
  }
}

function Breadcrumbs({
  className,
  crumbs,
  ...props
}: Breadcrumbs.Props) {
  const classes = useStyles()

  return (
    <MuiBreadcrumbs
      className={clsx(className, classes.root)}
      aria-label="breadcrumb"
      {...props}
    >
      {crumbs.map((crumb, index) => {
        const isCurrent = index === crumbs.length - 1
        const href = crumb.to ?? new Array(crumbs.length - index - 1).fill('..').join('/')

        return (
          <Crumb
            key={index}
            icon={crumb.icon}
            label={crumb.label}
            to={isCurrent ? undefined : href}
          />
        )
      })}
    </MuiBreadcrumbs>
  )
}

export default Breadcrumbs
