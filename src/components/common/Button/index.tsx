import MuiButton, { ButtonProps } from '@mui/material/Button'
import clsx from 'clsx'

import { useStyles } from './styles'

namespace Button {
  export interface Props extends ButtonProps {
    selected?: boolean
  }
}

function Button({
  className,
  disableElevation = true,
  selected,
  variant = 'contained',
  color = variant === 'outlined' ? 'secondary' : 'primary',
  ...props
}: Button.Props) {
  const classes = useStyles({ color })

  return (
    <MuiButton
      className={clsx(className, classes.root, className, {
        [classes.selectable]: selected !== undefined,
        [classes.contained]: selected || variant === 'contained',
        [classes.outlined]: variant === 'outlined',
        [classes.text]: variant === 'text',
      })}
      color={color}
      disableElevation={disableElevation}
      variant={variant}
      {...props}
    />
  )
}

export default Button
