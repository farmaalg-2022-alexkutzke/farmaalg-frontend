import { makeStyles } from '@mui/styles'

namespace useStyles {
  export interface Props {
    color:
    | 'inherit'
    | 'primary'
    | 'secondary'
    | 'success'
    | 'error'
    | 'info'
    | 'warning'
  }
}

export const useStyles = makeStyles((theme) => {
  const containedStyle = {
    borderColor: 'transparent',
    borderWidth: 2,
    backgroundColor({ color }: useStyles.Props) {
      return color === 'primary' || color === 'secondary'
        ? theme.palette[color].main
        : undefined
    },

    color: 'white',

    '& svg': {
      color: theme.palette.secondary.light,
    },
  }

  return {
    root: {
      fontSize: '0.875rem',
      fontWeight: 700,
    },

    contained: {
      ...containedStyle,

      '&:disabled': {
        backgroundColor: theme.palette.primary.light,
      },
    },

    outlined: {
      borderColor: theme.palette.secondary.light,
      borderWidth: 2,
      color: theme.palette.primary.main,

      '&:hover': {
        ...containedStyle,
      },

      '&:disabled': {
        borderColor: theme.palette.primary.light,
        borderWidth: 2,
        color: theme.palette.primary.light,
      },
    },

    text: {},

    selectable: {
      '&:hover, &$contained': {
        ...containedStyle,
      },
    },

    dropDown: {
      minWidth: 'auto',
      paddingRight: 0,
      paddingLeft: 0,

      '& .MuiButton-label': {
        width: 'auto',
      },
    },
  }
})
