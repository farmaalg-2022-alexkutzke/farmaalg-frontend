import MuiCheckbox, { CheckboxProps } from '@mui/material/Checkbox'
import FormControl from '@mui/material/FormControl'
import ControlLabel from '@mui/material/FormControlLabel'
import FormHelperText from '@mui/material/FormHelperText'
import clsx from 'clsx'
import { Control, Controller } from 'react-hook-form'

import { useStyles } from './styles'

namespace Checkbox {
  export interface Props<T = any> extends CheckboxProps {
    label: string
    controller?: {
      control: Control<T>
      name: string
      defaultValue?: any
    }
    error?: boolean
    helperText?: string
  }
}

function Checkbox({
  color = 'secondary',
  controller,
  disabled,
  error,
  helperText,
  label,
  ...props
}: Checkbox.Props) {
  const classes = useStyles()

  return (
    <FormControl
      disabled={disabled}
      error={error}
      className={clsx(classes.root, props.className)}
    >
      <ControlLabel
        label={label}
        control={controller ? (
          <Controller
            control={controller.control}
            defaultValue={controller.defaultValue}
            name={controller.name}
            render={({ field }) => (
              <MuiCheckbox
                color={color}
                checked={Boolean(field.value)}
                {...props}
                {...field}
              />
            )}
          />
        ) : (
          <MuiCheckbox color={color} {...props} />
        )}
      />
      <FormHelperText error={error}>
        {helperText}
      </FormHelperText>
    </FormControl>
  )
}

export default Checkbox
