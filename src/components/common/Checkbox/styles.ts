import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.primary.main,
    fontWeight: 500,
  },
}))
