import CloseIcon from '@mui/icons-material/Close'
import SaveIcon from '@mui/icons-material/Save'
import Box from '@mui/material/Box'
import { useEffect } from 'react'
import { useDebounce } from 'react-use'
import * as Yup from 'yup'

import Button from '@/common/Button'
import FormField from '@/common/FormField'
import {
  useAuth,
  useClassroomContext,
  useForm,
  useUserContext,
} from '~/helpers/hooks'
import { Turma, TurmaDTO, TurmaStatus } from '~/models'

import { useStyles } from './styles'

namespace ClassroomForm {
  export interface Props {
    classroom: Partial<Turma>
    onSave: (classroom: TurmaDTO) => void
    onCancel?: () => void
  }
}

const manageClassroomValSchema = Yup.object({
  disciplina: Yup.string().required().max(50),
  nome: Yup.string().required().max(50),
  descricao: Yup.string().optional().max(255),
  codigoAcesso: Yup.string().required().max(20).classroomAccessCode(),
  status: Yup.string().required().max(20).oneOf(Object.values(TurmaStatus)),
  professor: Yup.object({
    id: Yup.string().required(),
  }).required(),
})

function ClassroomForm({
  classroom,
  onCancel,
  onSave,
}: ClassroomForm.Props) {
  const classes = useStyles()
  const userContext = useUserContext()
  const { user: currentUser } = useAuth()
  const { isSaving } = useClassroomContext()
  const { Form, getController, setValue, trigger, watch } = useForm<TurmaDTO>({
    valSchema: manageClassroomValSchema,
  })
  const accessCode = watch('codigoAcesso')

  useEffect(() => {
    currentUser?.isAdmin() && userContext.fetch()
  }, [currentUser?.isAdmin()]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    setValue('disciplina', classroom.disciplina ?? '')
    setValue('nome', classroom.nome ?? '')
    setValue('descricao', classroom.descricao ?? '')
    setValue('codigoAcesso', classroom.codigoAcesso ?? '')
    setValue('status', classroom.status ?? TurmaStatus.ATIVA)
    setValue('professor.id', (
      currentUser?.isTeacher() ? currentUser?.id : classroom.professor?.id
    ) as any ?? '')
  }, [classroom]) // eslint-disable-line react-hooks/exhaustive-deps

  useDebounce(async () => {
    accessCode && await trigger('codigoAcesso')
  }, 500, [accessCode])

  return (
    <Form onSubmit={onSave}>
      {currentUser?.isAdmin() && (
        <FormField
          controller={getController('professor.id')}
          label="Professor responsável"
          options={userContext.teachers.map((teacher) => ({
            label: teacher.nome,
            value: teacher.id,
          }))}
          required
        />
      )}
      <FormField
        controller={getController('disciplina')}
        label="Nome da disciplina"
        required
      />
      <FormField
        controller={getController('nome')}
        label="Nome da turma"
        required
      />
      <FormField
        controller={getController('descricao')}
        label="Descrição adicional"
        multiline
        rows={3}
      />
      <FormField
        controller={getController('codigoAcesso')}
        label="Código de acesso à turma"
        helperText="Informe um valor único que será utilizado para se cadastrar na turma."
        required
      />
      <FormField
        controller={getController('status')}
        label="Situação da turma"
        options={Object.values(TurmaStatus)}
        required
      />

      <Box className={classes.formActions}>
        {onCancel && (
          <Button
            startIcon={<CloseIcon />}
            variant="text"
            onClick={() => onCancel()}
          >
            Cancelar
          </Button>
        )}

        <Button
          disabled={isSaving}
          startIcon={<SaveIcon />}
          type="submit"
        >
          Salvar
        </Button>
      </Box>
    </Form>
  )
}

export default ClassroomForm
