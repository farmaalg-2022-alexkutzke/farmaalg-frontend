import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  formActions: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    gap: theme.spacing(2),

    marginTop: theme.spacing(2),
    borderTopColor: theme.palette.divider,
    borderTopStyle: 'solid',
    borderTopWidth: 1,
    paddingTop: theme.spacing(2),
  },
}))
