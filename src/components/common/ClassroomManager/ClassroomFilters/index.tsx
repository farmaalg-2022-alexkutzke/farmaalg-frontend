import SearchIcon from '@mui/icons-material/Search'
import Box from '@mui/material/Box'
import { useEffect, useMemo, useState } from 'react'
import { useDebounce } from 'react-use'

import FormField from '@/common/FormField'
import Switch from '@/common/Switch'
import { Turma, TurmaStatus } from '~/models'

import { useStyles } from './styles'

namespace ClassroomFilters {
  export interface Props {
    classrooms: Turma[]
    debounce?: number | false
    defaultValue?: string
    onFilter: (classrooms: Turma[]) => void
  }
}

function ClassroomFilters({
  debounce = 500,
  defaultValue = '',
  classrooms,
  onFilter,
}: ClassroomFilters.Props) {
  const classes = useStyles()
  const [activeOnly, setActiveOnly] = useState(true)
  const [searchText, setSearchText] = useState(defaultValue)
  const actualDebounceTime = debounce && debounce > 0 ? debounce : 0
  const visibleClassrooms = useMemo(() => {
    return classrooms
      .filter(({ status }) => !activeOnly || status === TurmaStatus.ATIVA)
      .filter((classroom) => {
        const pattern = new RegExp(searchText, 'i')
        const searchable = [
          classroom.id,
          classroom.nome,
          classroom.descricao,
          classroom.disciplina,
        ].join(' ')

        return !searchText || pattern.test(searchable)
      })
  }, [activeOnly, classrooms, searchText])

  useDebounce(() => {
    onFilter(visibleClassrooms)
  }, actualDebounceTime, [visibleClassrooms])

  useEffect(() => {
    onFilter(visibleClassrooms)
  }, [activeOnly]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Box className={classes.root}>
      <FormField
        color="secondary"
        hiddenLabel
        placeholder="Pesquisar"
        size="small"
        endAdornment={<SearchIcon />}
        type="search"
        value={searchText}
        onChange={(event) => setSearchText(event.target.value)}
      />

      <Switch
        checked={activeOnly}
        label="Somente turmas ativas"
        onChange={(_, checked) => setActiveOnly(checked)}
      />
    </Box>
  )
}

export default ClassroomFilters
