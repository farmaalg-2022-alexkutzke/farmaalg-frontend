import ConfirmIcon from '@mui/icons-material/AddTask'
import Box from '@mui/material/Box'
import { FormEvent, useEffect, useState } from 'react'

import FormField from '@/common/FormField'
import Modal from '@/common/Modal'
import { useClassroomContext } from '~/helpers/hooks'

import { useStyles } from './styles'

namespace ClassroomRegistration {
  export interface Props {
    open: boolean
    onClose: () => void
  }
}

function ClassroomRegistration({
  open,
  onClose,
}: ClassroomRegistration.Props) {
  const classes = useStyles()
  const [accessCode, setAccessCode] = useState('')
  const { isUpdating, register } = useClassroomContext()

  async function handleRegister(event: FormEvent) {
    event.preventDefault()
    await register(accessCode)
    onClose()
  }

  useEffect(() => {
    open && setAccessCode('')
  }, [open]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Modal
      closable
      fullWidth
      maxWidth="xs"
      open={open}
      title="Registro em Nova Turma"
      onClose={() => onClose()}
      actions={[{
        disabled: !accessCode || isUpdating,
        icon: <ConfirmIcon />,
        label: 'Confirmar',
        type: 'submit',
        form: 'register-form',
      }]}
    >
      <Box id="register-form" component="form" onSubmit={handleRegister}>
        <FormField
          autoFocus
          className={classes.registrationField}
          label="Código de acesso"
          type="password"
          value={accessCode}
          onChange={(event) => setAccessCode(event.target.value)}
        />
      </Box>
    </Modal>
  )
}

export default ClassroomRegistration
