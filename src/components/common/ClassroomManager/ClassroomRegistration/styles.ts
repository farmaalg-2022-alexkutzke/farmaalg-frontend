import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  registrationField: {
    transform: `translateY(-${theme.spacing(2)})`,
  },
}))
