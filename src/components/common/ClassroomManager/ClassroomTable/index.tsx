import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import EnterIcon from '@mui/icons-material/Login'
import Box from '@mui/material/Box'
import Chip from '@mui/material/Chip'
import IconButton from '@mui/material/IconButton'
import { Link } from 'react-router-dom'

import DataTable from '@/common/DataTable'
import Menu from '@/common/Menu'
import { Turma, TurmaStatus } from '~/models'

import { useStyles } from './styles'

namespace ClassroomsTable {
  export interface Props {
    classrooms: Turma[]
    loading?: boolean
    onDelete?: (classroom: Turma) => void
    onEdit?: (classroom: Turma) => void
  }
}

function ClassroomsTable({
  classrooms,
  loading,
  onDelete,
  onEdit,
}: ClassroomsTable.Props) {
  const classes = useStyles()
  const columns: DataTable.Column[] = [
    { field: 'id', headerName: 'ID', width: 70, align: 'right' },
    { field: 'disciplina', headerName: 'Disciplina', flex: 2, minWidth: 120 },
    { field: 'nome', headerName: 'Nome', flex: 2, minWidth: 120 },
    { field: 'descricao', headerName: 'Descrição', flex: 3, minWidth: 150 },
    {
      field: 'status',
      headerName: 'Situação',
      align: 'center',
      renderCell({ row }) {
        const color: any = {
          [TurmaStatus.ATIVA]: 'secondary',
          [TurmaStatus.ENCERRADA]: 'error',
        }

        return (
          <Chip
            label={row.status}
            size="small"
            color={row.status in color
              ? color[row.status]
              : 'warning'}
          />
        )
      },
    },
    {
      field: ' ',
      sortable: false,
      width: (onDelete || onEdit) ? 120 : 56,
      renderCell({ row }) {
        const menuItems: Menu.ItemProps[] = []
        onEdit && menuItems.push({ label: 'Editar', icon: <EditIcon />, onClick: () => onEdit(row) })
        onDelete && menuItems.push({ label: 'Excluir', icon: <DeleteIcon />, onClick: () => onDelete(row) })

        return (
          <Box className={classes.recordActions}>
            <IconButton size="small" component={Link} to={`${row.id}`}>
              <EnterIcon />
            </IconButton>
            {menuItems.length > 0 && (
              <Menu id={`classroom-${row.id}`} items={menuItems} />
            )}
          </Box>
        )
      },
    },
  ]

  return (
    <Box className={classes.root}>
      <DataTable
        columns={columns}
        loading={loading}
        rows={classrooms}
      />
    </Box>
  )
}

export default ClassroomsTable
