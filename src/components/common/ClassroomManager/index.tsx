import AddIcon from '@mui/icons-material/Add'
import EnterIcon from '@mui/icons-material/Login'
import Box from '@mui/material/Box'
import { useState } from 'react'

import Button from '@/common/Button'
import ClassroomForm from '@/common/ClassroomForm'
import Modal from '@/common/Modal'
import { useClassroomContext } from '~/helpers/hooks'
import { Turma, TurmaDTO } from '~/models'

import ClassroomFilters from './ClassroomFilters'
import ClassroomRegistration from './ClassroomRegistration'
import ClassroomsTable from './ClassroomTable'
import { useStyles } from './styles'

namespace ClassroomManager {
  export interface Props {
    classrooms: Turma[]
    canCreate?: boolean
    canDelete?: boolean
    canRegister?: boolean
    canUpdate?: boolean
    filterable?: boolean
  }
}

function ClassroomManager({
  canCreate,
  canDelete,
  canRegister,
  canUpdate,
  filterable,
  classrooms,
}: ClassroomManager.Props) {
  const classes = useStyles()
  const classroomContext = useClassroomContext()
  const [isClassroomFormOpen, setClassroomFormOpen] = useState(false)
  const [isRegistrationFormOpen, setRegistrationFormOpen] = useState(false)
  const [focusedClassroom, setFocusedClassroom] = useState<Partial<Turma>>({})
  const [visibleClassrooms, setVisibleClassrooms] = useState(classrooms)
  const formTitle = focusedClassroom?.id
    ? `${focusedClassroom.nome}`
    : 'Nova Turma'

  function handleFormOpen(classroom: Partial<Turma>) {
    setFocusedClassroom(classroom)
    setClassroomFormOpen(true)
  }

  function handleFormClose() {
    // Don't use "setFocusedClassroom(...)" because
    // it will remove data from the modal before it closes
    setClassroomFormOpen(false)
  }

  function handleRegisterStart() {
    setRegistrationFormOpen(true)
  }

  async function handleSubmit(data: TurmaDTO) {
    focusedClassroom?.id
      ? await classroomContext.update(focusedClassroom.id, data)
      : await classroomContext.create(data)
    handleFormClose()
  }

  function handleDelete(classroom: Turma) {
    if (classroom.id && confirm(`Proceder com a exclusão da linguagem "${classroom.nome}"?`)) {
      classroomContext.destroy(classroom.id)
    }
  }

  return <>
    <Box className={classes.upperControls}>
      <Box>{filterable && (
        <ClassroomFilters classrooms={classrooms} onFilter={setVisibleClassrooms} />
      )}</Box>

      <Box>
        {canRegister && (
          <Button startIcon={<EnterIcon />} onClick={() => handleRegisterStart()}>
            Cadastrar-se em Nova Turma
          </Button>
        )}

        {canCreate && (
          <Button startIcon={<AddIcon />} onClick={() => handleFormOpen({})}>
            Nova Turma
          </Button>
        )}
      </Box>
    </Box>

    <ClassroomsTable
      classrooms={visibleClassrooms}
      onDelete={canDelete ? handleDelete : undefined}
      onEdit={canUpdate ? handleFormOpen : undefined}
    />

    <ClassroomRegistration
      open={isRegistrationFormOpen}
      onClose={() => setRegistrationFormOpen(false)}
    />

    <Modal
      closable
      fullWidth
      disabled={classroomContext.isSaving}
      keepMounted
      open={isClassroomFormOpen}
      title={formTitle}
      onClose={handleFormClose}
    >
      <ClassroomForm
        classroom={focusedClassroom}
        onCancel={handleFormClose}
        onSave={handleSubmit}
      />
    </Modal>
  </>
}

export default ClassroomManager
