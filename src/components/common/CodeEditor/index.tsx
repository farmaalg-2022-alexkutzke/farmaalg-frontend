import CodeIcon from '@mui/icons-material/Code'
import CodeTestIcon from '@mui/icons-material/DeveloperMode'
import Box from '@mui/material/Box'
import clsx from 'clsx'
import { ChangeEvent, useState } from 'react'

import Button from '@/common/Button'
import FormField from '@/common/FormField'
import Switch from '@/common/Switch'
import { Linguagem } from '~/models'

import { useStyles } from './styles'

namespace CodeEditor {
  export interface EditingData {
    code: string
    langId: number
  }

  export interface Props {
    languages: Linguagem[]
    disabled?: boolean
    onTest: (data: EditingData) => void
    onSubmit?: (data: EditingData) => void
  }
}

function CodeEditor({
  disabled,
  languages,
  onTest,
  onSubmit,
}: CodeEditor.Props) {
  const classes = useStyles()
  const [code, setCode] = useState(languages[0].exemplo ?? '')
  const [withLigature, setLigature] = useState(true)
  const [activeLanguage, setActiveLanguage] = useState(languages[0].id)

  function handleChangeLanguage(event: ChangeEvent<HTMLInputElement>) {
    const currentLanguage = languages.find((lang) => lang.id === activeLanguage)
    const untouched = currentLanguage?.exemplo === code
    const confirmChanges = untouched || window.confirm(
      'Todo o código já escrito será perdido. Deseja continuar com a?',
    )

    confirmChanges && setActiveLanguage(Number(event.target.value))

  }

  return (
    <Box className={classes.root}>
      <Box className={classes.controls}>
        <FormField
          className={classes.languageSelector}
          label="Linguagem de programação"
          options={languages.map(({ id, nome }) => ({
            label: nome,
            value: id,
          }))}
          value={activeLanguage}
          onChange={handleChangeLanguage}
        />

        <Switch
          className={classes.ligatureSwitch}
          checked={withLigature}
          label="Ligadura de fontes"
          onChange={() => setLigature(!withLigature)}
        />
      </Box>

      <FormField
        autoCapitalize="off"
        autoComplete="off"
        autoCorrect="off"
        autoFocus
        className={clsx(classes.textField, {
          [classes.ligature]: withLigature,
        })}
        hiddenLabel
        inputProps={{ style: { height: '98%' } }}
        multiline
        minRows={20}
        placeholder="Digite aqui o código fonte..."
        spellCheck={false}
        value={code}
        onChange={(event) => setCode(event.target.value)}
      />

      <Box className={classes.actionButtons}>
        <Button
          disabled={disabled}
          size="large"
          startIcon={<CodeTestIcon />}
          variant="outlined"
          onClick={() => onTest({ code, langId: activeLanguage })}
        >
          Executar
        </Button>

        {onSubmit && (
          <Button
            disabled={disabled}
            size="large"
            startIcon={<CodeIcon />}
            onClick={() => onSubmit({ code, langId: activeLanguage })}
          >
            Executar &amp; Enviar
          </Button>
        )}
      </Box>
    </Box>
  )
}

export default CodeEditor
