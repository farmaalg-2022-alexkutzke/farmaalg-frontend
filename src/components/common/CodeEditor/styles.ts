import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',

    height: '100%',
    backgroundColor: theme.palette.grey[200],
    padding: theme.spacing(1),
  },

  controls: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  languageSelector: {
    maxWidth: 200,
  },

  ligatureSwitch: {},

  textField: {
    flex: 1,
    margin: 0,
    overflow: 'auto',

    '& .MuiInputBase-root, & .MuiInputBase-input': {
      alignItems: 'start',

      height: '100%',
      padding: theme.spacing(0.5),

      fontFamily: '"JetBrains Mono", Consolas, Monaco, Andale Mono, monospace',
      fontSize: 12,
      fontWeight: 600,
      lineHeight: 1.4,
      whiteSpace: 'nowrap',
    },
  },

  ligature: {
    '& .MuiInputBase-input': {
      WebkitFontFeatureSettings: '"calt"',
      MozFontFeatureSettings: '"calt"',
      fontFeatureSettings: '"calt"',
      WebkitFontVariantLigatures: 'contextual',
      fontVariantLigatures: 'contextual',
    },
  },

  actionButtons: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    gap: theme.spacing(2),

    padding: theme.spacing(1),
  },
}))
