import Box from '@mui/material/Box'
import { DataGrid, DataGridProps, GridColDef, GridValueGetterParams } from '@mui/x-data-grid'
import { ptBR } from '@mui/x-data-grid'
import clsx from 'clsx'
import { useSessionStorage } from 'react-use'

import { useStyles } from './styles'

namespace DataTable {
  export type Props = Omit<DataGridProps, 'pageSize' | 'onPageSizeChange'>

  export type Column = GridColDef

  export type ValueGetter = GridValueGetterParams
}

function DataTable({
  className,
  components,
  rows,
  ...props
}: DataTable.Props) {
  const [actualPageSize, setPageSize] = useSessionStorage('items_per_page', 20)
  const classes = useStyles()

  return (
    <DataGrid
      autoHeight={rows.length > 0}
      className={clsx(className, classes.root)}
      components={{
        NoRowsOverlay: () => (
          <Box className={classes.noRowsOverlay}>
            <img src="/assets/img/void.svg" alt="tabela vazia" />
            <p>Nenhum registro encontrado</p>
          </Box>
        ),
        ...components,
      }}
      disableColumnMenu
      disableSelectionOnClick
      hideFooterPagination={rows.length <= actualPageSize}
      localeText={(ptBR as any).props?.MuiDataGrid.localeText}
      pageSize={actualPageSize}
      rows={rows}
      rowsPerPageOptions={[20, 50, 100]}
      onPageSizeChange={setPageSize}
      {...props}
    />
  )
}

export default DataTable
