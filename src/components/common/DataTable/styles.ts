import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {},

  noRowsOverlay: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

    padding: theme.spacing(16, 0, 8),

    '& img': {
      opacity: 0.8,
      width: 160,
    },

    '& p': {
      color: theme.palette.primary.main,
      fontSize: '1.25rem',
      fontWeight: 700,
    },
  },
}))
