import DeleteIcon from '@mui/icons-material/Delete'
import SaveIcon from '@mui/icons-material/Save'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import * as Yup from 'yup'

import Button from '@/common/Button'
import FormField from '@/common/FormField'
import { useForm } from '~/helpers/hooks'
import { DicaExercicioDTO } from '~/models'

import { useStyles } from './styles'

namespace TipsForm {
  export interface TempTip extends DicaExercicioDTO {
    index?: number
  }

  export interface Props {
    tip: TempTip | undefined
    onDelete: (tip: TempTip) => void
    onSubmit: (tip: TempTip) => void
  }
}

const manageTipValSchema = Yup.object().shape({
  titulo: Yup.string().required().max(50),
  descricao: Yup.string().required().max(255),
  numeroTentativas: Yup.number().required().integer().positive(),
}).required()

function TipsForm({ tip, onDelete, onSubmit }: TipsForm.Props) {
  const classes = useStyles()
  const { Form, getController } = useForm<TipsForm.TempTip>({
    valSchema: manageTipValSchema,
    defaultValues: {
      index: tip?.index,
      titulo: tip?.titulo,
      descricao: tip?.descricao,
      numeroTentativas: tip?.numeroTentativas ?? 3,
    },
  })

  return (
    <Form className={classes.root} onSubmit={onSubmit}>
      {!tip && <Typography variant="h3">Nova Dica</Typography>}

      <FormField
        autoFocus
        controller={getController('titulo')}
        label="Título"
        required
      />
      <FormField
        controller={getController('descricao')}
        label="Descrição"
        multiline
        placeholder="Texto compatível com Markdown"
        required
        rows={3}
      />
      <FormField
        controller={getController('numeroTentativas')}
        label="Tentativas para desbloquear dica"
        required
        type="number"
      />

      <Box className={classes.actions}>
        <Button
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          variant="contained"
        >
          Salvar
        </Button>

        {tip?.index !== undefined && (
          <Button
            color="error"
            startIcon={<DeleteIcon />}
            variant="text"
            onClick={() => onDelete(tip)}
          >
            Excluir
          </Button>
        )}
      </Box>
    </Form>
  )
}

export default TipsForm
