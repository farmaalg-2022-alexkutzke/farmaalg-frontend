import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    padding: theme.spacing(2),

    '& h3': {
      borderBottom: `1px solid ${theme.palette.divider}`,

      color: theme.palette.primary.main,
      fontSize: '1.5rem',
      fontWeight: 700,
    },
  },

  actions: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row-reverse',

    marginTop: theme.spacing(1),
    borderTop: `1px solid ${theme.palette.divider}`,
    paddingTop: theme.spacing(1),
  },
}))
