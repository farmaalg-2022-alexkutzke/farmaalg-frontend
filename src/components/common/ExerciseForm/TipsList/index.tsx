import AddIcon from '@mui/icons-material/Add'
import LightbulbIcon from '@mui/icons-material/Lightbulb'
import Box from '@mui/material/Box'
import IconButton from '@mui/material/IconButton'
import Popover from '@mui/material/Popover'
import Tooltip from '@mui/material/Tooltip'
import { MouseEvent, useState } from 'react'

import Button from '@/common/Button'
import { DicaExercicioDTO } from '~/models'

import { useStyles } from './styles'
import TipsForm from './TipForm'

namespace TipsList {
  export interface Props {
    tips: DicaExercicioDTO[]
    onSubmit: (tips: DicaExercicioDTO[]) => void
  }
}

function TipsList({ tips, onSubmit }: TipsList.Props) {
  const classes = useStyles()
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const [focusedTip, setFocusedTip] = useState<TipsForm.TempTip>()

  function handleOpenPopover(event: MouseEvent<HTMLButtonElement>, tip?: TipsForm.TempTip) {
    setAnchorEl(event.currentTarget)
    setFocusedTip(tip)
  }

  function handleDelete(tip: TipsForm.TempTip) {
    tip.index !== undefined && tips.splice(tip.index, 1)
    onSubmit([...tips])
    setAnchorEl(null)
  }

  function handleSubmit(tip: TipsForm.TempTip) {
    /**
     * Tips are never updated in backend. Instead, they and their
     * relationships with the exercise are completely deleted and
     * recreated, so no reconciliation process is required on the
     * existing tips. For this reason, the array index is used as
     * identifier here.
     */
    tip.index === undefined
      ? tips.push(tip)
      : tips.splice(tip.index, 1, tip)
    onSubmit([...tips])
    setAnchorEl(null)
  }

  return (
    <Box className={classes.root}>
      {tips.map((tip, index) => (
        <IconButton
          key={index}
          onClick={(event) => handleOpenPopover(event, {
            ...tip,
            index,
          })}
        >
          <Tooltip placement="top" title={tip.titulo}>
            <LightbulbIcon />
          </Tooltip>
        </IconButton>
      ))}

      <Button
        startIcon={<AddIcon />}
        size="large"
        variant="outlined"
        onClick={handleOpenPopover}
        aria-describedby="tip-form-popover"
      >
        Dica para resolução
      </Button>

      <Popover
        anchorEl={anchorEl}
        id="tip-form-popover"
        open={Boolean(anchorEl)}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        transformOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        onClose={() => setAnchorEl(null)}
      >
        <TipsForm
          tip={focusedTip}
          onDelete={handleDelete}
          onSubmit={handleSubmit}
        />
      </Popover>
    </Box>)
}

export default TipsList
