import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',

    margin: theme.spacing(2, 0),

    '& button:last-child': {
      marginLeft: theme.spacing(1),
    },

    '& button:not(:last-child)': {
      color: theme.palette.primary.main,
    },
  },
}))
