import CloseIcon from '@mui/icons-material/Close'
import SaveIcon from '@mui/icons-material/Save'
import Autocomplete from '@mui/material/Autocomplete'
import Box from '@mui/material/Box'
import SimpleCheckbox from '@mui/material/Checkbox'
import Grid from '@mui/material/Grid'
import ListItemText from '@mui/material/ListItemText'
import MenuItem from '@mui/material/MenuItem'
import { useEffect, useMemo } from 'react'
import * as Yup from 'yup'

import Button from '@/common/Button'
import Checkbox from '@/common/Checkbox'
import FormField from '@/common/FormField'
import MarkdownRenderer from '@/common/MarkdownRenderer'
import {
  useForm,
  useClassroomContext,
  useExerciseContext,
  useLanguageContext,
} from '~/helpers/hooks'
import { Exercicio, ExercicioDTO, Linguagem } from '~/models'

import { useStyles } from './styles'
import TipsForm from './TipsList'

namespace ExerciseForm{
  export interface Props {
    exercise: Partial<Exercicio>
    onSave: (exercise: ExercicioDTO) => void
    onCancel?: () => void
  }
}

const manageExerciseValSchema = Yup.object().shape({
  turma: Yup.object({
    id: Yup.number().required().typeError('Seleção obrigatória.'),
  }).required(),
  titulo: Yup.string().required().max(50),
  descricao: Yup.string().required().max(255),
  enunciado: Yup.string().required(),
  ehPublico: Yup.boolean().required(),
  linguagens: Yup.array().min(1).of(Yup.object({
    id: Yup.number().required().integer(),
  }).required()),
  tags: Yup.array().of(Yup.object({
    nome: Yup.string().required()
      .max(30, 'Uma tag deve ter no máximo 30 caracteres.'),
  }).optional()),
}).required()

function ExerciseForm({
  exercise,
  onCancel,
  onSave,
}: ExerciseForm.Props) {
  const classes = useStyles()
  const { languages } = useLanguageContext()
  const { classrooms } = useClassroomContext()
  const { isSaving, tags } = useExerciseContext()
  const classroomsComboBox = useMemo(() => classrooms.map(({ id, nome }) => ({
    value: id,
    label: nome,
  })), [classrooms])
  const { Form, getController, getValues, setValue, watch, formState } = useForm<ExercicioDTO>({
    defaultValues: { linguagens: [], dicas: [], tags: [] },
    valSchema: manageExerciseValSchema,
  })
  const formId = 'exercise-form'

  useEffect(() => {
    setValue('turma.id', exercise.turma?.id as any ?? '')
    setValue('titulo', exercise.titulo ?? '')
    setValue('descricao', exercise.descricao ?? '')
    setValue('enunciado', exercise.enunciado ?? '')
    setValue('ehPublico', exercise.ehPublico ?? false)
    setValue('tags', exercise.tags ?? [])
    setValue('dicas', exercise.dicas ?? [])
    // this is necessary to match the reference of the objects
    // selected in the combo box
    setValue('linguagens', (exercise.linguagens ?? []).map(({ id }) => {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      return languages.find((lang) => id === lang.id)!
    }))
  }, [exercise]) // eslint-disable-line react-hooks/exhaustive-deps

  return <>
    <Grid container columnSpacing={2}>
      <Grid
        item
        xs={12}
        md={5}
        container
        component={Form}
        id={formId}
        onSubmit={onSave}
      >
        <Grid item xs={12}>
          <FormField
            controller={getController('turma.id')}
            label="Turma"
            options={classroomsComboBox}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <FormField
            controller={getController('titulo')}
            label="Título"
            required
          />
        </Grid>
        <Grid item xs={12}>
          <FormField
            controller={getController('descricao')}
            label="Descrição"
            required
            multiline
            rows={2}
          />
        </Grid>
        <Grid item xs={12}>
          <FormField
            className={classes.monospace}
            controller={getController('enunciado')}
            label="Enunciado"
            placeholder="Digite o enunciado do exercício (compatível com Markdown)"
            required
            multiline
            rows={8}
          />
        </Grid>
        <Grid item xs={12}>
          <Checkbox
            controller={getController('ehPublico')}
            label="Exercício visível para outros professores"
          />
        </Grid>
        <Grid item xs={12}>
          <FormField
            className={classes.monospace}
            controller={getController('linguagens')}
            label="Linguagens de programação"
            placeholder="Seelecione pelo menos uma"
            required
            SelectProps={{
              multiple: true,
              renderValue: (values) => (values as Linguagem[]).map(({ nome }) => nome).join(', '),
              onChange: (event) => setValue('linguagens', event.target.value as any),
            }}
          >
            {languages.map((lang) => (
              <MenuItem key={lang.id} value={lang as any}>
                <SimpleCheckbox
                  checked={Boolean(watch('linguagens').some((foo) => lang.id === foo.id))}
                />
                <ListItemText primary={lang.nome} />
              </MenuItem>
            ))}
          </FormField>
        </Grid>
        <Grid item xs={12}>
          <Autocomplete
            className={classes.tags}
            defaultValue={getValues('tags')}
            freeSolo
            getOptionLabel={(option) => option.nome}
            multiple
            options={tags}
            renderInput={({ InputLabelProps, InputProps, inputProps }) => (
              <FormField
                error={Boolean(formState.errors.tags)}
                helperText={formState.errors.tags?.filter((tag) => tag)?.[0]?.nome?.message}
                InputLabelProps={InputLabelProps}
                InputProps={InputProps}
                inputProps={inputProps}
                label="Tags"
                placeholder="Tecle ENTER para adicionar uma nova tag"
              />
            )}
            value={watch('tags')}
            onChange={(_, value) => setValue('tags', value.map((option) => ({
              nome: typeof option === 'string' ? option.toLowerCase() : option.nome,
            })))}
          />
        </Grid>
      </Grid>

      <Grid
        item
        xs={12}
        md={7}
        container
      >
        <Grid item xs={12}>
          <MarkdownRenderer title={watch('titulo')}>
            {watch('enunciado')}
          </MarkdownRenderer>
        </Grid>

        <Grid item xs={12} marginTop="auto">
          <TipsForm
            tips={watch('dicas')}
            onSubmit={(tips) => setValue('dicas', tips)}
          />
        </Grid>
      </Grid>
    </Grid>

    <Box className={classes.formActions}>
      {onCancel && (
        <Button
          startIcon={<CloseIcon />}
          variant="text"
          onClick={() => onCancel()}
        >
          Cancelar
        </Button>
      )}

      <Button
        disabled={isSaving}
        form={formId}
        startIcon={<SaveIcon />}
        type="submit"
      >
        Salvar
      </Button>
    </Box>
  </>
}

export default ExerciseForm
