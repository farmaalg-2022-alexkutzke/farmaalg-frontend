import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  monospace: {
    '& input, & textarea': {
      fontFamily: '"JetBrains Mono", Consolas, Monaco, Andale Mono, monospace',
      fontSize: '0.875rem',
      fontWeight: 600,
      lineHeight: 1.2,
    },
  },

  tags: {
    '& .MuiChip-root': {
      backgroundColor: theme.palette.secondary.main,
      color: theme.palette.secondary.contrastText,

      '& svg': {
        color: theme.palette.secondary.light,
      },
    },
  },

  formActions: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    gap: theme.spacing(2),

    marginTop: theme.spacing(2),
    borderTopColor: theme.palette.divider,
    borderTopStyle: 'solid',
    borderTopWidth: 1,
    paddingTop: theme.spacing(2),
  },
}))
