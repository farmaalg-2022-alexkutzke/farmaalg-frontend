import SearchIcon from '@mui/icons-material/Search'
import Box from '@mui/material/Box'
import { useEffect, useState } from 'react'
import { useDebounce } from 'react-use'

import FormField from '@/common/FormField'
import Switch from '@/common/Switch'
import { useAuth } from '~/helpers/hooks'
import { Exercicio, Perfil } from '~/models'

import { useStyles } from './styles'

namespace ExerciseFilters {
  export interface Props {
    exercises: Exercicio[]
    debounce?: number | false
    defaultValue?: string
    onFilter: (exercises: Exercicio[]) => void
  }
}

function ExerciseFilters({
  debounce = 500,
  defaultValue = '',
  exercises,
  onFilter,
}: ExerciseFilters.Props) {
  const { user } = useAuth()
  const classes = useStyles()
  const [searchText, setSearchText] = useState(defaultValue)
  const [ownItemsOnly, setOwnItemsOnly] = useState(false)
  const actualDebounceTime = debounce && debounce > 0 ? debounce : 0

  useEffect(() => {
    onFilter(exercises.filter((exercise) => {
      return !ownItemsOnly || exercise.turma.professor.id === user?.id
    }))
  }, [exercises, ownItemsOnly]) // eslint-disable-line react-hooks/exhaustive-deps

  useDebounce(() => {
    onFilter(exercises.filter((ex) => {
      const pattern = new RegExp(searchText, 'i')
      const searchable = [
        ex.id,
        ex.titulo,
        ex.descricao,
        ex.enunciado,
        ex.tags.map((tag) => tag.nome).join(' '),
      ].join(' ')

      return !searchText || pattern.test(searchable)
    }))
  }, actualDebounceTime, [exercises, searchText])

  return (
    <Box className={classes.root}>
      <FormField
        color="secondary"
        hiddenLabel
        placeholder="Pesquisar"
        size="small"
        endAdornment={<SearchIcon />}
        type="search"
        value={searchText}
        onChange={(event) => setSearchText(event.target.value)}
      />

      {user?.perfil === Perfil.PROFESSOR && (
        <Switch
          checked={ownItemsOnly}
          label="Somente criados por mim"
          onChange={(_, checked) => setOwnItemsOnly(checked)}
        />
      )}
    </Box>
  )
}

export default ExerciseFilters
