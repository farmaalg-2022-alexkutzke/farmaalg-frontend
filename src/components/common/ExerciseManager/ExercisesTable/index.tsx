import ConfirmIcon from '@mui/icons-material/AddTask'
import ReplicateIcon from '@mui/icons-material/ControlPointDuplicate'
import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import EnterIcon from '@mui/icons-material/Login'
import ExecuteIcon from '@mui/icons-material/PlayCircleOutline'
import Box from '@mui/material/Box'
import IconButton from '@mui/material/IconButton'
import { useMemo, useState } from 'react'
import { useNavigate } from 'react-router-dom'

import DataTable from '@/common/DataTable'
import FormField from '@/common/FormField'
import Menu from '@/common/Menu'
import Modal from '@/common/Modal'
import { useAuth, useClassroomContext } from '~/helpers/hooks'
import { Exercicio } from '~/models'

import { useStyles } from './styles'

namespace ExercisesTable {
  export interface Props {
    exercises: Exercicio[]
    expandable?: boolean
    loading?: boolean
    onDelete?: (exercise: Exercicio) => void
    onEdit?: (exercise: Exercicio) => void
    onReplicate?: (exerciseId: number, classroomId: number) => void
  }
}

function ExercisesTable({
  exercises,
  expandable,
  loading,
  onDelete,
  onEdit,
  onReplicate,
}: ExercisesTable.Props) {
  const classes = useStyles()
  const navigate = useNavigate()
  const { user } = useAuth()
  const [replicateExercise, setReplicateExercise] = useState<Exercicio>()
  const [replicateClassroomId, setReplicateClassroomId] = useState<number>()
  const { classrooms } = useClassroomContext()
  const classroomsComboBox = useMemo(() => classrooms.map(({ id, nome }) => ({
    value: id,
    label: nome,
  })), [classrooms])
  const columns: DataTable.Column[] = [
    { field: 'id', headerName: 'ID', width: 70, align: 'right' },
    { field: 'titulo', headerName: 'Nome', flex: 1, minWidth: 200 },
    { field: 'descricao', headerName: 'Descrição', flex: 2, minWidth: 400 },
    {
      field: ' ',
      sortable: false,
      width: (expandable || onDelete || onEdit || onReplicate) ? 120 : 56,
      renderCell({ row }: { row: Exercicio }) {
        const menuItems: Menu.ItemProps[] = []
        expandable && menuItems.push({ label: 'Executar', icon: <ExecuteIcon />, onClick: () => handleExecute(row) })
        onReplicate && menuItems.push({ label: 'Replicar', icon: <ReplicateIcon />, onClick: () => handleReplicateTrigger(row) })
        if (user?.isAdmin() || user?.id === row.turma.professor.id) {
          onEdit && menuItems.push({ label: 'Editar', icon: <EditIcon />, onClick: () => onEdit(row) })
          onDelete && menuItems.push({ label: 'Excluir', icon: <DeleteIcon />, onClick: () => onDelete(row) })
        }

        return (
          <Box className={classes.recordActions}>
            {expandable ? (
              <IconButton size="small" onClick={() => handleNavigate(row)}>
                <EnterIcon />
              </IconButton>
            ) : (
              <IconButton size="small" onClick={() => handleExecute(row)}>
                <ExecuteIcon />
              </IconButton>
            )}
            {menuItems.length > 0 && (
              <Menu id={`exercise-${row.id}`} items={menuItems} />
            )}
          </Box>
        )
      },
    },
  ]

  function handleNavigate(exercise: Exercicio) {
    navigate(`./${exercise.id}/respostas`)
  }

  function handleExecute(exercise: Exercicio) {
    navigate('/editor', { state: { exerciseId: exercise.id } })
  }

  function handleReplicateTrigger(exercise: Exercicio) {
    setReplicateClassroomId(undefined)
    setReplicateExercise(exercise)
  }

  function handleReplicateSubmit() {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const exerciseId = replicateExercise!.id!
    const classroomId = replicateClassroomId
    classroomId && onReplicate?.(exerciseId, classroomId)
    setReplicateExercise(undefined)
  }

  return (
    <Box className={classes.root}>
      <DataTable
        columns={columns}
        loading={loading}
        rows={exercises}
      />

      <Modal
        closable
        fullWidth
        maxWidth="xs"
        open={Boolean(replicateExercise)}
        title="Replicar exercício para..."
        onClose={() => setReplicateExercise(undefined)}
        actions={[{
          disabled: !replicateClassroomId,
          icon: <ConfirmIcon />,
          label: 'Confirmar',
          onClick: handleReplicateSubmit,
        }]}
      >
        <FormField
          className={classes.classroomSelector}
          label="Selecione a turma"
          value={replicateClassroomId ?? ''}
          options={classroomsComboBox}
          onChange={(event) => setReplicateClassroomId(Number(event.target.value))}
        />
      </Modal>
    </Box>
  )
}

export default ExercisesTable
