import AddIcon from '@mui/icons-material/Add'
import Box from '@mui/material/Box'
import { useState } from 'react'

import Button from '@/common/Button'
import ExerciseForm from '@/common/ExerciseForm'
import Modal from '@/common/Modal'
import { useExerciseContext } from '~/helpers/hooks'
import { Exercicio, ExercicioDTO } from '~/models'

import ExerciseFilters from './ExerciseFilters'
import ExercisesTable from './ExercisesTable'
import { useStyles } from './styles'

namespace ExerciseManager {
  export interface Props {
    exercises: Exercicio[]
    canCreate?: boolean
    canDelete?: boolean
    canExpand?: boolean
    canUpdate?: boolean
    filterable?: boolean
  }
}

function ExerciseManager({
  canCreate,
  canDelete,
  canExpand,
  canUpdate,
  exercises,
  filterable,
}: ExerciseManager.Props) {
  const classes = useStyles()
  const exerciseContext = useExerciseContext()
  const [isFormOpen, setFormOpen] = useState(false)
  const [focusedExercise, setFocusedExercise] = useState<Partial<Exercicio>>({})
  const [visibleExercises, setVisibleExercises] = useState(exercises)
  const formTitle = focusedExercise.titulo
    ? `Exercício "${focusedExercise.titulo}"`
    : 'Novo Exercicio'

  function handleFormOpen(exercise: Partial<Exercicio>) {
    setFocusedExercise(exercise)
    setFormOpen(true)
  }

  function handleFormClose() {
    // Don't use "setFocusedExercise(...)" because
    // it will remove data from the modal before it closes
    setFormOpen(false)
  }

  async function handleSubmit(data: ExercicioDTO) {
    focusedExercise?.id
      ? await exerciseContext.update(focusedExercise.id, data)
      : await exerciseContext.create(data)
    handleFormClose()
  }

  function handleReplicate(exerciseId: number, classroomId: number) {
    exerciseContext.replicate(exerciseId, classroomId)
  }

  function handleDelete(exercise: Exercicio) {
    if (exercise.id && confirm(`Proceder com a exclusão do exercício "${exercise.titulo}"?`)) {
      exerciseContext.destroy(exercise.id)
    }
  }

  return <>
    <Box className={classes.upperControls}>
      <Box>{filterable && (
        <ExerciseFilters exercises={exercises} onFilter={setVisibleExercises} />
      )}</Box>

      <Box>{canCreate && (
        <Button startIcon={<AddIcon />} onClick={() => handleFormOpen({})}>
          Novo Exercicio
        </Button>
      )}</Box>
    </Box>

    <ExercisesTable
      exercises={visibleExercises}
      expandable={canExpand}
      onDelete={canDelete ? handleDelete : undefined}
      onEdit={canUpdate ? handleFormOpen : undefined}
      onReplicate={canUpdate ? handleReplicate : undefined}
    />

    <Modal
      closable
      fullWidth
      maxWidth="lg"
      open={isFormOpen}
      title={formTitle}
      onClose={handleFormClose}
    >
      <ExerciseForm
        exercise={focusedExercise}
        onCancel={handleFormClose}
        onSave={handleSubmit}
      />
    </Modal>
  </>
}

export default ExerciseManager
