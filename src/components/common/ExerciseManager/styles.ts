import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles({
  upperControls: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
})
