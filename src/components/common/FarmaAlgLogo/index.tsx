import clsx from 'clsx'

import { useStyles } from './styles'

function FarmaAlgLogo() {
  const classes = useStyles()

  return (
    <strong className={clsx('FarmaAlgLogo', classes.root)}>
      Farma<span>•</span>Alg
    </strong>
  )
}

export default FarmaAlgLogo
