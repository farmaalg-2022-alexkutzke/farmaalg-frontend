import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.primary.main,
    fontFamily: '"JetBrains Mono", Consolas, Monaco, Andale Mono, monospace',
    fontSize: '2rem',
    fontWeight: 800,
    textDecoration: 'none',
    userSelect: 'none',

    '&::before, &::after, & > span': {
      color: theme.palette.secondary.main,
      fontSize: '1.6rem',
      fontWeight: 800,
    },

    '&::before': {
      marginRight: 4,
      content: '"<"',
    },

    '&::after': {
      marginLeft: 2,

      content: '"/>"',
      WebkitFontFeatureSettings: '"calt"',
      MozFontFeatureSettings: '"calt"',
      fontFeatureSettings: '"calt"',
      WebkitFontVariantLigatures: 'contextual',
      fontVariantLigatures: 'contextual',
    },

    '& > span': {
      margin: '0 -2px',
    },
  },
}))
