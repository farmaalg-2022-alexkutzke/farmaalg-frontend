import InputAdornment from '@mui/material/InputAdornment'
import MenuItem from '@mui/material/MenuItem'
import TextField, { TextFieldProps } from '@mui/material/TextField'
import clsx from 'clsx'
import { ReactNode } from 'react'
import { Control, Controller } from 'react-hook-form'

import { useStyles } from './styles'

namespace FormField {
  export type Props<T = any> = Omit<TextFieldProps, 'select'> & {
    controller?: {
      control: Control<T>
      name: string
      defaultValue?: any
      getValue: () => any
      setValue: (value: any) => void
    }
    options?: string[] | Array<{ value: number | string, label: ReactNode }>
    readOnly?: boolean
    startAdornment?: ReactNode
    endAdornment?: ReactNode
  }
}

function FormField({
  children,
  controller,
  endAdornment,
  InputProps,
  options,
  readOnly,
  startAdornment,
  ...props
}: FormField.Props) {
  const classes = useStyles()
  const actualProps: TextFieldProps = {
    ...props,
    className: clsx(props.className, classes.root, {
      [classes.hidden]: props.type === 'hidden',
    }),
    color: props.color ?? 'primary',
    fullWidth: props.fullWidth ?? true,
    InputProps: {
      endAdornment: endAdornment && (
        <InputAdornment position="end">{endAdornment}</InputAdornment>
      ),
      startAdornment: startAdornment && (
        <InputAdornment position="start">{startAdornment}</InputAdornment>
      ),
      readOnly,
      ...InputProps,
    },
    select: !readOnly && Boolean(children || options),
    size: props.size ?? 'small',
    type: props.type ?? 'text',
    variant: props.variant ?? 'filled',
    children: readOnly ? undefined : (children ?? options?.map((option) => {
      const value = typeof option === 'string' ? option : option.value
      const label = typeof option === 'string' ? option : option.label

      return (
        <MenuItem key={value} value={value}>
          {label}
        </MenuItem>
      )
    })),
  }

  if (!controller) {
    return <TextField {...actualProps} />
  }

  return (
    <Controller
      {...controller}
      render={({ field, fieldState: { error } }) => (
        <TextField
          {...actualProps}
          {...field}
          error={Boolean(error)}
          helperText={error ? error.message : actualProps.helperText}
          onBlur={(event) => {
            actualProps.onBlur?.(event)
            field.onBlur()
          }}
          onChange={(event) => {
            actualProps.onChange?.(event)
            field.onChange(event)
          }}
        />
      )}
    />
  )
}

export default FormField
