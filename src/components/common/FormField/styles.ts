import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(1, 0, 1),

    // date picker icon in Chromium-based browsers
    '& input::-webkit-calendar-picker-indicator': {
      position: 'relative',
      top: -8,
      cursor: 'pointer',
      // Hack to convert black to #005F86 (theme.palette.primary.main)
      // generated in https://codepen.io/sosuke/pen/Pjoqqp
      filter: `
        invert(18%)
        sepia(81%)
        saturate(2427%)
        hue-rotate(178deg)
        brightness(99%)
        contrast(102%)
      `,
    },

    '& .MuiInputLabel-filled': {
      color: theme.palette.primary.main,

      '&.MuiInputLabel-shrink': {
        color: theme.palette.secondary.main,
        fontWeight: 500,

        '& + .MuiFilledInput-underline::before': {
          borderBottom: `1px solid ${theme.palette.primary.main}`,
        },
      },

      '&.Mui-error, &.Mui-error + * svg': {
        color: theme.palette.error.main,
      },
    },

    '& .MuiFilledInput-root': {
      backgroundColor: '#e6f3ec',
      color: theme.palette.primary.main,

      '&.Mui-error': {
        backgroundColor: theme.palette.error.light,
      },

      '&::before': {
        borderColor: 'transparent',
      },

      '&.Mui-focused': {
        backgroundColor: '#bfe0ce',
      },
    },

    '& .MuiSelect-icon': {
      color: theme.palette.secondary.main,
    },
  },

  hidden: {
    display: 'none',
  },
}))
