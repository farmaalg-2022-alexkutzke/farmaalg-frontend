import DeleteIcon from '@mui/icons-material/Delete'
import CheckIcon from '@mui/icons-material/Done'
import EditIcon from '@mui/icons-material/Edit'
import MenuIcon from '@mui/icons-material/MoreVert'
import Grid from '@mui/material/Grid'
import IconButton from '@mui/material/IconButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import { useState } from 'react'
import * as Yup from 'yup'

import Button from '@/common/Button'
import FormField from '@/common/FormField'
import { useForm } from '~/helpers/hooks'
import { InstituicaoContato, InstituicaoContatoDTO } from '~/models'

import { useStyles } from './styles'

namespace ContactForm {
  export interface Props {
    contact: InstituicaoContato | undefined
    startEditing?: boolean
    onDelete: (contactId: number) => void
    onSave: (contact: InstituicaoContatoDTO) => void
  }
}

const manageContactValSchema = Yup.object().shape({
  tipo: Yup.string().optional().nullable().max(14),
  contato: Yup.string().optional().nullable().max(100),
  pessoa: Yup.string().optional().nullable().max(50),
}).required()

function ContactForm({
  contact,
  startEditing: initializeEditing = false,
  onDelete,
  onSave,
}: ContactForm.Props) {
  const classes = useStyles()
  const RANDOM_ID = Math.random().toString(36).slice(-5)
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const [isEditing, setEditing] = useState(initializeEditing)
  const { Form, getController } = useForm<InstituicaoContatoDTO>({
    valSchema: manageContactValSchema,
    defaultValues: contact && {
      id: contact.id,
      tipo: contact.tipo,
      contato: contact.contato,
      pessoa: contact.pessoa,
    },
  })

  function handleSubmit(data: InstituicaoContatoDTO) {
    onSave(data)
    setEditing(false)
  }

  function handleEdit() {
    setAnchorEl(null)
    setEditing(true)
  }

  function handleDelete() {
    setAnchorEl(null)
    contact?.id && onDelete(contact.id)
  }

  return (
    <Grid
      container
      columnSpacing={2}
      alignItems="center"
      component={Form}
      onSubmit={handleSubmit}
    >
      <Grid item xs>
        <FormField
          autoFocus
          controller={getController('tipo')}
          label="Tipo"
          size="small"
          options={['E-mail', 'Telefone', 'Celular', 'Outro']}
          readOnly={!isEditing}
        />
      </Grid>
      <Grid item xs>
        <FormField
          controller={getController('contato')}
          label="Contato"
          size="small"
          readOnly={!isEditing}
        />
      </Grid>
      <Grid item xs>
        <FormField
          controller={getController('pessoa')}
          label="Pessoa"
          size="small"
          readOnly={!isEditing}
        />
      </Grid>
      <Grid item xs={false}>
        {isEditing ? (
          <Button className={classes.confirmButton} type="submit">
            <CheckIcon />
          </Button>
        ) : (
          <>
            <IconButton
              id={`button-${RANDOM_ID}`}
              size="large"
              onClick={(event) => setAnchorEl(event.currentTarget)}
              aria-controls={`menu-${RANDOM_ID}`}
              aria-expanded={anchorEl ? 'true' : undefined}
              aria-haspopup="true"
              aria-label="opções"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id={`menu-${RANDOM_ID}`}
              MenuListProps={{
                'aria-labelledby': `button-${RANDOM_ID}`,
              }}
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={() => setAnchorEl(null)}
            >
              <MenuItem onClick={handleEdit}>
                <ListItemIcon><EditIcon /></ListItemIcon>
                <ListItemText>Editar</ListItemText>
              </MenuItem>
              <MenuItem onClick={handleDelete}>
                <ListItemIcon><DeleteIcon /></ListItemIcon>
                <ListItemText>Excluir</ListItemText>
              </MenuItem>
            </Menu>
          </>
        )}
      </Grid>
    </Grid>
  )
}

export default ContactForm
