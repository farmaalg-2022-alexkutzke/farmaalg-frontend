import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  confirmButton: {
    height: 48,
    width: 48,
    minWidth: 'auto',
    marginBottom: theme.spacing(1),
    padding: 0,
  },
}))
