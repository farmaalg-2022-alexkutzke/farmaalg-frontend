import AddIcon from '@mui/icons-material/Add'
import { useState } from 'react'

import Button from '@/common/Button'
import { useInstitutionContext } from '~/helpers/hooks'
import { InstituicaoContato, InstituicaoContatoDTO } from '~/models'

import ContactForm from './ContactForm'
import { useStyles } from './styles'

namespace ContactsList {
  export interface Props {
    institutionId: number
    contacts: InstituicaoContato[]
  }
}

function ContactsList({ institutionId, contacts }: ContactsList.Props) {
  const classes = useStyles()
  const [isCreating, setCreating] = useState(false)
  const { addContact, updateContact, removeContact } = useInstitutionContext()

  async function handleSave(data: InstituicaoContatoDTO) {
    data?.id
      ? await updateContact(institutionId, data.id, data)
      : await addContact(institutionId, data)
    setCreating(false)
  }

  function handleDelete(contactId: number) {
    removeContact(institutionId, contactId)
  }

  return (
    <>
      {contacts.map((contact) => (
        <ContactForm
          key={contact.id}
          contact={contact}
          onDelete={handleDelete}
          onSave={handleSave}
        />
      ))}

      {isCreating ? (
        <ContactForm
          contact={undefined}
          startEditing
          onDelete={handleDelete}
          onSave={handleSave}
        />
      ) : (
        <Button
          startIcon={<AddIcon />}
          variant="outlined"
          className={classes.addNewButton}
          onClick={() => setCreating(true)}
        >
          Novo Contato
        </Button>
      )}
    </>
  )
}

export default ContactsList
