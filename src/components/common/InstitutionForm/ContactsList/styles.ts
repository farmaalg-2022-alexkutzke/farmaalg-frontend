import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles({
  addNewButton: {
    height: 48,
    width: '100%',
  },
})
