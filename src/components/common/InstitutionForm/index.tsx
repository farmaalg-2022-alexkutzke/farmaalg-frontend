import CloseIcon from '@mui/icons-material/Close'
import SaveIcon from '@mui/icons-material/Save'
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import CepService from 'cep-promise/dist/cep-promise-browser'
import { useEffect } from 'react'
import * as Yup from 'yup'

import Button from '@/common/Button'
import FormField from '@/common/FormField'
import { useForm, useInstitutionContext } from '~/helpers/hooks'
import { Instituicao, InstituicaoDTO } from '~/models'

import ContactsList from './ContactsList'
import { useStyles } from './styles'

namespace InstitutionForm {
  export interface Props {
    institution: Partial<Instituicao>
    onSave: (language: InstituicaoDTO) => void
    onCancel?: () => void
  }
}

const STATES_LIST = [
  'AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO',
  'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI',
  'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO',
]

const manageInstitutionValSchema = Yup.object().shape({
  nome: Yup.string().required().max(100),
  cnpj: Yup.string().required().cnpj(),
  cep: Yup.string().required().cep(),
  endereco: Yup.string().required().max(50),
  numero: Yup.string().required().max(10),
  estado: Yup.string().required().oneOf(STATES_LIST),
  cidade: Yup.string().required().max(50),
  bairro: Yup.string().required().max(50),
}).required()

function InstitutionForm({
  institution,
  onCancel,
  onSave,
}: InstitutionForm.Props) {
  const classes = useStyles()
  const { isSaving } = useInstitutionContext()
  const { Form, getController, setValue, watch } = useForm<InstituicaoDTO>({
    valSchema: manageInstitutionValSchema,
  })
  const cep = watch('cep')
  const formId = 'institution-form'

  useEffect(() => {
    setValue('nome', institution.nome ?? '')
    setValue('cnpj', institution.cnpj ?? '')
    setValue('cep', institution.cep ?? '')
    setValue('endereco', institution.endereco ?? '')
    setValue('numero', institution.numero ?? '')
    setValue('estado', institution.estado ?? '')
    setValue('cidade', institution.cidade ?? '')
    setValue('bairro', institution.bairro ?? '')
  }, [institution]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    // search CEP while typing it
    const cepNumbers = String(cep).replace(/\D/g, '')
    if (cepNumbers.length === 8) {
      CepService(cepNumbers).then((response) => {
        setValue('endereco', response.street)
        setValue('estado', response.state)
        setValue('cidade', response.city)
        setValue('bairro', response.neighborhood)
      })
    }
  }, [cep]) // eslint-disable-line react-hooks/exhaustive-deps

  return <>
    <Grid
      container
      columnSpacing={2}
      component={Form}
      id={formId}
      onSubmit={onSave}
    >
      {!institution.id && (
        <Grid item xs={12} >
          <FormField
            autoFocus
            controller={getController('nome')}
            label="Nome da instituição"
            required
          />
        </Grid>
      )}
      <Grid item xs={12} sm={6}>
        <FormField
          controller={getController('cnpj')}
          label="CNPJ"
          required
          readOnly={Boolean(institution.id)}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <FormField
          controller={getController('cep')}
          label="CEP"
          required
          readOnly={Boolean(institution.id)}
        />
      </Grid>
      <Grid item xs={8}>
        <FormField
          controller={getController('endereco')}
          label="Endereço"
          required
          readOnly={Boolean(institution.id)}
        />
      </Grid>
      <Grid item xs={4}>
        <FormField
          controller={getController('numero')}
          label="Número"
          required
          readOnly={Boolean(institution.id)}
        />
      </Grid>
      <Grid item xs={2}>
        <FormField
          controller={getController('estado')}
          label="UF"
          options={STATES_LIST}
          required
          readOnly
        />
      </Grid>
      <Grid item xs={5}>
        <FormField
          controller={getController('cidade')}
          label="Cidade"
          required
          readOnly
        />
      </Grid>
      <Grid item xs={5}>
        <FormField
          controller={getController('bairro')}
          label="Bairro"
          required
          readOnly={Boolean(institution.id)}
        />
      </Grid>
    </Grid>

    {Boolean(institution.id) && (
      <Grid item xs={12} container>
        <Grid item xs={12}>
          <Typography variant="h6">Contatos</Typography>
        </Grid>
        <Grid item xs={12}>
          <ContactsList
            institutionId={institution.id as number}
            contacts={institution.contatos ?? []}
          />
        </Grid>
      </Grid>
    )}

    <Box className={classes.formActions}>
      {onCancel && (
        <Button
          startIcon={<CloseIcon />}
          variant={institution.id ? 'contained' : 'text'}
          onClick={() => onCancel()}
        >
          {institution.id ? 'Fechar' : 'Cancelar'}
        </Button>
      )}

      {!institution.id && (
        <Button
          disabled={isSaving}
          form={formId}
          startIcon={<SaveIcon />}
          type="submit"
        >
          Salvar
        </Button>
      )}
    </Box>
  </>
}

export default InstitutionForm
