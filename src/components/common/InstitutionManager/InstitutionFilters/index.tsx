import SearchIcon from '@mui/icons-material/Search'
import Box from '@mui/material/Box'
import { useState } from 'react'
import { useDebounce } from 'react-use'

import FormField from '@/common/FormField'
import { Instituicao } from '~/models'

import { useStyles } from './styles'

namespace InstitutionFilters {
  export interface Props {
    institutions: Instituicao[]
    debounce?: number | false
    defaultValue?: string
    onFilter: (languages: Instituicao[]) => void
  }
}

function InstitutionFilters({
  debounce = 500,
  defaultValue = '',
  institutions,
  onFilter,
}: InstitutionFilters.Props) {
  const classes = useStyles()
  const [searchText, setSearchText] = useState(defaultValue)
  const actualDebounceTime = debounce && debounce > 0 ? debounce : 0

  useDebounce(() => {
    onFilter(institutions.filter((institution) => {
      const pattern = new RegExp(searchText, 'i')
      const searchable = [
        institution.id,
        institution.nome,
        institution.cnpj,
        institution.cep,
        institution.endereco,
        institution.numero,
        institution.bairro,
        institution.cidade,
        institution.estado,
        institution.contatos?.map((c) => {
          return Object.values(c).join(' ')
        }).join(' ') ?? '',
      ].join(' ')

      return !searchText || pattern.test(searchable)
    }))
  }, actualDebounceTime, [institutions, searchText])

  return (
    <Box className={classes.root}>
      <FormField
        color="secondary"
        hiddenLabel
        placeholder="Pesquisar"
        size="small"
        endAdornment={<SearchIcon />}
        type="search"
        value={searchText}
        onChange={(event) => setSearchText(event.target.value)}
      />
    </Box>
  )
}

export default InstitutionFilters
