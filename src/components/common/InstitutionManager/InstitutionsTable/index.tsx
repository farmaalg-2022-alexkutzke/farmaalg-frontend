import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import ViewIcon from '@mui/icons-material/Visibility'
import Box from '@mui/material/Box'
import IconButton from '@mui/material/IconButton'

import DataTable from '@/common/DataTable'
import Menu from '@/common/Menu'
import { Instituicao } from '~/models'

import { useStyles } from './styles'

namespace InstitutionsTable {
  export interface Props {
    institutions: Instituicao[]
    loading?: boolean
    onView: (institution: Instituicao) => void
    onDelete?: (institution: Instituicao) => void
    onEdit?: (institution: Instituicao) => void
  }
}

function InstitutionsTable({
  institutions,
  loading,
  onDelete,
  onEdit,
  onView,
}: InstitutionsTable.Props) {
  const classes = useStyles()
  const columns: DataTable.Column[] = [
    { field: 'id', headerName: 'ID', width: 70, align: 'right' },
    { field: 'nome', headerName: 'Nome', flex: 1, minWidth: 200 },
    {
      field: 'location',
      headerName: 'Localização',
      flex: 1,
      renderCell: ({ row }) => `${row.bairro}, ${row.cidade}, ${row.estado}`,
    },
    {
      field: ' ',
      sortable: false,
      align: 'center',
      width: (onDelete || onEdit) ? 120 : 56,
      renderCell({ row }) {
        const menuItems: Menu.ItemProps[] = []
        onEdit && menuItems.push({ label: 'Editar', icon: <EditIcon />, onClick: () => onEdit(row) })
        onDelete && menuItems.push({ label: 'Excluir', icon: <DeleteIcon />, onClick: () => onDelete(row) })

        return (
          <Box className={classes.recordActions}>
            <IconButton size="small" onClick={() => onView(row)}>
              <ViewIcon />
            </IconButton>
            <Menu id={`institution-${row.id}`} items={menuItems} />
          </Box>
        )
      },
    },
  ]

  return (
    <Box className={classes.root}>
      <DataTable
        columns={columns}
        loading={loading}
        rows={institutions}
      />
    </Box>
  )
}

export default InstitutionsTable
