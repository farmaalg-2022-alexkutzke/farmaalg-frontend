import AddIcon from '@mui/icons-material/Add'
import Box from '@mui/material/Box'
import { useState } from 'react'

import Button from '@/common/Button'
import InstitutionForm from '@/common/InstitutionForm'
import Modal from '@/common/Modal'
import { useInstitutionContext } from '~/helpers/hooks'
import { Instituicao, InstituicaoDTO } from '~/models'

import InstitutionFilters from './InstitutionFilters'
import InstitutionsTable from './InstitutionsTable'
import { useStyles } from './styles'

namespace InstitutionManager {
  export interface Props {
    institutions: Instituicao[]
    canCreate?: boolean
    canDelete?: boolean
    canUpdate?: boolean
    filterable?: boolean
  }
}

function InstitutionManager({
  canCreate,
  canDelete,
  canUpdate,
  filterable,
  institutions,
}: InstitutionManager.Props) {
  const classes = useStyles()
  const institutionContext = useInstitutionContext()
  const [isFormOpen, setFormOpen] = useState(false)
  const [focusedInstitution, setFocusedInstitution] = useState<Partial<Instituicao>>({})
  const [visibleLanguages, setVisibleInstitutions] = useState(institutions)
  const formTitle = focusedInstitution?.nome
    ? focusedInstitution.nome
    : 'Nova Instituição'

  function handleFormOpen(language: Partial<Instituicao>) {
    setFocusedInstitution(language)
    setFormOpen(true)
  }

  function handleFormClose() {
    // Don't use "setFocusedLanguage(...)" because
    // it will remove data from the modal before it closes
    setFormOpen(false)
  }

  async function handleSubmit(data: InstituicaoDTO) {
    focusedInstitution?.id
      ? await institutionContext.update(focusedInstitution.id, data)
      : await institutionContext.create(data)
    handleFormClose()
  }

  function handleDelete(institution: Instituicao) {
    if (institution.id && confirm(`Proceder com a exclusão da instituição "${institution.nome}" e todos os dados associados a ela?`)) {
      institutionContext.destroy(institution.id)
    }
  }

  return <>
    <Box className={classes.upperControls}>
      <Box>{filterable && (
        <InstitutionFilters institutions={institutions} onFilter={setVisibleInstitutions} />
      )}</Box>

      <Box>{canCreate && (
        <Button startIcon={<AddIcon />} onClick={() => handleFormOpen({})}>
          Nova Linguagem
        </Button>
      )}</Box>
    </Box>

    <InstitutionsTable
      institutions={visibleLanguages}
      onDelete={canDelete ? handleDelete : undefined}
      onEdit={canUpdate ? handleFormOpen : undefined}
      onView={handleFormOpen}
    />

    <Modal
      closable
      fullWidth
      disabled={institutionContext.isSaving}
      keepMounted
      open={isFormOpen}
      title={formTitle}
      onClose={handleFormClose}
    >
      <InstitutionForm
        institution={focusedInstitution}
        onCancel={handleFormClose}
        onSave={handleSubmit}
      />
    </Modal>
  </>
}

export default InstitutionManager
