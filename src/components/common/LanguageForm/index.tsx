import CloseIcon from '@mui/icons-material/Close'
import SaveIcon from '@mui/icons-material/Save'
import Box from '@mui/material/Box'
import { useEffect } from 'react'
import * as Yup from 'yup'

import Button from '@/common/Button'
import FormField from '@/common/FormField'
import { useForm, useLanguageContext } from '~/helpers/hooks'
import { Linguagem, LinguagemDTO } from '~/models'

import { useStyles } from './styles'

namespace LanguageForm{
  export interface Props {
    language: Partial<Linguagem>
    onSave: (language: LinguagemDTO) => void
    onCancel?: () => void
  }
}

const manageLanguageValSchema = Yup.object().shape({
  nome: Yup.string().required().max(50),
  codigoSandbox: Yup.string().required().max(50),
  exemplo: Yup.string().optional().nullable().max(255),
}).required()

function LanguageForm({
  language,
  onCancel,
  onSave,
}: LanguageForm.Props) {
  const classes = useStyles()
  const { isSaving } = useLanguageContext()
  const { Form, getController, setValue } = useForm<LinguagemDTO>({
    valSchema: manageLanguageValSchema,
  })

  useEffect(() => {
    setValue('nome', language.nome ?? '')
    setValue('codigoSandbox', language.codigoSandbox ?? '')
    setValue('exemplo', language.exemplo ?? '')
  }, [language]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Form onSubmit={onSave}>
      <FormField
        controller={getController('nome')}
        label="Nome visível da linguagem"
        required
      />
      <FormField
        controller={getController('codigoSandbox')}
        label="Nome da linguagem no Sandbox"
        required
      />
      <FormField
        controller={getController('exemplo')}
        className={classes.monospace}
        label="Código inicial na resolução dos exercícios"
        placeholder={'// Por exemplo, em C:\n#include<stdio.h>\nvoid main() {\n  printf("Hello, world!");\n}'}
        multiline
        minRows={8}
      />

      <Box className={classes.formActions}>
        {onCancel && (
          <Button
            startIcon={<CloseIcon />}
            variant="text"
            onClick={() => onCancel()}
          >
            Cancelar
          </Button>
        )}

        <Button
          disabled={isSaving}
          startIcon={<SaveIcon />}
          type="submit"
        >
          Salvar
        </Button>
      </Box>
    </Form>
  )
}

export default LanguageForm
