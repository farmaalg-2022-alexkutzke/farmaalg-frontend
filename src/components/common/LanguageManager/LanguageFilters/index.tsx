import SearchIcon from '@mui/icons-material/Search'
import Box from '@mui/material/Box'
import { useState } from 'react'
import { useDebounce } from 'react-use'

import FormField from '@/common/FormField'
import { Linguagem } from '~/models'

import { useStyles } from './styles'

namespace LanguageFilters {
  export interface Props {
    languages: Linguagem[]
    debounce?: number | false
    defaultValue?: string
    onFilter: (languages: Linguagem[]) => void
  }
}

function LanguageFilters({
  debounce = 500,
  defaultValue = '',
  languages,
  onFilter,
}: LanguageFilters.Props) {
  const classes = useStyles()
  const [searchText, setSearchText] = useState(defaultValue)
  const actualDebounceTime = debounce && debounce > 0 ? debounce : 0

  useDebounce(() => {
    onFilter(languages.filter((lang) => {
      const pattern = new RegExp(searchText, 'i')
      const searchable = [lang.id, lang.nome, lang.codigoSandbox].join(' ')

      return !searchText || pattern.test(searchable)
    }))
  }, actualDebounceTime, [languages, searchText])

  return (
    <Box className={classes.root}>
      <FormField
        color="secondary"
        hiddenLabel
        placeholder="Pesquisar"
        size="small"
        endAdornment={<SearchIcon />}
        type="search"
        value={searchText}
        onChange={(event) => setSearchText(event.target.value)}
      />
    </Box>
  )
}

export default LanguageFilters
