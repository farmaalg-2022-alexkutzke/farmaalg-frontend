import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import Box from '@mui/material/Box'

import DataTable from '@/common/DataTable'
import Menu from '@/common/Menu'
import { Linguagem } from '~/models'

import { useStyles } from './styles'

namespace LanguagesTable {
  export interface Props {
    languages: Linguagem[]
    loading?: boolean
    onDelete?: (language: Linguagem) => void
    onEdit?: (language: Linguagem) => void
  }
}

function LanguagesTable({
  languages,
  loading,
  onDelete,
  onEdit,
}: LanguagesTable.Props) {
  const classes = useStyles()
  const columns: DataTable.Column[] = [
    {
      field: 'id',
      headerName: 'ID',
      align: 'center',
      width: 150,
      renderCell: (row) => String(row.id).padStart(3, '0'),
    },
    { field: 'nome', headerName: 'Nome', flex: 1, minWidth: 200 },
    {
      field: ' ',
      sortable: false,
      width: (onDelete || onEdit) ? 56 : 0,
      renderCell({ row }) {
        const menuItems: Menu.ItemProps[] = []
        onEdit && menuItems.push({ label: 'Editar', icon: <EditIcon />, onClick: () => onEdit(row) })
        onDelete && menuItems.push({ label: 'Excluir', icon: <DeleteIcon />, onClick: () => onDelete(row) })

        return menuItems.length > 0 && (
          <Box className={classes.recordActions}>
            <Menu id={`language-${row.id}`} items={menuItems} />
          </Box>
        )
      },
    },
  ]

  return (
    <Box className={classes.root}>
      <DataTable
        columns={columns}
        loading={loading}
        rows={languages}
      />
    </Box>
  )
}

export default LanguagesTable
