import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(1),
  },

  recordActions: {
    display: 'flex',
    alignItems: 'center',
    gap: theme.spacing(2),

    '& button': {
      opacity: 0.6,
      backgroundColor: 'transparent',
      transition: 'color 200ms, opacity 200ms',

      '&:hover': {
        opacity: 1,
        backgroundColor: 'transparent',
        color: theme.palette.secondary.main,
      },
    },
  },
}))
