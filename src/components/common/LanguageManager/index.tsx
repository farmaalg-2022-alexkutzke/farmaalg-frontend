import AddIcon from '@mui/icons-material/Add'
import Box from '@mui/material/Box'
import { useState } from 'react'

import Button from '@/common/Button'
import LanguageForm from '@/common/LanguageForm'
import Modal from '@/common/Modal'
import { useLanguageContext } from '~/helpers/hooks'
import { Linguagem, LinguagemDTO } from '~/models'

import LanguageFilters from './LanguageFilters'
import LanguagesTable from './LanguagesTable'
import { useStyles } from './styles'

namespace LanguageManager {
  export interface Props {
    languages: Linguagem[]
    canCreate?: boolean
    canDelete?: boolean
    canUpdate?: boolean
    filterable?: boolean
  }
}

function LanguageManager({
  canCreate,
  canDelete,
  canUpdate,
  filterable,
  languages,
}: LanguageManager.Props) {
  const classes = useStyles()
  const languageContext = useLanguageContext()
  const [isFormOpen, setFormOpen] = useState(false)
  const [focusedLanguage, setFocusedLanguage] = useState<Partial<Linguagem>>({})
  const [visibleLanguages, setVisibleLanguages] = useState(languages)
  const formTitle = focusedLanguage?.id
    ? `Editando ${focusedLanguage.nome}`
    : 'Nova Linguagem de Programação'

  function handleFormOpen(language: Partial<Linguagem>) {
    setFocusedLanguage(language)
    setFormOpen(true)
  }

  function handleFormClose() {
    // Don't use "setFocusedLanguage(...)" because
    // it will remove data from the modal before it closes
    setFormOpen(false)
  }

  async function handleSubmit(data: LinguagemDTO) {
    focusedLanguage?.id
      ? await languageContext.update(focusedLanguage.id, data)
      : await languageContext.create(data)
    handleFormClose()
  }

  function handleDelete(language: Linguagem) {
    if (language.id && confirm(`Proceder com a exclusão da linguagem "${language.nome}"?`)) {
      languageContext.destroy(language.id)
    }
  }

  return <>
    <Box className={classes.upperControls}>
      <Box>{filterable && (
        <LanguageFilters languages={languages} onFilter={setVisibleLanguages} />
      )}</Box>

      <Box>{canCreate && (
        <Button startIcon={<AddIcon />} onClick={() => handleFormOpen({})}>
          Nova Linguagem
        </Button>
      )}</Box>
    </Box>

    <LanguagesTable
      languages={visibleLanguages}
      onDelete={canDelete ? handleDelete : undefined}
      onEdit={canUpdate ? handleFormOpen : undefined}
    />

    <Modal
      closable
      fullWidth
      disabled={languageContext.isSaving}
      keepMounted
      open={isFormOpen}
      title={formTitle}
      onClose={handleFormClose}
    >
      <LanguageForm
        language={focusedLanguage}
        onCancel={handleFormClose}
        onSave={handleSubmit}
      />
    </Modal>
  </>
}

export default LanguageManager
