import Box from '@mui/material/Box'
import CircularProgress from '@mui/material/CircularProgress'

import { useStyles } from './styles'

namespace LoadingOverlay {
  export interface Props {
    color?: 'primary' | 'secondary' | 'success' | 'error' | 'warning'
    size?: string | number
  }
}

function LoadingOverlay({ color = 'secondary', size = '6rem' }: LoadingOverlay.Props) {
  const classes = useStyles()

  return (
    <Box className={classes.loadingWrapper}>
      <CircularProgress color={color} size={size} />
    </Box>
  )
}

export default LoadingOverlay
