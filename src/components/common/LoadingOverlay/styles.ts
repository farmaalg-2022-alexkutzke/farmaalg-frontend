import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles({
  loadingWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    height: 480,
    minHeight: 'inherit',
    maxHeight: 'inherit',
    width: '100%',
  },
})
