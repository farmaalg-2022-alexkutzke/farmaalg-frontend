import clsx from 'clsx'
import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'

import './styles.scss'

namespace MarkdownRenderer {
  export interface Props {
    children: string
    className?: string
    title?: string
  }
}

function MarkdownRenderer({
  children,
  className,
  title,
}: MarkdownRenderer.Props) {
  return (
    <ReactMarkdown
      className={clsx('markdown-body', className)}
      remarkPlugins={[remarkGfm]}
    >
      {title ? `# ${title}\n\n${children}` : children}
    </ReactMarkdown>
  )
}

export default MarkdownRenderer
