import MenuIcon from '@mui/icons-material/MoreVert'
import IconButton from '@mui/material/IconButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import MuiMenu, { MenuProps } from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import { ReactNode, useState } from 'react'

namespace Menu {
  export interface ItemProps {
    label: string
    icon?: ReactNode
    onClick: () => void
  }

  export interface Props extends Omit<MenuProps, 'open'>{
    id: string
    items: ItemProps[]
    children?: ReactNode
  }
}

function Menu({
  children,
  id,
  items,
  ...props
}: Menu.Props) {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)

  return (
    <>
      <IconButton
        id={`actions-${id}`}
        onClick={(event) => setAnchorEl(event.currentTarget)}
        aria-controls={`menu-${id}`}
        aria-expanded={anchorEl ? 'true' : undefined}
        aria-haspopup="true"
        aria-label="opções"
      >
        {children ?? <MenuIcon />}
      </IconButton>
      <MuiMenu
        anchorEl={anchorEl}
        id={`menu-${id}`}
        MenuListProps={{ 'aria-labelledby': `actions-${id}` }}
        open={Boolean(anchorEl)}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        onClick={() => setAnchorEl(null)}
        onClose={() => setAnchorEl(null)}
        {...props}
      >
        {items.map(({ label, icon, onClick }) => (
          <MenuItem key={label} onClick={() => onClick()}>
            {icon && <ListItemIcon>{icon}</ListItemIcon>}
            <ListItemText>{label}</ListItemText>
          </MenuItem>
        ))}
      </MuiMenu>
    </>
  )
}

export default Menu
