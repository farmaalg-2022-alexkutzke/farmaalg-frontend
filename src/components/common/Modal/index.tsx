import CloseIcon from '@mui/icons-material/Close'
import Dialog, { DialogProps } from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import IconButton from '@mui/material/IconButton'
import clsx from 'clsx'
import { ReactNode } from 'react'

import Button from '@/common/Button'

import { useStyles } from './styles'

namespace Modal {
  export type ActionProps = {
    label: string
    color?: 'primary' | 'secondary'
    disabled?: boolean
    icon?: ReactNode
    variant?: 'text' | 'outlined' | 'contained'
  } & ({
    type: 'submit' | 'reset'
    form: string
    onClick?: never
  } | {
    type?: 'button'
    onClick: () => void
    form?: never
  })

  export interface Props extends DialogProps {
    children: ReactNode
    title: string
    actions?: ActionProps[]
    closable?: boolean
    disabled?: boolean
    onClose?: () => void
  }
}

function Modal({
  actions = [],
  children,
  className,
  closable,
  disabled,
  maxWidth = 'sm',
  title,
  onClose,
  ...props
}: Modal.Props) {
  const classes = useStyles()

  function handleClose() {
    !disabled && onClose?.()
  }

  return (
    <Dialog
      className={clsx(classes.root, className)}
      maxWidth={maxWidth}
      onClose={handleClose}
      {...props}
    >
      <DialogTitle className={classes.title}>
        {title}

        {closable && (
          <IconButton onClick={handleClose} aria-label="fechar">
            <CloseIcon />
          </IconButton>
        )}
      </DialogTitle>

      <DialogContent className={classes.content}>
        {children}
      </DialogContent>

      {actions.length > 0 && (
        <DialogActions className={classes.actions}>
          {actions.map(({ icon, label, ...buttonProps }) => (
            <Button
              key={label}
              disabled={disabled}
              startIcon={icon}
              {...buttonProps}
            >
              {label}
            </Button>
          ))}
        </DialogActions>
      )}
    </Dialog>
  )
}

export default Modal
