import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiPaper-root': {
      padding: theme.spacing(3),
    },
  },

  title: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    padding: theme.spacing(0, 0, 1),

    color: theme.palette.primary.dark,
    fontSize: '1.75rem',
    fontWeight: 700,

    '& button': {
      position: 'absolute',
      top: theme.spacing(2),
      right: theme.spacing(2),

      color: theme.palette.primary.dark,
    },
  },

  content: {
    marginTop: theme.spacing(3),
    padding: 0,
  },

  actions: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    gap: theme.spacing(2),

    borderTop: `1px solid ${theme.palette.divider}`,
    padding: theme.spacing(3, 0, 0),
  },
}))
