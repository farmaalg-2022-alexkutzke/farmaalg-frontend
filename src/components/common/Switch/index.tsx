import FormControl from '@mui/material/FormControl'
import ControlLabel from '@mui/material/FormControlLabel'
import FormHelperText from '@mui/material/FormHelperText'
import MuiSwitch, { SwitchProps } from '@mui/material/Switch'
import clsx from 'clsx'
import { Control, Controller } from 'react-hook-form'

import { useStyles } from './styles'

namespace Switch {
  export interface Props<T = any> extends SwitchProps {
    label: string
    controller?: {
      control: Control<T>
      name: string
      defaultValue?: any
    }
    error?: boolean
    helperText?: string
  }
}

function Switch({
  color = 'secondary',
  controller,
  disabled,
  error,
  helperText,
  label,
  ...props
}: Switch.Props) {
  const classes = useStyles()

  return (
    <FormControl
      disabled={disabled}
      error={error}
      className={clsx(classes.root, props.className)}
    >
      <ControlLabel
        label={label}
        className={classes.label}
        control={controller ? (
          <Controller
            control={controller.control}
            defaultValue={controller.defaultValue}
            name={controller.name}
            render={({ field }) => (
              <MuiSwitch color={color} {...props} {...field} />
            )}
          />
        ) : (
          <MuiSwitch color={color} {...props} />
        )}
      />
      <FormHelperText error={error}>
        {helperText}
      </FormHelperText>
    </FormControl>
  )
}

export default Switch
