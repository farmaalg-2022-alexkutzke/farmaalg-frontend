import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: { },

  label: {
    color: theme.palette.primary.main,
    fontWeight: 500,
    fontStyle: 'italic',
  },
}))
