import CloseIcon from '@mui/icons-material/Close'
import SaveIcon from '@mui/icons-material/Save'
import Grid from '@mui/material/Grid'
import { useEffect } from 'react'
import * as Yup from 'yup'

import Button from '@/common/Button'
import FormField from '@/common/FormField'
import { useForm, useTestCaseContext } from '~/helpers/hooks'
import { CasoTeste, CasoTesteDTO } from '~/models'

import { useStyles } from './styles'

namespace TestCaseForm{
  export interface Props {
    testCase: Partial<CasoTeste>
    onSave: (testCase: CasoTesteDTO) => void
    onCancel?: () => void
  }
}

const manageTestCaseValSchema = Yup.object().shape({
  titulo: Yup.string().required().max(100),
  input: Yup.string().optional().nullable().max(200),
  output: Yup.string().required(),
}).required()

function TestCaseForm({
  testCase,
  onCancel,
  onSave,
}: TestCaseForm.Props) {
  const classes = useStyles()
  const { isSaving } = useTestCaseContext()
  const { Form, getController, setValue } = useForm<CasoTesteDTO>({
    valSchema: manageTestCaseValSchema,
  })

  function handleSubmit(data: CasoTesteDTO) {
    if (data.input && data.input.slice(-1) !== '\n') {
      if (!confirm('O valor da entrada não termina com uma quebra de linha. Deseja continuar?')) {
        return
      }
    }
    if (data.output && data.output.slice(-1) !== '\n') {
      if (!confirm('O valor da saída não termina com uma quebra de linha. Deseja continuar?')) {
        return
      }
    }
    onSave(data)
  }

  useEffect(() => {
    setValue('exercicio', testCase.exercicio?.id as any)
    setValue('titulo', testCase.titulo ?? '')
    setValue('input', testCase.input ?? '')
    setValue('output', testCase.output ?? '')
  }, [testCase]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Grid
      container
      component={Form}
      onSubmit={handleSubmit}
    >
      <Grid item xs={12}>
        <FormField
          controller={getController('titulo')}
          label="Nome do caso de teste"
          required
        />
      </Grid>
      <Grid
        item
        xs={12}
        container
        columnSpacing={2}
      >
        <Grid item xs={6}>
          <FormField
            className={classes.monospace}
            controller={getController('input')}
            label="Entradas (opcional)"
            multiline
            minRows={8}
          />
        </Grid>
        <Grid item xs={6}>
          <FormField
            className={classes.monospace}
            controller={getController('output')}
            label="Saídas esperadas"
            multiline
            minRows={8}
          />
        </Grid>
      </Grid>

      <Grid item xs={12} className={classes.formActions}>
        {onCancel && (
          <Button
            startIcon={<CloseIcon />}
            variant="text"
            onClick={() => onCancel()}
          >
            Cancelar
          </Button>
        )}

        <Button
          disabled={isSaving}
          startIcon={<SaveIcon />}
          type="submit"
        >
          Salvar
        </Button>
      </Grid>
    </Grid>
  )
}

export default TestCaseForm
