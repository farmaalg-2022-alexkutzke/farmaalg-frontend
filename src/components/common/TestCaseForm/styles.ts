import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  monospace: {
    '& input, & textarea': {
      color: theme.palette.grey[900],
      fontFamily: '"JetBrains Mono", Consolas, Monaco, Andale Mono, monospace',
      fontSize: '1.2rem',
      fontWeight: 600,
      lineHeight: 1.2,
    },
  },

  formActions: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    gap: theme.spacing(2),

    marginTop: theme.spacing(2),
    borderTopColor: theme.palette.divider,
    borderTopStyle: 'solid',
    borderTopWidth: 1,
    paddingTop: theme.spacing(2),
  },
}))
