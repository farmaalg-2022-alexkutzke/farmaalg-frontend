import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import { Typography } from '@mui/material'
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'

import LoadingOverlay from '@/common/LoadingOverlay'
import MarkdownRenderer from '@/common/MarkdownRenderer'
import Menu from '@/common/Menu'
import { CasoTeste } from '~/models'

import { useStyles } from './styles'

namespace TestCasesList {
  export interface Props {
    testCases: CasoTeste[]
    loading?: boolean
    onDelete?: (testCase: CasoTeste) => void
    onEdit?: (testCase: CasoTeste) => void
  }
}

function TestCasesList({
  testCases,
  loading,
  onDelete,
  onEdit,
}: TestCasesList.Props) {
  const classes = useStyles()

  if (loading || !testCases) {
    return <LoadingOverlay />
  }

  return (
    <Box className={classes.root}>
      {testCases.length === 0 ? (
        <Typography variant="subtitle1">
          Nenhum caso de teste definido ainda.
        </Typography>
      ) : (
        testCases.map((test) => {
          const menuItems: Menu.ItemProps[] = []
          onEdit && menuItems.push({ label: 'Editar', icon: <EditIcon />, onClick: () => onEdit(test) })
          onDelete && menuItems.push({ label: 'Excluir', icon: <DeleteIcon />, onClick: () => onDelete(test) })

          return (
            <Grid
              key={test.id}
              container
              columnSpacing={2}
              className={classes.item}
            >
              <Grid item xs={12} className={classes.itemHeader}>
                <Typography variant="h6">{test.titulo}</Typography>
                {menuItems.length > 0 && (
                  <Menu id={`testcase-${test.id}`} items={menuItems} />
                )}
              </Grid>
              <Grid item xs={6}>
                {test.input ? (
                  <MarkdownRenderer>
                    {`\`\`\`\n${test.input}\n\`\`\`\n`}
                  </MarkdownRenderer>
                ) : (
                  <Typography className={classes.noInputTest}>
                    Nenhuma entrada
                  </Typography>
                )}
              </Grid>
              <Grid item xs={6}>
                <MarkdownRenderer>
                  {`\`\`\`\n${test.output}\n\`\`\`\n`}
                </MarkdownRenderer>
              </Grid>
            </Grid>
          )
        })
      )}
    </Box>
  )
}

export default TestCasesList
