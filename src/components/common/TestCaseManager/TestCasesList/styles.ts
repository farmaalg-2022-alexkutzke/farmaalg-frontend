import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(0, 0, 1),
  },

  item: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    paddingTop: theme.spacing(1),

    '& .markdown-body': {
      padding: theme.spacing(1, 0),
    },
  },

  itemHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',

    '& h6': {
      fontWeight: 700,
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },

    '& button': {
      opacity: 0.6,
      backgroundColor: 'transparent',
      transition: 'color 200ms, opacity 200ms',

      '&:hover': {
        opacity: 1,
        backgroundColor: 'transparent',
        color: theme.palette.secondary.main,
      },
    },
  },

  noInputTest: {
    paddingTop: theme.spacing(2),

    color: theme.palette.grey[500],
    fontStyle: 'italic',
  },
}))
