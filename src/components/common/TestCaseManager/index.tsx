import AddIcon from '@mui/icons-material/Add'
import Box from '@mui/material/Box'
import { useState } from 'react'

import Button from '@/common/Button'
import Modal from '@/common/Modal'
import TestCaseForm from '@/common/TestCaseForm'
import { useTestCaseContext } from '~/helpers/hooks'
import { CasoTeste, CasoTesteDTO } from '~/models'

import { useStyles } from './styles'
import TestCasesList from './TestCasesList'

namespace TestCaseManager {
  export interface Props {
    testCases: CasoTeste[]
    canCreate?: boolean
    canDelete?: boolean
    canUpdate?: boolean
  }
}

function TestCaseManager({
  canCreate,
  canDelete,
  canUpdate,
  testCases,
}: TestCaseManager.Props) {
  const classes = useStyles()
  const testCaseContext = useTestCaseContext()
  const [isFormOpen, setFormOpen] = useState(false)
  const [focusedTestCase, setFocusedTestCase] = useState<Partial<CasoTeste>>({})
  const formTitle = focusedTestCase?.id
    ? `Editando ${focusedTestCase.titulo}`
    : 'Novo Caso de Teste'

  function handleFormOpen(testCase: Partial<CasoTeste>) {
    setFocusedTestCase(testCase)
    setFormOpen(true)
  }

  function handleFormClose() {
    // Don't use "setFocusedTestCase(...)" because
    // it will remove data from the modal before it closes
    setFormOpen(false)
  }

  async function handleSubmit(data: CasoTesteDTO) {
    focusedTestCase?.id
      ? await testCaseContext.update(focusedTestCase.id, data)
      : await testCaseContext.create(data)
    handleFormClose()
  }

  function handleDelete(testCase: CasoTeste) {
    if (testCase.id && confirm(`Proceder com a exclusão do caso de teste "${testCase.titulo}"?`)) {
      testCaseContext.destroy(testCase.id)
    }
  }

  return (
    <Box className={classes.root}>
      <TestCasesList
        testCases={testCases}
        onDelete={canDelete ? handleDelete : undefined}
        onEdit={canUpdate ? handleFormOpen : undefined}
      />

      {canCreate && (
        <Button
          className={classes.createTestCaseButton}
          startIcon={<AddIcon />}
          onClick={() => handleFormOpen({})}
        >
          Novo Caso de Teste
        </Button>
      )}

      <Modal
        closable
        fullWidth
        disabled={testCaseContext.isSaving}
        keepMounted
        open={isFormOpen}
        title={formTitle}
        onClose={handleFormClose}
      >
        <TestCaseForm
          testCase={focusedTestCase}
          onCancel={handleFormClose}
          onSave={handleSubmit}
        />
      </Modal>
    </Box>
  )
}

export default TestCaseManager
