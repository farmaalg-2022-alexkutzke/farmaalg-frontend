import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2, 3),
  },

  createTestCaseButton: {
    width: `calc(100% - ${theme.spacing(10)})`,
    margin: theme.spacing(2, 4, 3),
  },
}))
