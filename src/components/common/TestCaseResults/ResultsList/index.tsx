import { Typography } from '@mui/material'
import Box from '@mui/material/Box'
import Chip from '@mui/material/Chip'
import Grid from '@mui/material/Grid'

import LoadingOverlay from '@/common/LoadingOverlay'
import MarkdownRenderer from '@/common/MarkdownRenderer'
import { ExecucaoCasoTeste } from '~/models'

import { useStyles } from './styles'

namespace ResultsList {
  export interface Props {
    testRuns: ExecucaoCasoTeste[]
    loading?: boolean
  }
}

function ResultsList({ loading, testRuns }: ResultsList.Props) {
  const classes = useStyles()

  if (loading || !testRuns) {
    return <LoadingOverlay />
  }

  return (
    <Box className={classes.root}>
      {testRuns.length === 0 ? (
        <Typography variant="subtitle1">
          Nenhum caso de teste executado ainda.
        </Typography>
      ) : (
        testRuns.map((test) => (
          <Grid key={test.id} container className={classes.item}>
            <Grid item xs={12} className={classes.itemHeader}>
              <Typography variant="h6">{test.titulo}</Typography>
              {test.estahCorreto ? (
                <Chip color="success" label="Passou" size="small" />
              ) : (
                <Chip color="error" label="Falhou" size="small" />
              )}
            </Grid>
            <Grid item xs={12}>
              <Typography variant="body2">Saída esperada</Typography>
              <MarkdownRenderer>
                {`\`\`\`\n${test.output}\n\`\`\`\n`}
              </MarkdownRenderer>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="body2">Saída obtida</Typography>
              <MarkdownRenderer>
                {`\`\`\`\n${test.outputObtido}\n\`\`\`\n`}
              </MarkdownRenderer>
            </Grid>
          </Grid>
        ))
      )}
    </Box>
  )
}

export default ResultsList
