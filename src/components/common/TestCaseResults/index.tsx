import Box from '@mui/material/Box'

import { useTestCaseContext } from '~/helpers/hooks'
import { ExecucaoCasoTeste } from '~/models'

import ResultsList from './ResultsList'
import { useStyles } from './styles'

namespace TestCaseResults {
  export interface Props {
    testRuns: ExecucaoCasoTeste[]
  }
}

function TestCaseResults({ testRuns }: TestCaseResults.Props) {
  const classes = useStyles()
  const { isLoading } = useTestCaseContext()

  return (
    <Box className={classes.root}>
      <ResultsList
        testRuns={testRuns}
        loading={isLoading}
      />
    </Box>
  )
}

export default TestCaseResults
