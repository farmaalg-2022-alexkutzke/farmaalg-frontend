import CloseIcon from '@mui/icons-material/Close'
import SaveIcon from '@mui/icons-material/Save'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import clsx from 'clsx'
import * as Yup from 'yup'

import AvatarField from '@/common/AvatarField'
import Button from '@/common/Button'
import FormField from '@/common/FormField'
import { isAdmin, isStudent, isTeacher } from '~/helpers'
import { useForm, useFormMeta, useUserContext } from '~/helpers/hooks'
import { Perfil, Usuario, UsuarioDTO } from '~/models'

import { useStyles } from '../styles'

namespace GeneralDataForm {
  export interface Props {
    role: Perfil
    user: Partial<Usuario>
    hideTitle?: boolean
    onSave: (user: UsuarioDTO) => void
    onCancel?: () => void
  }
}

const sharedValObject = {
  id: Yup.number().optional(),
  nome: Yup.string().required().max(100),
  email: Yup.string().required().email().max(50),
  avatar: Yup.string().required().url().max(255),
  senha: Yup.string().when('id', {
    is: (id?: number) => Boolean(id),
    then: Yup.string().optional(),
    otherwise: Yup.string().required().min(8),
  }),
  confirmarSenha: Yup.string()
    .oneOf([Yup.ref('senha')], 'As senhas não conferem'),
}

const specificValSchema: Record<Perfil, Yup.AnyObjectSchema> = {
  [Perfil.ADMIN]: Yup.object().shape({
    telefone: Yup.string().required().min(10).max(15),
    ...sharedValObject,
  }),
  [Perfil.ALUNO]: Yup.object().shape({
    instituicao: Yup.object({
      id: Yup.number().required().integer().positive(),
    }),
    idInstitucional: Yup.string().required().max(25),
    ...sharedValObject,
  }),
  [Perfil.PROFESSOR]: Yup.object().shape({
    telefone: Yup.string().required().min(10).max(11),
    instituicao: Yup.object({
      id: Yup.number().required().integer().positive(),
    }),
    idInstitucional: Yup.string().required(),
    ...sharedValObject,
  }),
}

function GeneralDataForm({
  hideTitle,
  role,
  user,
  onCancel,
  onSave,
}: GeneralDataForm.Props) {
  const classes = useStyles()
  const { isSaving } = useUserContext()
  const { institutionsCombobox } = useFormMeta()
  const { Form, getController } = useForm<UsuarioDTO>({
    defaultValues: { id: user.id, perfil: role },
    valSchema: specificValSchema[role],
  })

  return (
    <Grid
      container
      columnSpacing={2}
      component={Form}
      onSubmit={onSave}
    >
      {!hideTitle && user.id && (
        <Grid item xs={12}>
          <Typography className={classes.sectionTitle} variant="h5">
            Dados Gerais
          </Typography>
        </Grid>
      )}
      <Grid item xs={12}>
        <FormField
          autoComplete="name"
          controller={getController('nome', user.nome)}
          label="Nome completo"
          required
        />
      </Grid>
      <Grid item xs={(isAdmin(role) || isTeacher(role)) ? 6 : 12}>
        <FormField
          autoComplete="email"
          controller={getController('email', user.email)}
          label="Endereço de email"
          required
        />
      </Grid>
      {(isAdmin(role) || isTeacher(role)) && (
        <Grid item xs={6}>
          <FormField
            autoComplete="tel"
            controller={getController('telefone', user.telefone)}
            label="Telefone para contato"
            required
          />
        </Grid>
      )}
      {(isStudent(role) || isTeacher(role)) && (
        <>
          <Grid item xs={12}>
            <FormField
              controller={getController('instituicao.id', user.instituicao?.id)}
              label="Instituição de ensino"
              options={institutionsCombobox.map((institution) => ({
                label: institution.nome,
                value: institution.id,
              }))}
              required
            />
          </Grid>
          <Grid item xs={12}>
            <FormField
              controller={getController('idInstitucional', user.idInstitucional)}
              label={isStudent(role) ? 'GRR' : 'Matrícula institucional'}
              required
            />
          </Grid>
        </>
      )}
      <Grid item xs={12}>
        <AvatarField
          controller={getController('avatar', user.avatar)}
          generatorButton
          label="URL para avatar"
          required
        />
      </Grid>
      {!user.id && <>
        <Grid item xs={12}>
          <FormField
            autoComplete="new-password"
            controller={getController('senha')}
            label="Senha de acesso"
            required
            type="password"
          />
        </Grid>
        <Grid item xs={12}>
          <FormField
            autoComplete="new-password"
            controller={getController('confirmarSenha')}
            label="Confirmar senha de acesso"
            required
            type="password"
          />
        </Grid>
      </>}

      <Grid
        item
        xs={12}
        className={clsx(classes.formActions, {
          [classes.topDivider]: !user.id,
        })}
      >
        {onCancel && (
          <Button
            startIcon={<CloseIcon />}
            variant="text"
            onClick={() => onCancel()}
          >
            Cancelar
          </Button>
        )}

        <Button
          disabled={isSaving}
          startIcon={<SaveIcon />}
          type="submit"
        >
          Salvar
        </Button>
      </Grid>
    </Grid>
  )
}

export default GeneralDataForm
