import CloseIcon from '@mui/icons-material/Close'
import SaveIcon from '@mui/icons-material/Save'
import Box from '@mui/material/Box'
import Divider from '@mui/material/Divider'
import Typography from '@mui/material/Typography'
import * as Yup from 'yup'

import Button from '@/common/Button'
import FormField from '@/common/FormField'
import { useForm, useUserContext } from '~/helpers/hooks'
import { NovaSenhaDTO } from '~/models'

import { useStyles } from '../styles'

namespace PasswordForm {
  export interface Props {
    asAdmin?: boolean
    onSave: (data: NovaSenhaDTO) => void
    onCancel?: () => void
  }
}

const passwordAsOwnUserValSchema = Yup.object().shape({
  senhaAntiga: Yup.string().required(),
  senhaNova: Yup.string().required().min(8),
  confirmarSenha: Yup.string()
    .oneOf([Yup.ref('senhaNova')], 'As senhas não conferem'),
})

const passwordAsAdminValSchema = Yup.object().shape({
  senhaNova: Yup.string().required().min(8),
  confirmarSenha: Yup.string()
    .oneOf([Yup.ref('senhaNova')], 'As senhas não conferem'),
})

function PasswordForm({
  asAdmin,
  onCancel,
  onSave,
}: PasswordForm.Props) {
  const classes = useStyles()
  const { isSaving } = useUserContext()
  const { Form, getController, setValue } = useForm<NovaSenhaDTO>({
    valSchema: asAdmin
      ? passwordAsAdminValSchema
      : passwordAsOwnUserValSchema,
  })

  function handleSubmit(data: NovaSenhaDTO) {
    setValue('senhaAntiga', '')
    setValue('senhaNova', '')
    setValue('confirmarSenha', '')
    onSave(data)
  }

  return (
    <Form onSubmit={handleSubmit}>
      <Divider />
      <Typography className={classes.sectionTitle} variant="h5">
        Atualizar Senha
      </Typography>
      {!asAdmin && (
        <FormField
          autoComplete="password"
          controller={getController('senhaAntiga')}
          label="Senha atual"
          required
          type="password"
        />
      )}
      <FormField
        autoComplete="new-password"
        controller={getController('senhaNova')}
        label="Nova senha"
        required
        type="password"
      />
      <FormField
        autoComplete="new-password"
        controller={getController('confirmarSenha')}
        label="Repetir nova senha"
        required
        type="password"
      />

      <Box className={classes.formActions}>
        {onCancel && (
          <Button
            startIcon={<CloseIcon />}
            variant="text"
            onClick={() => onCancel()}
          >
            Cancelar
          </Button>
        )}

        <Button
          disabled={isSaving}
          startIcon={<SaveIcon />}
          type="submit"
        >
          Salvar
        </Button>
      </Box>
    </Form>
  )
}

export default PasswordForm
