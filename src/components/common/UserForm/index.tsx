import Box from '@mui/material/Box'
import Collapse from '@mui/material/Collapse'
import Divider from '@mui/material/Divider'
import { useState } from 'react'

import FormField from '@/common/FormField'
import { NovaSenhaDTO, Perfil, Usuario, UsuarioDTO } from '~/models'

import GeneralDataForm from './GeneralDataForm'
import PasswordForm from './PasswordForm'
import { useStyles } from './styles'

namespace UserForm {
  export interface Props {
    user: Partial<Usuario>
    asAdmin?: boolean
    onSave: (data: UsuarioDTO) => void
    onUpdatePassword: (data: NovaSenhaDTO) => void
    onCancel?: () => void
  }
}

function UserForm({
  asAdmin,
  user,
  onCancel,
  onSave,
  onUpdatePassword,
}: UserForm.Props) {
  const classes = useStyles()
  const [role, setRole] = useState<Perfil | ''>(() => user.perfil ?? '')

  return (
    <Box className={classes.root}>
      {asAdmin && (
        <FormField
          label="Perfil de acesso"
          options={Object.values(Perfil).map((perfil) => ({
            value: perfil,
            label: (
              <span style={{ textTransform: 'capitalize' }}>
                {perfil.toLowerCase()}
              </span>
            ),
          }))}
          readOnly={Boolean(user?.id)}
          required
          value={role}
          onChange={(event) => setRole(event.target.value as Perfil)}
        />
      )}

      <Collapse in={Boolean(role)}>
        {role && <>
          <GeneralDataForm
            hideTitle={asAdmin}
            role={role}
            user={user}
            onCancel={user.id ? undefined : onCancel}
            onSave={onSave}
          />
          {user.id && <>
            <Divider sx={{ mt: 2 }} />
            <PasswordForm
              asAdmin={asAdmin}
              onSave={onUpdatePassword}
            />
          </>}
        </>}
      </Collapse>
    </Box>
  )
}

export default UserForm
