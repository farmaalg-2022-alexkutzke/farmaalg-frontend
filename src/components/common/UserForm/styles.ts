import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {},

  sectionTitle: {
    padding: theme.spacing(2, 0),

    color: theme.palette.primary.main,
    fontSize: '1rem',
    fontWeight: 700,
  },

  formActions: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    gap: theme.spacing(2),

    paddingTop: theme.spacing(2),
  },

  topDivider: {
    marginTop: theme.spacing(2),
    borderTopColor: theme.palette.divider,
    borderTopStyle: 'solid',
    borderTopWidth: 1,
  },
}))
