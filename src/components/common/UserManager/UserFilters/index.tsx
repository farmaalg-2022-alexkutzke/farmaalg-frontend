import SearchIcon from '@mui/icons-material/Search'
import Box from '@mui/material/Box'
import { useState } from 'react'
import { useDebounce } from 'react-use'

import FormField from '@/common/FormField'
import { Usuario } from '~/models'

import { useStyles } from './styles'

namespace UserFilters {
  export interface Props {
    users: Usuario[]
    debounce?: number | false
    defaultValue?: string
    onFilter: (users: Usuario[]) => void
  }
}

function UserFilters({
  debounce = 500,
  defaultValue = '',
  users,
  onFilter,
}: UserFilters.Props) {
  const classes = useStyles()
  const [searchText, setSearchText] = useState(defaultValue)
  const actualDebounceTime = debounce && debounce > 0 ? debounce : 0

  useDebounce(() => {
    onFilter(users.filter((user) => {
      const pattern = new RegExp(searchText, 'i')
      const searchable = [
        user.id,
        user.nome,
        user.email,
        user.idInstitucional ?? '',
        user.instituicao?.nome ?? '',
        user.telefone ?? '',
      ].join(' ')

      return !searchText || pattern.test(searchable)
    }))
  }, actualDebounceTime, [users, searchText])

  return (
    <Box className={classes.root}>
      <FormField
        color="secondary"
        hiddenLabel
        placeholder="Pesquisar"
        size="small"
        endAdornment={<SearchIcon />}
        type="search"
        value={searchText}
        onChange={(event) => setSearchText(event.target.value)}
      />
    </Box>
  )
}

export default UserFilters
