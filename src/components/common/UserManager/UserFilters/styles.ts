import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles({
  root: {
    '& .MuiTextField-root': {
      width: 'auto',

      '& .MuiFilledInput-root': {
        width: 180,
        transition: 'width 200ms ease-in-out',

        '&.Mui-focused': {
          width: 240,
        },
      },
    },
  },
})
