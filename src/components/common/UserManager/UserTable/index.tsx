import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import Avatar from '@mui/material/Avatar'
import Box from '@mui/material/Box'
import Chip from '@mui/material/Chip'
import Typography from '@mui/material/Typography'

import DataTable from '@/common/DataTable'
import Menu from '@/common/Menu'
import { Perfil, Usuario } from '~/models'

import { useStyles } from './styles'

namespace UsersTable {
  export interface Props {
    users: Usuario[]
    loading?: boolean
    onDelete?: (user: Usuario) => void
    onEdit?: (user: Usuario) => void
  }
}

function UsersTable({
  users,
  loading,
  onDelete,
  onEdit,
}: UsersTable.Props) {
  const classes = useStyles()
  const columns: DataTable.Column[] = [
    { field: 'id', headerName: 'ID', width: 70, align: 'right' },
    {
      field: 'nome',
      headerName: 'Nome',
      flex: 1,
      minWidth: 300,
      renderCell: ({ row }) => (
        <Box className={classes.avatarWithName}>
          <Avatar src={row.avatar} alt={row.nome} />
          <Typography>{row.nome}</Typography>
        </Box>
      ),
    },
    { field: 'idInstitucional', headerName: 'GRR', flex: 1, align: 'center' },
    {
      field: 'perfil',
      headerName: 'Perfil',
      align: 'center',
      renderCell({ row }) {
        const color: any = {
          [Perfil.ADMIN]: 'error',
          [Perfil.ALUNO]: 'secondary',
          [Perfil.PROFESSOR]: 'primary',
        }

        return (
          <Chip
            label={row.perfil}
            size="small"
            color={color[row.perfil]}
          />
        )
      },
    },
    {
      field: ' ',
      sortable: false,
      width: (onDelete || onEdit) ? 56 : 0,
      renderCell({ row }) {
        const menuItems: Menu.ItemProps[] = []
        onEdit && menuItems.push({ label: 'Editar', icon: <EditIcon />, onClick: () => onEdit(row) })
        onDelete && menuItems.push({ label: 'Excluir', icon: <DeleteIcon />, onClick: () => onDelete(row) })

        return menuItems.length > 0 && (
          <Box className={classes.recordActions}>
            <Menu id={`user-${row.id}`} items={menuItems} />
          </Box>
        )
      },
    },
  ]

  return (
    <Box className={classes.root}>
      <DataTable
        columns={columns}
        loading={loading}
        rows={users}
      />
    </Box>
  )
}

export default UsersTable
