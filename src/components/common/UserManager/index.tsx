import AddIcon from '@mui/icons-material/Add'
import Box from '@mui/material/Box'
import { useState } from 'react'

import Button from '@/common/Button'
import Modal from '@/common/Modal'
import UserForm from '@/common/UserForm'
import { useAuth, useUserContext } from '~/helpers/hooks'
import { NovaSenhaDTO, Usuario, UsuarioDTO } from '~/models'

import { useStyles } from './styles'
import UserFilters from './UserFilters'
import UsersTable from './UserTable'

namespace UserManager {
  export interface Props {
    users: Usuario[]
    canCreate?: boolean
    canDelete?: boolean
    canUpdate?: boolean
    filterable?: boolean
  }
}

function UserManager({
  canCreate,
  canDelete,
  canUpdate,
  filterable,
  users,
}: UserManager.Props) {
  const classes = useStyles()
  const { user: currentUser } = useAuth()
  const userContext = useUserContext()
  const [isFormOpen, setFormOpen] = useState(false)
  const [visibleUsers, setVisibleUsers] = useState(users)
  const [focusedUser, setFocusedUser] = useState<Partial<Usuario>>({})
  const formTitle = focusedUser?.id ? `${focusedUser.nome}` : 'Novo Usuário'

  function handleFormOpen(user: Partial<Usuario>) {
    setFocusedUser(user)
    setFormOpen(true)
  }

  function handleFormClose() {
    // Don't use "setFocusedUser(...)" because
    // it will remove data from the modal before it closes
    setFormOpen(false)
  }

  async function handleDataSubmit<M extends UsuarioDTO>(data: M) {
    focusedUser.id
      ? await userContext.update(focusedUser.id, data)
      : await userContext.create(data)
    handleFormClose()
  }

  async function handlePasswordSubmit(data: NovaSenhaDTO) {
    focusedUser.id && await userContext.updatePassword({ id: focusedUser.id, ...data })
    handleFormClose()
  }

  function handleDelete<M extends Usuario>(user: M) {
    if (user.id && confirm(`Proceder com a exclusão de "${user.nome}" e todos os seus dados?`)) {
      userContext.destroy(user.id)
    }
  }

  return <>
    <Box className={classes.upperControls}>
      <Box>{filterable && (
        <UserFilters users={users} onFilter={setVisibleUsers} />
      )}</Box>

      <Box>{canCreate && (
        <Button startIcon={<AddIcon />} onClick={() => handleFormOpen({})}>
          Novo Usuario
        </Button>
      )}</Box>
    </Box>

    <UsersTable
      users={visibleUsers}
      onDelete={canDelete ? handleDelete : undefined}
      onEdit={canUpdate ? handleFormOpen : undefined}
    />

    <Modal
      closable
      fullWidth
      disabled={userContext.isSaving}
      open={isFormOpen}
      title={formTitle}
      onClose={handleFormClose}
    >
      <UserForm
        asAdmin={currentUser?.isAdmin()}
        user={focusedUser}
        onCancel={handleFormClose}
        onSave={handleDataSubmit}
        onUpdatePassword={handlePasswordSubmit}
      />
    </Modal>
  </>
}

export default UserManager
