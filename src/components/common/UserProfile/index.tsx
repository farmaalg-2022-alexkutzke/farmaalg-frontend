import EditIcon from '@mui/icons-material/Edit'
import Avatar from '@mui/material/Avatar'
import Box from '@mui/material/Box'
import Chip from '@mui/material/Chip'
import Typography from '@mui/material/Typography'

import Button from '@/common/Button'
import { Admin, Aluno, Perfil, Usuario } from '~/models'

import { useStyles } from './styles'

namespace UserProfile {
  export interface Props {
    user: Usuario
    onEdit?: () => void
  }
}

function UserProfile({ user, onEdit }: UserProfile.Props) {
  const classes = useStyles()
  const chipColor: any = {
    [Perfil.ADMIN]: 'error',
    [Perfil.ALUNO]: 'secondary',
    [Perfil.PROFESSOR]: 'primary',
  }

  return (
    <Box className={classes.root}>
      <Avatar
        className={classes.avatar}
        src={user.avatar}
        alt={`avatar de ${user.nome}`}
      />
      <Typography className={classes.name} component="h4">
        {user.nome}
        <Chip label={user.perfil} size="small" color={chipColor[user.perfil]} />
      </Typography>

      {onEdit && (
        <Button
          size="small"
          startIcon={<EditIcon />}
          variant="outlined"
          onClick={() => onEdit()}
        >
          Atualizar dados
        </Button>
      )}

      {user.email && (
        <Typography className={classes.labeled}>
          <Typography component="span">Endereço de e-mail</Typography>
          {user.email}
        </Typography>
      )}
      {(user as Admin).telefone && (
        <Typography className={classes.labeled}>
          <Typography component="span">Telefone para contato</Typography>
          {(user as Admin).telefone}
        </Typography>
      )}
      {(user as Aluno).instituicao && (
        <Typography className={classes.labeled}>
          <Typography component="span">Instituição de ensino</Typography>
          {(user as Aluno).instituicao.nome}<br />
          {(user as Aluno).instituicao.endereco},{' '}
          {(user as Aluno).instituicao.numero}<br />
          {(user as Aluno).instituicao.bairro},{' '}
          {(user as Aluno).instituicao.cidade},{' '}
          {(user as Aluno).instituicao.estado}
        </Typography>
      )}
    </Box>
  )
}

export default UserProfile
