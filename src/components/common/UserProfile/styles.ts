import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    gap: theme.spacing(3),

    width: '100%',
  },

  avatar: {
    height: theme.spacing(20),
    width: theme.spacing(20),
  },

  name: {
    display: 'flex',
    alignItems: 'center',
    gap: theme.spacing(1),

    color: theme.palette.primary.main,
    fontSize: '2rem',
    fontWeight: 700,
  },

  labeled: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

    color: theme.palette.primary.main,
    fontSize: '1.2rem',
    textAlign: 'center',

    '& > span': {
      color: theme.palette.secondary.main,
      fontSize: '0.65rem',
      fontWeight: 700,
      textTransform: 'uppercase',
    },
  },
}))
