import Box from '@mui/material/Box'
import Tab from '@mui/material/Tab'
import Tabs from '@mui/material/Tabs'
import { useState } from 'react'

import AppSettingsForm from '@/common/AppSettingsForm'
import LoadingOverlay from '@/common/LoadingOverlay'
import Modal from '@/common/Modal'
import UserForm from '@/common/UserForm'
import UserProfile from '@/common/UserProfile'
import { useAuth, useUserContext } from '~/helpers/hooks'
import { NovaSenhaDTO, UsuarioDTO } from '~/models'

import { useStyles } from './styles'

namespace UserSettings {
  export interface Props {
    open: boolean
    onClose: () => void
  }
}

function UserSettings({ open, onClose }: UserSettings.Props) {
  const classes = useStyles()
  const { user } = useAuth()
  const { update: updateData, updatePassword } = useUserContext()
  const [tabIndex, setTabIndex] = useState(0)

  function handleUpdateGeneralData(data: UsuarioDTO) {
    user?.id && updateData(user.id, data)
  }

  function handleUpdatePassword(data: NovaSenhaDTO) {
    updatePassword(data)
  }

  return (
    <Modal
      className={classes.root}
      closable
      fullWidth
      open={open}
      title="Configurações"
      onClose={onClose}
    >
      <Box className={classes.content}>
        <Tabs
          classes={{
            indicator: classes.tabIndicator,
            root: classes.tabSelector,
          }}
          value={tabIndex}
          variant="fullWidth"
          onChange={(_, value) => setTabIndex(value)}
          aria-label="full width tabs example"
        >
          <Tab label="Perfil" id="tab-control-0" aria-controls="tab-profile" />
          <Tab label="Gerenciar Conta" id="tab-control-1" aria-controls="tab-account" />
          <Tab label="Definições do App" id="tab-control-2" aria-controls="tab-settings" />
        </Tabs>

        {!user ? (
          <LoadingOverlay />
        ) : (
          <Box className={classes.tavView}>
            <Box
              hidden={tabIndex !== 0}
              id="tab-profile"
              role="tabpanel"
              aria-labelledby="tab-control-0"
            >
              <UserProfile user={user} onEdit={() => setTabIndex(1)} />
            </Box>
            <Box
              hidden={tabIndex !== 1}
              id="tab-account"
              role="tabpanel"
              aria-labelledby="tab-control-1"
            >
              <UserForm
                user={user}
                onSave={handleUpdateGeneralData}
                onUpdatePassword={handleUpdatePassword}
              />
            </Box>
            <Box
              hidden={tabIndex !== 2}
              id="tab-settings"
              role="tabpanel"
              aria-labelledby="tab-control-2"
            >
              <AppSettingsForm />
            </Box>
          </Box>
        )}
      </Box>
    </Modal>
  )
}

export default UserSettings
