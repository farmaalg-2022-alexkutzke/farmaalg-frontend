import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiDialog-container': {
      alignItems: 'start',
      // marginTop: theme.spacing(10),
    },
  },

  content: {
    marginBottom: theme.spacing(1),
  },

  tabSelector: {
    '& .MuiTab-root': {
      color: theme.palette.primary.light,
      fontWeight: 700,

      '&.Mui-selected': {
        color: theme.palette.primary.main,
      },
    },
  },

  tabIndicator: {
    height: 4,
    backgroundColor: theme.palette.secondary.light,
  },

  tavView: {
    marginTop: theme.spacing(2),
  },
}))
