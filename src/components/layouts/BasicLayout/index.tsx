import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import { Outlet } from 'react-router-dom'

import Header from '@/common/AppHeader'
import Footer from '@/common/AppFooter'
import { useStyles } from './styles'

namespace BasicLayout {
  export type Props = Header.Props
}

function BasicLayout({
  contained = false,
  ...headerProps
}: BasicLayout.Props) {
  const classes = useStyles()

  return (
    <Box className={classes.wrapper}>
      <Header contained={contained} {...headerProps} />
      <Box className={classes.content}>
        <Container component="main" maxWidth={contained && 'lg'}>
          <Outlet />
        </Container>
      </Box>
      <Footer />
    </Box>
  )
}

export default BasicLayout
