import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',

    '& > header, & > footer': {
      flexShrink: 0,
    },
  },

  content: {
    flex: '1 0 auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

    height: '100%',
    paddingTop: '4rem',

    '& > main': {
      flex: 1,
      padding: 0,
    },
  },
})
