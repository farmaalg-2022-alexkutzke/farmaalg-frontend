import Box from '@mui/material/Box'
import { Outlet } from 'react-router-dom'

import Header from '@/common/AppHeader'
import Footer from '@/common/AppFooter'
import Drawer from '@/common/AppDrawer'
import { useStyles } from './styles'

function PanelLayout() {
  const classes = useStyles()

  return (
    <Box className={classes.wrapper}>
      <Header drawer />
      <Drawer />
      <Box className={classes.content}>
        <Box component="main">
          <Outlet />
        </Box>
        <Footer />
      </Box>
    </Box>
  )
}

export default PanelLayout
