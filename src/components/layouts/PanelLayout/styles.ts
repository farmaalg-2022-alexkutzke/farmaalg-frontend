import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: 'flex',
    overflow: 'hidden',

    width: '100vw',
    maxWidth: '100%',
    height: '100vh',
    maxHeight: '100%',
  },

  content: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',

    maxHeight: '100%',
    paddingTop: '5rem',
    overflow: 'auto',

    '& main': {
      flex: '1 0 auto',
      width: '100%',
      padding: theme.spacing(4),
    },
  },
}))
