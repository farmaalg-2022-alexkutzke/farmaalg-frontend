import Grid from '@mui/material/Grid'
import Link from '@mui/material/Link'
import Paper from '@mui/material/Paper'
import Typography from '@mui/material/Typography'
import { MouseEvent, useState } from 'react'
import { useMatch, useNavigate } from 'react-router-dom'

import Button from '@/common/Button'
import Modal from '@/common/Modal'
import UserProfile from '@/common/UserProfile'
import { useAuth } from '~/helpers/hooks'
import { Turma } from '~/models'

import { useStyles } from './styles'

namespace AboutClassroom {
  export interface Props {
    classroom: Turma
  }
}

function AboutClassroom({ classroom }: AboutClassroom.Props) {
  const classes = useStyles()
  const { user } = useAuth()
  const navigate = useNavigate()
  const [open, setOpen] = useState(false)
  const isInExercises = useMatch('/turmas/:id/exercicios')
  const buttonLabel = isInExercises ? 'Lista de Alunos' : 'Lista de exercícios'

  function handleToggleNavigation() {
    isInExercises ? navigate('../alunos') : navigate('../exercicios')
  }

  function handleDisplayTeacherProfile(event: MouseEvent<HTMLAnchorElement>) {
    event.preventDefault()
    setOpen(true)
  }

  return (
    <>
      <Paper className={classes.root} elevation={0}>
        <Grid container spacing={2}>
          <Grid item xs={3}>
            <Typography component="label">Disciplina:</Typography>
          </Grid>
          <Grid item xs={8}>
            <Typography>{classroom.disciplina}</Typography>
          </Grid>
          <Grid item xs={3}>
            <Typography component="label">Professor:</Typography>
          </Grid>
          <Grid item xs={8}>
            <Typography component={Link} onClick={handleDisplayTeacherProfile}>
              {classroom.professor.nome}
            </Typography>
          </Grid>
          <Grid item xs={3}>
            <Typography component="label">Descrição:</Typography>
          </Grid>
          <Grid item xs={8}>
            <Typography>{classroom.descricao}</Typography>
          </Grid>
        </Grid>

        {(user?.isAdmin() || user?.isTeacher()) && (
          <Button variant="outlined" onClick={handleToggleNavigation}>
            {buttonLabel}
          </Button>
        )}
      </Paper>

      <Modal
        closable
        fullWidth
        maxWidth="xs"
        open={open}
        onClose={() => setOpen(false)}
        title="Perfil do Professor"
      >
        <UserProfile user={classroom.professor} />
      </Modal>
    </>
  )
}

export default AboutClassroom
