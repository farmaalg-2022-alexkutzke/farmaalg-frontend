import { alpha } from '@mui/material/styles'
import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    gap: theme.spacing(4),

    marginBottom: theme.spacing(3),
    backgroundColor: alpha(theme.palette.secondary.light, 0.2),
    padding: theme.spacing(2),

    '& > div:first-child': {
      flex: 1,
    },

    '& label': {
      fontWeight: 700,
    },

    '& a': {
      color: theme.palette.secondary.main,
      cursor: 'pointer',
    },
  },
}))
