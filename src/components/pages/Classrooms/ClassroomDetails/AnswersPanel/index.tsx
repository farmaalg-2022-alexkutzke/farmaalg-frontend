import SchoolIcon from '@mui/icons-material/School'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

import AnswerCard from '@/common/AnswerCard'
import Breadcrumbs from '@/common/Breadcrumbs'
import LoadingOverlay from '@/common/LoadingOverlay'
import UserProfile from '@/common/UserProfile'
import { useAuth, useAnswerContext, useClassroomContext } from '~/helpers/hooks'

import { useStyles } from '../../styles'
import About from '../AboutClassroom'

function AnswersPanel() {
  const classes = useStyles()
  const { user } = useAuth()
  const params = useParams<{ exerciseId: string, studentId: string }>()
  const exerciseId = Number(params.exerciseId)
  const studentId = Number(params.studentId) || user?.id
  const { focusedClassroom } = useClassroomContext()
  const answerContext = useAnswerContext()
  const crumbs = [
    { label: 'Minhas turmas', icon: SchoolIcon },
    { label: focusedClassroom?.nome ?? '' },
    { label: 'Exercícios' },
    { label: `ID ${exerciseId}` },
  ]
  params.studentId && crumbs.push(
    { label: 'Alunos' },
    { label: `ID ${studentId}` },
  )

  useEffect(() => {
    if (studentId && exerciseId) {
      answerContext.findByStudentAndExercise(studentId, exerciseId)
    }
  }, [exerciseId, studentId]) // eslint-disable-line react-hooks/exhaustive-deps

  if (answerContext.isLoading) {
    return <LoadingOverlay />
  }

  if (!answerContext.answers.length) {
    return (
      <Typography component="h3">
        Nenhuma resposta a este exercício.
      </Typography>
    )
  }

  return (
    <Box component="section">
      {focusedClassroom !== null && (
        <>
          <Breadcrumbs className={classes.breadcrumbs} crumbs={crumbs} />
          <About classroom={focusedClassroom} />
          <UserProfile user={answerContext.answers[0].aluno} />

          {answerContext.answers.map((answer) => (
            <AnswerCard key={answer.id} answer={answer} />
          ))}
        </>
      )}


    </Box>
  )
}

export default AnswersPanel
