import SchoolIcon from '@mui/icons-material/School'
import Box from '@mui/material/Box'

import Breadcrumbs from '@/common/Breadcrumbs'
import ExerciseManager from '@/common/ExerciseManager'
import { useAuth, useClassroomContext } from '~/helpers/hooks'

import { useStyles } from '../../styles'
import About from '../AboutClassroom'

function ExercisesListSubpage() {
  const classes = useStyles()
  const { user } = useAuth()
  const { focusedClassroom } = useClassroomContext()

  return (
    <Box component="section">
      {focusedClassroom !== null && (
        <>
          <Breadcrumbs
            className={classes.breadcrumbs}
            crumbs={[
              { label: 'Minhas turmas', icon: SchoolIcon },
              { label: focusedClassroom.nome },
              { label: 'Exercícios' },
            ]}
          />

          <About classroom={focusedClassroom} />

          <ExerciseManager
            exercises={focusedClassroom.exercicios ?? []}
            canCreate={user?.isAdmin() || user?.isTeacher()}
            canDelete={user?.isAdmin() || user?.isTeacher()}
            canExpand
            canUpdate={user?.isAdmin() || user?.isTeacher()}
            filterable
          />
        </>
      )}


    </Box>
  )
}

export default ExercisesListSubpage
