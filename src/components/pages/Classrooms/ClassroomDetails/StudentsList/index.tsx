import SchoolIcon from '@mui/icons-material/School'
import Box from '@mui/material/Box'

import Breadcrumbs from '@/common/Breadcrumbs'
import UserManager from '@/common/UserManager'
import { useAuth, useClassroomContext } from '~/helpers/hooks'

import { useStyles } from '../../styles'
import About from '../AboutClassroom'

function StudentsListSubpage() {
  const classes = useStyles()
  const { user } = useAuth()
  const { focusedClassroom } = useClassroomContext()

  return (
    <Box component="section">
      {focusedClassroom !== null && (
        <>
          <Breadcrumbs
            className={classes.breadcrumbs}
            crumbs={[
              { label: 'Minhas turmas', icon: SchoolIcon },
              { label: focusedClassroom.nome },
              { label: 'Exercícios' },
            ]}
          />

          <About classroom={focusedClassroom} />

          <UserManager
            users={focusedClassroom.alunos ?? []}
            canCreate={user?.isAdmin()}
            canDelete={user?.isAdmin()}
            canUpdate={user?.isAdmin()}
            filterable
          />
        </>
      )}
    </Box>
  )
}

export default StudentsListSubpage
