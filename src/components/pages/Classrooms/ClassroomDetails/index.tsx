import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import { useEffect } from 'react'
import { Outlet, useParams } from 'react-router-dom'

import LoadingOverlay from '@/common/LoadingOverlay'
import { useClassroomContext } from '~/helpers/hooks'

import { useStyles } from '../styles'

function ClassroomDetailsPage() {
  const classes = useStyles()
  const routeParams = useParams()
  const classroomId = Number(routeParams.classroomId)
  const { find, focusedClassroom, isFetching } = useClassroomContext()

  useEffect(() => {
    find(classroomId)
  }, [classroomId]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Container className={classes.root}>
      {isFetching || !focusedClassroom ? (
        <LoadingOverlay />
      ) : (
        <>
          <Typography component="h1">
            {focusedClassroom.nome}
          </Typography>

          <Outlet />
        </>
      )}
    </Container>
  )
}

export default ClassroomDetailsPage
