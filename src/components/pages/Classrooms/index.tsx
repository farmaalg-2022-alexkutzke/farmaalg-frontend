import SchoolIcon from '@mui/icons-material/School'
import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'

import Breadcrumbs from '@/common/Breadcrumbs'
import ClassroomManager from '@/common/ClassroomManager'
import LoadingOverlay from '@/common/LoadingOverlay'
import { useAuth, useClassroomContext } from '~/helpers/hooks'

import { useStyles } from './styles'

function ClassroomsPage() {
  const classes = useStyles()
  const { user } = useAuth()
  const { classrooms, isFetching } = useClassroomContext()

  return (
    <Container className={classes.root}>
      <Typography component="h1">
        Turmas
      </Typography>

      <Box component="section">
        {isFetching ? (
          <LoadingOverlay />
        ) : (
          <>
            <Breadcrumbs
              className={classes.breadcrumbs}
              crumbs={[
                { label: 'Minhas turmas', icon: SchoolIcon },
              ]}
            />

            <ClassroomManager
              classrooms={classrooms}
              canCreate={user?.isAdmin() || user?.isTeacher()}
              canDelete={user?.isAdmin() || user?.isTeacher()}
              canRegister={user?.isStudent()}
              canUpdate={user?.isAdmin() || user?.isTeacher()}
              filterable
            />
          </>
        )}
      </Box>
    </Container>
  )
}

export default ClassroomsPage
