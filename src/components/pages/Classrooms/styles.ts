import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    '& > h1': {
      color: theme.palette.primary.dark,
      fontSize: '2.5rem',
      fontWeight: 700,
    },

    '& > section': {
      minHeight: '50vh',
    },
  },

  breadcrumbs: {
    marginBottom: theme.spacing(4),
  },
}))
