import Box from '@mui/material/Box'
import Container from '@mui/material/Container'

import { useStyles } from './styles'

function DashboardPage() {
  const classes = useStyles()

  return (
    <Container className={classes.root}>
      <Box className={classes.illustration}>
        <img src="/assets/img/dashboard.svg" alt="dashboard" />
      </Box>
    </Container>
  )
}

export default DashboardPage
