import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    height: '100%',
  },

  illustration: {
    height: '60vh',
    marginTop: '10%',

    '& > img': {
      width: '100%',
      maxWidth: 720,
      height: 'auto',
    },
  },
})
