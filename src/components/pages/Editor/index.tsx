import Box from '@mui/material/Box'
import Tab from '@mui/material/Tab'
import Tabs from '@mui/material/Tabs'
import { useEffect, useState } from 'react'
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom'

import Button from '@/common/Button'
import CodeEditor from '@/common/CodeEditor'
import LoadingOverlay from '@/common/LoadingOverlay'
import MarkdownRenderer from '@/common/MarkdownRenderer'
import Modal from '@/common/Modal'
import TestCaseManager from '@/common/TestCaseManager'
import TestCaseResults from '@/common/TestCaseResults'
import {
  useAlert,
  useAuth,
  useAnswerContext,
  useExerciseContext,
  useTestCaseContext,
} from '~/helpers/hooks'
import { ExecucaoCasoTeste } from '~/models'

import { useStyles } from './styles'

function EditorPage() {
  const classes = useStyles()
  const { user } = useAuth()
  const navigate = useNavigate()
  const notification = useAlert()
  const answerContext = useAnswerContext()
  const testCaseContext = useTestCaseContext()
  const { find, focusedExercise } = useExerciseContext()
  const [finishedExercise, setFinishedExercise] = useState(false)
  const [tabIndex, setTabIndex] = useState(0)
  const [testRuns, setTestRuns] = useState<ExecucaoCasoTeste[]>([])
  const isUserAllowed = Boolean(user?.isAdmin() || user?.id === focusedExercise?.turma.professor.id)

  const fromRouteState = useLocation().state?.exerciseId
  const fromQueryString = useSearchParams()[0].get('exid')
  const exerciseId = Number(fromRouteState ?? fromQueryString)

  function handleTeacherTest({ code, langId }: CodeEditor.EditingData) {
    return Promise.all(testCaseContext.testCases.map((test) => {
      return testCaseContext.run(test.id, langId, code)
    }))
  }

  function handleStudentTest({ code, langId }: CodeEditor.EditingData) {
    return answerContext.test(langId, code)
  }

  async function handleTest({ code, langId }: CodeEditor.EditingData) {
    const results = user?.isStudent()
      ? await handleStudentTest({ code, langId })
      : await handleTeacherTest({ code, langId })
    const correct = results.reduce((acc, test) => (test.estahCorreto ? acc + 1 : acc), 0)
    switch (correct) {
      case 0:
        notification.warning('Todos os casos de teste falharam.')
        break
      case results.length:
        notification.success('Todos os casos de teste executaram corretamente.')
        break
      default:
        notification.warning(`Apenas ${correct} de ${results.length} executaram corretamente.`)
    }
    setTestRuns(results)
    setTabIndex(2)
  }

  async function handleSubmit({ code, langId }: CodeEditor.EditingData) {
    await answerContext.submit(langId, code)
    setFinishedExercise(true)
  }

  useEffect(() => {
    find(exerciseId)
  }, [exerciseId]) // eslint-disable-line react-hooks/exhaustive-deps

  if (!focusedExercise) {
    return <LoadingOverlay />
  }

  return (
    <Box className={classes.root}>
      <Box className={classes.information}>
        <Tabs
          classes={{
            indicator: classes.tabIndicator,
            root: classes.tabSelector,
          }}
          value={tabIndex}
          variant="fullWidth"
          onChange={(_, value) => setTabIndex(value)}
        >
          <Tab label="Enunciado" id="tab-control-0" aria-controls="tab-statement" />
          <Tab label="Casos de Teste" id="tab-control-1" aria-controls="tab-testcases" />
          <Tab label="Execuções" id="tab-control-2" aria-controls="tab-results" />
        </Tabs>

        <Box
          className={classes.tabPanel}
          hidden={tabIndex !== 0}
          id="tab-statement"
          role="tabpanel"
          aria-labelledby="tab-control-0"
        >
          <MarkdownRenderer title={focusedExercise.titulo}>
            {focusedExercise.enunciado}
          </MarkdownRenderer>
        </Box>
        <Box
          className={classes.tabPanel}
          hidden={tabIndex !== 1}
          id="tab-testcases"
          role="tabpanel"
          aria-labelledby="tab-control-1"
        >
          <TestCaseManager
            testCases={testCaseContext.testCases}
            canCreate={isUserAllowed}
            canDelete={isUserAllowed}
            canUpdate={isUserAllowed}
          />
        </Box>
        <Box
          className={classes.tabPanel}
          hidden={tabIndex !== 2}
          id="tab-results"
          role="tabpanel"
          aria-labelledby="tab-control-2"
        >
          <TestCaseResults testRuns={testRuns} />
        </Box>
      </Box>
      <Box className={classes.editor}>
        <CodeEditor
          languages={focusedExercise.linguagens}
          disabled={testCaseContext.isRunning || answerContext.isCreating}
          onTest={handleTest}
          onSubmit={user?.isStudent() ? handleSubmit : undefined}
        />
      </Box>

      <Modal
        className={classes.finishedModal}
        open={finishedExercise}
        title="Exercício finalizado"
      >
        <Button variant="outlined" onClick={() => navigate(-1)}>
          Voltar para Turma
        </Button>
      </Modal>
    </Box>
  )
}

export default EditorPage
