import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'grid',
    gridTemplateColumns: 'repeat(2, 1fr)',
    gridTemplateRows: '1fr',

    height: '82.55vh',
  },

  information: {
    overflowY: 'auto',
  },

  tabSelector: {
    marginTop: theme.spacing(2),
    borderBottom: `1px solid ${theme.palette.divider}`,

    '& .MuiTab-root': {
      color: theme.palette.primary.light,
      fontWeight: 700,

      '&.Mui-selected': {
        color: theme.palette.primary.main,
      },
    },
  },

  tabIndicator: {
    height: 4,
    backgroundColor: theme.palette.secondary.light,
  },

  tabPanel: {
    overflow: 'auto',
  },

  editor: {
    borderLeft: `2px solid ${theme.palette.grey[600]}`,
  },

  finishedModal: {
    '& .MuiBackdrop-root': {
      backgroundColor: 'rgb(200, 200, 200, 0.8)',
    },

    '& .MuiDialogContent-root': {
      display: 'flex',
      justifyContent: 'center',
      margin: theme.spacing(2, 0),
    },
  },
}))
