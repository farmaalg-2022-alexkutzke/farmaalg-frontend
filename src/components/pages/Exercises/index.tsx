import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import { useEffect } from 'react'

import ExerciseManager from '@/common/ExerciseManager'
import LoadingOverlay from '@/common/LoadingOverlay'
import { useAuth, useExerciseContext } from '~/helpers/hooks'

import { useStyles } from './styles'

function ExercisesPage() {
  const classes = useStyles()
  const { user } = useAuth()
  const { exercises, fetch, isFetching } = useExerciseContext()

  useEffect(() => {
    fetch()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Container className={classes.root}>
      <Typography component="h1">
        Exercícios
      </Typography>

      <Box component="section">
        {isFetching ? (
          <LoadingOverlay />
        ) : (
          <ExerciseManager
            exercises={exercises}
            canCreate={user?.isAdmin() || user?.isTeacher()}
            canDelete={user?.isAdmin() || user?.isTeacher()}
            canUpdate={user?.isAdmin() || user?.isTeacher()}
            filterable
          />
        )}
      </Box>
    </Container>
  )
}

export default ExercisesPage
