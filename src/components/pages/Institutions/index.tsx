import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import { useEffect } from 'react'

import InstitutionManager from '@/common/InstitutionManager'
import LoadingOverlay from '@/common/LoadingOverlay'
import { useAuth, useInstitutionContext } from '~/helpers/hooks'

import { useStyles } from './styles'

function InstitutionsPage() {
  const classes = useStyles()
  const { user } = useAuth()
  const { fetch, institutions, isFetching } = useInstitutionContext()

  useEffect(() => {
    fetch()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Container className={classes.root}>
      <Typography component="h1">
        Instituições
      </Typography>

      <Box component="section">
        {isFetching ? (
          <LoadingOverlay />
        ) : (
          <InstitutionManager
            institutions={institutions}
            canCreate={user?.isAdmin()}
            canDelete={user?.isAdmin()}
            canUpdate={user?.isAdmin()}
            filterable
          />
        )}
      </Box>
    </Container>
  )
}

export default InstitutionsPage
