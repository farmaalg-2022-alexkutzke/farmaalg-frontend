import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'

import { useStyles } from './styles'

function LandingPage() {
  const classes = useStyles()

  return (
    <Box className={classes.root}>
      <img
        className={classes.media}
        src="/assets/img/landing.svg"
        alt="ilustração"
      />
      <Typography className={classes.slogan} component="h1">
        O seu lugar de <br />
        estudos de algorítmos
      </Typography>
    </Box>
  )
}

export default LandingPage
