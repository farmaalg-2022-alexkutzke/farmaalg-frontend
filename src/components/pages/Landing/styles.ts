import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',

    margin: theme.spacing(10, 0),

    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
      gap: theme.spacing(2),
      margin: theme.spacing(2),
    },
  },

  media: {
    position: 'relative',
    top: 32,
    left: 100,

    transform: 'scale(1.5)',

    width: 600,
    height: 600,
    objectFit: 'contain',

    [theme.breakpoints.down('md')]: {
      position: 'static',

      transform: 'unset',
      width: '100%',
    },
  },

  slogan: {
    position: 'relative',
    left: -100,

    color: theme.palette.primary.main,
    fontSize: '4rem',
    fontWeight: 900,
    textAlign: 'right',
    lineHeight: 1.4,

    [theme.breakpoints.down('md')]: {
      position: 'static',
      textAlign: 'center',
    },
  },
}))
