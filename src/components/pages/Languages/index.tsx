import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import { useEffect } from 'react'

import LanguageManager from '@/common/LanguageManager'
import LoadingOverlay from '@/common/LoadingOverlay'
import { useAuth, useLanguageContext } from '~/helpers/hooks'

import { useStyles } from './styles'

function LanguagesPage() {
  const classes = useStyles()
  const { user } = useAuth()
  const { fetch, isFetching, languages } = useLanguageContext()

  useEffect(() => {
    fetch()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Container className={classes.root}>
      <Typography component="h1">
        Linguagens de Programação Suportadas
      </Typography>

      <Box component="section">
        {isFetching ? (
          <LoadingOverlay />
        ) : (
          <LanguageManager
            languages={languages}
            canCreate={user?.isAdmin()}
            canDelete={user?.isAdmin()}
            canUpdate={user?.isAdmin()}
            filterable
          />
        )}
      </Box>
    </Container>
  )
}

export default LanguagesPage
