import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import Grid from '@mui/material/Grid'
import Link from '@mui/material/Link'
import Paper from '@mui/material/Paper'
import { Link as RouterLink } from 'react-router-dom'
import * as Yup from 'yup'

import Button from '@/common/Button'
import Checkbox from '@/common/Checkbox'
import FarmaAlgLogo from '@/common/FarmaAlgLogo'
import FormField from '@/common/FormField'
import { useAuth, useForm } from '~/helpers/hooks'
import { useStyles } from './styles'

namespace SignInPage {
  export interface FormData {
    email: string
    senha: string
    manterConectado: boolean
  }
}

const signInFormValSchema = Yup.object().shape({
  email: Yup.string().required().email().trim(),
  senha: Yup.string().required(),
  manterConectado: Yup.boolean().optional(),
})

function SignInPage() {
  const classes = useStyles()
  const { isLoading, signIn } = useAuth()
  const { Form, getController, setValue } = useForm<SignInPage.FormData>({
    valSchema: signInFormValSchema,
  })

  function handleSubmit({ email, senha, manterConectado }: SignInPage.FormData) {
    signIn(email, senha, manterConectado).catch(() => {
      setValue('senha', '')
    })
  }

  return (
    <Box className={classes.root}>
      <Container maxWidth="xs">
        <Paper className={classes.card} elevation={4}>
          <Box component="header">
            <FarmaAlgLogo />
          </Box>

          <Form disabled={isLoading} onSubmit={handleSubmit}>
            <FormField
              controller={getController('email')}
              label="Endereço de email"
              autoComplete="email"
              autoFocus
            />
            <FormField
              type="password"
              controller={getController('senha')}
              label="Senha de acesso"
              autoComplete="current-password"
            />
            <Checkbox
              controller={getController('manterConectado', false)}
              label="Mantenha-me conectado"
            />

            <Button disabled={isLoading} type="submit" >
              Entrar
            </Button>
          </Form>

          <Grid container>
            <Grid item xs>
              <Link to="signup" component={RouterLink} variant="body2">
                Esqueceu sua senha?
              </Link>
            </Grid>
            <Grid item>
              <Link to="registrar" component={RouterLink} variant="body2">
                Ainda não sou cadastrado
              </Link>
            </Grid>
          </Grid>
        </Paper>
      </Container>
    </Box>
  )
}

export default SignInPage
