import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    paddingTop: '10vh',
  },

  card: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(4),

    '& header': {
      padding: theme.spacing(4, 4, 8),

      '& > *': {
        display: 'block',
        transform: 'scale(1.3)',
      },
    },

    '& > *:last-child': {
      marginTop: theme.spacing(4),
    },

    '& [type="submit"]': {
      width: '100%',
      marginTop: theme.spacing(4),
      padding: theme.spacing(1),
    },
  },
}))
