import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import * as Yup from 'yup'

import Button from '@/common/Button'
import FormField from '@/common/FormField'
import { generateAvatar } from '~/helpers'
import { useAuth, useForm, useFormMeta } from '~/helpers/hooks'
import { AlunoDTO, Perfil } from '~/models'

import { useStyles } from '../styles'

const studentSignUpValSchema = Yup.object({
  nome: Yup.string().required().max(120),
  email: Yup.string().required().email(),
  instituicao: Yup.object({
    id: Yup.number().required().typeError('Preenchimento obrigatório.'),
  }),
  idInstitucional: Yup.string().required(),
  senha: Yup.string().required().min(8),
  confirmarSenha: Yup.string().required()
    .oneOf([Yup.ref('senha')], 'As senhas não conferem'),
})

function StudentSignUpForm() {
  const classes = useStyles()
  const { isLoading, signUp } = useAuth()
  const { institutionsCombobox } = useFormMeta()
  const { Form, getController } = useForm<AlunoDTO>({
    valSchema: studentSignUpValSchema,
    defaultValues: {
      perfil: Perfil.ALUNO,
      avatar: generateAvatar(),
    },
  })

  function handleSubmit(data: AlunoDTO) {
    signUp(data)
  }

  return (
    <Grid container>
      <Grid
        item
        xs={12}
        md={8}
        lg={6}
      >
        <Paper className={classes.card} elevation={3}>
          <Form disabled={isLoading} onSubmit={handleSubmit}>
            <FormField
              controller={getController('nome')}
              label="Nome completo"
              autoComplete="name"
            />
            <FormField
              controller={getController('email')}
              label="Email de contato"
              autoComplete="email"
            />
            <FormField
              controller={getController('instituicao.id')}
              label="Instituição de ensino"
              options={institutionsCombobox.map((institution) => ({
                label: institution.nome,
                value: institution.id,
              }))}
            />
            <FormField
              controller={getController('idInstitucional')}
              label="GRR"
            />
            <FormField
              type="password"
              controller={getController('senha')}
              label="Senha de acesso"
              autoComplete="new-password"
            />
            <FormField
              type="password"
              controller={getController('confirmarSenha')}
              label="Confirmar senha de acesso"
              autoComplete="new-password"
            />

            <Button disabled={isLoading} type="submit">
              Cadastrar
            </Button>
          </Form>
        </Paper>
      </Grid>
      <Grid
        item
        xs={false}
        md={4}
        lg={6}
      >
        <img src="/assets/img/studying.svg" alt="estudando" />
      </Grid>
    </Grid>
  )
}

export default StudentSignUpForm
