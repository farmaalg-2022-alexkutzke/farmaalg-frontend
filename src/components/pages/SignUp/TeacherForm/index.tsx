import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import * as Yup from 'yup'

import Button from '@/common/Button'
import FormField from '@/common/FormField'
import { generateAvatar } from '~/helpers'
import { useAuth, useForm, useFormMeta } from '~/helpers/hooks'
import { Perfil, ProfessorDTO } from '~/models'

import { useStyles } from '../styles'

const teacherSignUpValSchema = Yup.object().shape({
  nome: Yup.string().required().max(120),
  instituicao: Yup.object({
    id: Yup.number().required().typeError('Preenchimento obrigatório.'),
  }),
  idInstitucional: Yup.string().required(),
  telefone: Yup.string().required().min(10).max(11),
  email: Yup.string().required().email(),
  senha: Yup.string().required().min(8),
  confirmarSenha: Yup.string().required()
    .oneOf([Yup.ref('senha')], 'As senhas não conferem'),
}).required()

function TeacherSignUpForm() {
  const classes = useStyles()
  const { isLoading, signUp } = useAuth()
  const { institutionsCombobox } = useFormMeta()
  const { Form, getController } = useForm<ProfessorDTO>({
    valSchema: teacherSignUpValSchema,
    defaultValues: {
      perfil: Perfil.PROFESSOR,
      avatar: generateAvatar(),
    },
  })

  function handleSubmit(data: ProfessorDTO) {
    signUp(data)
  }

  return (
    <Grid container>
      <Grid
        item
        xs={false}
        md={4}
        lg={6}
      >
        <img src="/assets/img/teaching.svg" alt="ensinando" />
      </Grid>
      <Grid
        item
        xs={12}
        md={8}
        lg={6}
      >
        <Paper className={classes.card} elevation={3}>
          <Form disabled={isLoading} onSubmit={handleSubmit}>
            <FormField
              controller={getController('nome')}
              label="Nome completo"
              autoComplete="name"
            />
            <FormField
              controller={getController('instituicao.id')}
              label="Instituição de ensino"
              options={institutionsCombobox.map((institution) => ({
                label: institution.nome,
                value: institution.id,
              }))}
            />
            <FormField
              controller={getController('idInstitucional')}
              label="Matrícula"
            />
            <FormField
              controller={getController('email')}
              label="Email de contato"
              autoComplete="email"
            />
            <FormField
              controller={getController('telefone')}
              label="Telefone para contato"
              autoComplete="tel"
            />
            <FormField
              type="password"
              controller={getController('senha')}
              label="Senha de acesso"
              autoComplete="new-password"
            />
            <FormField
              type="password"
              controller={getController('confirmarSenha')}
              label="Confirmar senha de acesso"
              autoComplete="new-password"
            />

            <Button disabled={isLoading} type="submit" >
              Cadastrar
            </Button>
          </Form>
        </Paper>
      </Grid>
    </Grid>
  )
}

export default TeacherSignUpForm
