import Box from '@mui/material/Box'
import ButtonGroup from '@mui/material/ButtonGroup'
import Typography from '@mui/material/Typography'
import { useState } from 'react'

import Button from '@/common/Button'
import StudentForm from './StudentForm'
import TeacherForm from './TeacherForm'
import { useStyles } from './styles'

function SignUpPage() {
  const classes = useStyles()
  const [visibleForm, setVisibleForm] = useState<'student' | 'teacher'>('student')

  return (
    <Box className={classes.root}>
      <Box className={classes.formSelector}>
        <Typography component="h2" variant="h4">
          Cadastre-se na Plataforma
        </Typography>

        <ButtonGroup fullWidth size="large">
          <Button
            selected={visibleForm === 'student'}
            variant="outlined"
            onClick={() => setVisibleForm('student')}
          >
            Sou Estudante
          </Button>
          <Button
            selected={visibleForm === 'teacher'}
            variant="outlined"
            onClick={() => setVisibleForm('teacher')}
          >
            Sou Docente
          </Button>
        </ButtonGroup>
      </Box>

      {visibleForm === 'student' && <StudentForm />}
      {visibleForm === 'teacher' && <TeacherForm />}
    </Box>
  )
}

export default SignUpPage
