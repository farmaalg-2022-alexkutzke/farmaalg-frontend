import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles((theme) => ({
  root: {
    '& img': {
      width: '90%',
      margin: theme.spacing(4, '5%'),
      objectFit: 'contain',
    },
  },

  formSelector: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

    maxWidth: 860,
    margin: theme.spacing(6, 'auto', 4),

    '& h2': {
      marginBottom: theme.spacing(4),
    },

    '& button': {
      borderColor: theme.palette.primary.main,

      '&:first-child': {
        borderTopLeftRadius: theme.spacing(3),
        borderBottomLeftRadius: theme.spacing(3),
        borderRight: 'none',
      },

      '&:last-child': {
        borderTopRightRadius: theme.spacing(3),
        borderBottomRightRadius: theme.spacing(3),
        borderLeft: 'none',
      },
    },
  },

  card: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    gap: 10,

    marginBottom: theme.spacing(8),

    '& form': {
      width: '100%',
      padding: theme.spacing(4),

      '& button': {
        width: '60%',
        margin: theme.spacing(4, '20%'),
        padding: theme.spacing(1),
      },
    },
  },
}))
