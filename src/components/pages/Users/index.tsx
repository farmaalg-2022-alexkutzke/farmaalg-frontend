import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import { useEffect } from 'react'

import LoadingOverlay from '@/common/LoadingOverlay'
import UserManager from '@/common/UserManager'
import { useAuth, useUserContext } from '~/helpers/hooks'

import { useStyles } from './styles'

function UsersPage() {
  const classes = useStyles()
  const { user } = useAuth()
  const { fetch, isFetching, users } = useUserContext()

  useEffect(() => {
    fetch()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Container className={classes.root}>
      <Typography component="h1">
        Usuários
      </Typography>

      <Box component="section">
        {isFetching ? (
          <LoadingOverlay />
        ) : (
          <UserManager
            users={users}
            canCreate={user?.isAdmin()}
            canDelete={user?.isAdmin()}
            canUpdate={user?.isAdmin()}
            filterable
          />
        )}
      </Box>
    </Container>
  )
}

export default UsersPage
