import { createTheme, Theme } from '@mui/material/styles'

declare module '@mui/styles/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}

export const theme = createTheme({
  palette: {
    primary: {
      main: '#004166',
      light: '#99bfcf',
    },
    secondary: {
      main: '#007582',
      light: '#99ceb1',
    },
    success: {
      main: '#466d0e',
    },
    warning: {
      main: '#e5c44d',
    },
    error: {
      main: '#9e2a2f',
      light: '#e7cacb',
    },
    background: {
      default: '#fdfffc',
    },
    divider: '#b2b2b2',
  },
})
