
declare module 'cep-promise/dist/cep-promise-browser' {
  export interface CEP {
    cep: string
    state: string
    city: string
    street: string
    neighborhood: string
    service: string
  }

  export interface Configurations {
    providers?: ('brasilapi' | 'correios' | 'viacep' | 'widenet')[]
    timeout?: number
  }

  function CepService(
    cep: string | number,
    configurations?: Configurations,
  ): Promise<CEP>

  export default CepService
}

interface ImportMetaEnv {
  readonly VITE_FARMAALG_SERVER: string
  readonly VITE_FARMAALG_AUTH_HEADER: string
  readonly VITE_FARMAALG_AUTH_STORAGE_KEY: string
  readonly VITE_FARMAALG_PUBLIC_HOME_PATH: string
  readonly VITE_FARMAALG_AUTH_HOME_PATH: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
