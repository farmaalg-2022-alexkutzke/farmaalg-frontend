/* eslint-disable no-template-curly-in-string */
import cnpjUtils from 'cnpj-utils'
import * as Yup from 'yup'

import { classroomService } from '~/services'

declare module 'yup' {
  interface StringSchema {
    cnpj: (message?: string) => StringSchema
    cep: (message?: string) => StringSchema
    classroomAccessCode: (message?: string) => StringSchema
  }
}

Yup.addMethod(Yup.string, 'cnpj', function (message?: string) {
  return this.test({
    name: 'cnpj',
    exclusive: true,
    message: message || "'${value}' não corresponde a um CNPJ válido.",
    test: (value) => !value || cnpjUtils.isValid(value),
  })
})

Yup.addMethod(Yup.string, 'cep', function (message?: string) {
  return this.test({
    name: 'cep',
    exclusive: true,
    message: message || "'${value}' não corresponde a um CEP válido.",
    test: (value) => !value || /^\d{5}-?\d{3}$/.test(value),
  })
})

Yup.addMethod(Yup.string, 'classroomAccessCode', function (message?: string) {
  return this.test({
    name: 'classroomAccessCode',
    exclusive: true,
    message: message || 'Este código já está em uso.',
    async test(value) {
      // eslint-disable-next-line no-return-await
      return !value || await classroomService.checkAccessCode(value)
    },
  })
})

Yup.setLocale({
  array: {
    min: 'Deve ter no mínimo ${min} item(ns).',
    max: 'Deve ter no máximo ${max} item(ns).',
  },
  mixed: {
    required: 'Preenchimento obrigatório.',
  },
  number: {
    min: 'Deve ser maior ou igual a ${min}.',
    max: 'Deve ser menor ou igual a ${max}.',
    lessThan: 'Deve ser menor que ${less}.',
    moreThan: 'Deve ser maior que ${more}.',
    positive: 'Deve ser um número positivo.',
    negative: 'Deve ser um número negativo.',
    integer: 'Deve ser um número inteiro.',
  },
  string: {
    length: 'Deve ter exatamente ${length} caracteres.',
    min: 'Deve ter no mínimo ${min} caracteres.',
    max: 'Deve ter no máximo ${max} caracteres.',
    email: 'Deve ser um email válido.',
    url: 'Deve ser uma URL válida.',
  },
})

export default Yup
