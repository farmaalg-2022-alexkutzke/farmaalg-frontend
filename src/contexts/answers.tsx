import { createContext, ReactNode, useState } from 'react'

import { useAlert, useAuth, useExerciseContext } from '~/helpers/hooks'
import { ExecucaoCasoTeste, Resposta } from '~/models'
import { exerciseService } from '~/services'

namespace AnswerContext {
  export interface Props {
    answers: Resposta[]
    isFetching: boolean
    isCreating: boolean
    isLoading: boolean
    findByStudentAndExercise: (studentId: number, exerciseId: number) => Promise<void>
    test: (languageId: number, srcCode: string) => Promise<ExecucaoCasoTeste[]>
    submit: (languageId: number, srcCode: string) => Promise<void>
  }
}

namespace AnswerProvider {
  export interface Props {
    children: ReactNode
  }
}

export const AnswerContext = createContext({} as AnswerContext.Props)

export function AnswerProvider({ children }: AnswerProvider.Props) {
  const notification = useAlert()
  const { user } = useAuth()
  const { focusedExercise } = useExerciseContext()
  const [answers, setAnswers] = useState<Resposta[]>([])
  const [isFetching, setFetching] = useState(false)
  const [isCreating, setCreating] = useState(false)
  const isLoading = isFetching || isCreating

  async function findByStudentAndExercise(studentId: number, exerciseId: number) {
    try {
      setFetching(true)
      setAnswers(await exerciseService.findAnswersByExerciseAndStudent(exerciseId, studentId))
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setFetching(false)
    }
  }

  async function test(languageId: number, srcCode: string) {
    setCreating(true)
    const newAnswer = await exerciseService.testAnswer({
      exercicio: { id: focusedExercise?.id as any },
      linguagem: { id: languageId },
      aluno: { id: user?.id as any },
      codigo: srcCode,
    })
    setAnswers([...answers, newAnswer])
    notification.success('Casos de teste executados.')
    setCreating(false)
    return newAnswer.casosTeste
  }

  async function submit(languageId: number, srcCode: string) {
    try {
      setCreating(true)
      const newAnswer = await exerciseService.submitAnswer({
        exercicio: { id: focusedExercise?.id as any },
        linguagem: { id: languageId },
        aluno: { id: user?.id as any },
        codigo: srcCode,
      })
      setAnswers([...answers, newAnswer])
      notification.success('Resposta salva com sucesso.')
    } catch (error: any) {
      notification.error('Exercício já foi respondido.')
    } finally {
      setCreating(false)
    }
  }

  return (
    <AnswerContext.Provider
      value={{
        answers,
        isFetching,
        isCreating,
        isLoading,
        findByStudentAndExercise,
        test,
        submit,
      }}
    >
      {children}
    </AnswerContext.Provider>
  )
}
