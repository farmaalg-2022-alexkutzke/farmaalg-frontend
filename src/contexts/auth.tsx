import { createContext, ReactNode, useEffect, useState } from 'react'

import { isAdmin, isStudent, isTeacher } from '~/helpers'
import { useAlert } from '~/helpers/hooks'
import { Usuario, UsuarioDTO } from '~/models'
import { AUTH_HOME_PATH, PUBLIC_HOME_PATH } from '~/routes'
import { authService, userService } from '~/services'

namespace AuthContext {
  export interface Props {
    isLoading: boolean
    isAuthenticated: boolean
    user?: Usuario & {
      isAdmin: () => boolean
      isStudent: () => boolean
      isTeacher: () => boolean
    }
    signIn: (email: string, password: string, rememberMe: boolean) => Promise<void>
    signOut: () => Promise<void>
    signUp: <U extends UsuarioDTO>(userData: U) => Promise<void>
  }
}

namespace AuthProvider {
  export interface Props {
    children: ReactNode
  }
}

export const AuthContext = createContext({} as AuthContext.Props)

export function AuthProvider({ children }: AuthProvider.Props) {
  const notification = useAlert()
  const [currentUser, setCurrentUser] = useState<Usuario>()
  const [isLoading, setLoading] = useState(false)
  const { isAuthenticated } = authService

  async function signIn(email: string, password: string, rememberMe: boolean) {
    try {
      setLoading(true)
      await authService.authenticate(email, password, rememberMe)
      window.location.href = AUTH_HOME_PATH
    } catch (error: any) {
      if (error.response?.status === 400) {
        notification.error('Credenciais Inválidas. Tente novamente.')
        throw error
      } else {
        console.error(error)
      }
    } finally {
      setLoading(false)
    }
  }

  async function signOut() {
    await authService.unlink()
    window.location.href = PUBLIC_HOME_PATH
  }

  async function signUp<U extends UsuarioDTO>(userData: U) {
    try {
      setLoading(true)
      console.info(userData)
      await userService.create(userData)
      await signIn(userData.email, userData.senha, false)
      window.location.href = AUTH_HOME_PATH
    } catch (error: any) {
      notification.error('Falha ao tentar cadastra novo usuário. Tente novamente mais tarde.')
      throw error
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    isAuthenticated && authService.getUserData().then(setCurrentUser)
  }, [isAuthenticated])

  return (
    <AuthContext.Provider
      value={{
        isLoading,
        isAuthenticated,
        signIn,
        signOut,
        signUp,
        user: currentUser && {
          ...currentUser,
          isAdmin: () => isAdmin(currentUser),
          isStudent: () => isStudent(currentUser),
          isTeacher: () => isTeacher(currentUser),
        },
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}
