import { createContext, ReactNode, useEffect, useState } from 'react'

import { useAlert, useAuth } from '~/helpers/hooks'
import { Turma, TurmaDTO } from '~/models'
import { classroomService } from '~/services'

namespace ClassroomContext {
  export interface Props {
    focusedClassroom: Turma | null
    classrooms: Turma[]
    isFetching: boolean
    isCreating: boolean
    isUpdating: boolean
    isDestroying: boolean
    isSaving: boolean
    isLoading: boolean
    fetch: () => Promise<void>
    find: (classroomId: number) => Promise<void>
    create: (classroom: TurmaDTO) => Promise<void>
    update: (classroomId: number, classroom: TurmaDTO) => Promise<void>
    destroy: (classroomId: number) => Promise<void>
    register: (accessCode: string) => void
  }
}

namespace ClassroomProvider {
  export interface Props {
    children: ReactNode
  }
}

export const ClassroomContext = createContext({} as ClassroomContext.Props)

export function ClassroomProvider({ children }: ClassroomProvider.Props) {
  const { user } = useAuth()
  const notification = useAlert()
  const [focusedClassroom, setFocusedClassroom] = useState<Turma | null>(null)
  const [classrooms, setClassrooms] = useState<Turma[]>([])
  const [isFetching, setFetching] = useState(false)
  const [isCreating, setCreating] = useState(false)
  const [isUpdating, setUpdating] = useState(false)
  const [isDestroying, setDestroying] = useState(false)
  const isSaving = isCreating || isUpdating
  const isLoading = isFetching || isCreating || isUpdating || isDestroying

  async function fetch() {
    try {
      setFetching(true)
      setClassrooms(await classroomService.get())
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setFetching(false)
    }
  }

  async function find(classroomId: number) {
    try {
      setFetching(true)
      setFocusedClassroom(await classroomService.find(classroomId))
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setFetching(false)
    }
  }

  async function create(data: TurmaDTO) {
    try {
      setCreating(true)
      const newClassroom = await classroomService.create(data)
      setClassrooms([...classrooms, newClassroom])
      notification.success('Turma criada com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setCreating(false)
    }
  }

  async function update(id: number, data: TurmaDTO) {
    try {
      setUpdating(true)
      const newState = await classroomService.update(id, data)
      setClassrooms(classrooms.map((classroom) => (classroom.id === id ? newState : classroom)))
      notification.success('Turma atualizada com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setUpdating(false)
    }
  }

  async function destroy(id: number) {
    try {
      setDestroying(true)
      await classroomService.destroy(id)
      setClassrooms(classrooms.filter((classroom) => classroom.id !== id))
      notification.success('Turma excluída com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setDestroying(false)
    }
  }

  async function register(accessCode: string) {
    try {
      setUpdating(true)
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const newClassroom = await classroomService.registerWithAccessCode(user!.id, accessCode)
      setClassrooms([...classrooms, newClassroom])
      setFocusedClassroom(newClassroom)
      notification.success('Cadastrado com sucesso.')
    } catch {
      notification.error(`Falha ao se cadastrar com o código "${accessCode}".`)
    } finally {
      setUpdating(false)
    }
  }

  useEffect(() => {
    fetch()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <ClassroomContext.Provider
      value={{
        focusedClassroom,
        classrooms,
        isFetching,
        isCreating,
        isUpdating,
        isDestroying,
        isSaving,
        isLoading,
        fetch,
        find,
        create,
        update,
        destroy,
        register,
      }}
    >
      {children}
    </ClassroomContext.Provider>
  )
}
