import { createContext, ReactNode } from 'react'
import { useLocalStorage } from 'react-use'

namespace DrawerContext {
  export interface Props {
    isOpen: boolean
    open: () => void
    close: () => void
    toggle: (toOpen?: boolean) => void
  }
}

namespace DrawerProvider {
  export interface Props {
    children: ReactNode
  }
}

export const DrawerContext = createContext({} as DrawerContext.Props)

export function DrawerProvider({ children }: DrawerProvider.Props) {
  const [isOpen, setOpen] = useLocalStorage('Farma-Alg::drawer', true)

  function open() {
    setOpen(true)
  }

  function close() {
    setOpen(false)
  }

  function toggle(toOpen = !isOpen) {
    setOpen(toOpen)
  }

  return (
    <DrawerContext.Provider
      value={{
        isOpen: Boolean(isOpen),
        open,
        close,
        toggle,
      }}
    >{children}</DrawerContext.Provider>
  )
}
