import { createContext, ReactNode, useState } from 'react'

import { useAlert } from '~/helpers/hooks'
import { Exercicio, ExercicioDTO, TagExercicio } from '~/models'
import { exerciseService, tagService } from '~/services'

namespace ExerciseContext {
  export interface Props {
    focusedExercise: Exercicio | null
    exercises: Exercicio[]
    tags: TagExercicio[]
    isFetching: boolean
    isCreating: boolean
    isUpdating: boolean
    isDestroying: boolean
    isSaving: boolean
    isLoading: boolean
    fetch: () => Promise<void>
    find: (exerciseId: number) => Promise<void>
    create: (exercise: ExercicioDTO) => Promise<void>
    update: (exerciseId: number, exercise: ExercicioDTO) => Promise<void>
    destroy: (exerciseId: number) => Promise<void>
    replicate: (exerciseId: number, classroomId: number) => Promise<Exercicio | null>
  }
}

namespace ExerciseProvider {
  export interface Props {
    children: ReactNode
  }
}

export const ExerciseContext = createContext({} as ExerciseContext.Props)

export function ExerciseProvider({ children }: ExerciseProvider.Props) {
  const notification = useAlert()
  const [focusedExercise, setFocusedExercise] = useState<Exercicio | null>(null)
  const [exercises, setExercises] = useState<Exercicio[]>([])
  const [tags, setTags] = useState<TagExercicio[]>([])
  const [isFetching, setFetching] = useState(false)
  const [isCreating, setCreating] = useState(false)
  const [isUpdating, setUpdating] = useState(false)
  const [isDestroying, setDestroying] = useState(false)
  const isSaving = isCreating || isUpdating
  const isLoading = isFetching || isCreating || isUpdating || isDestroying

  async function fetch() {
    try {
      setFetching(true)
      // eslint-disable-next-line no-shadow
      const [exercises, tags] = await Promise.all([
        exerciseService.get(),
        tagService.get(),
      ])
      setTags(tags)
      setExercises(exercises)
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setFetching(false)
    }
  }

  async function find(exerciseId: number) {
    try {
      setFetching(true)
      setFocusedExercise(await exerciseService.find(exerciseId))
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setFetching(false)
    }
  }

  async function create(data: ExercicioDTO) {
    try {
      setCreating(true)
      const newExercise = await exerciseService.createFull(data)
      setExercises([...exercises, newExercise])
      notification.success('Exercicio criado com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setCreating(false)
    }
  }

  async function update(id: number, data: ExercicioDTO) {
    try {
      setUpdating(true)
      const newState = await exerciseService.update(id, data)
      setExercises(exercises.map((exercise) => (exercise.id === id ? newState : exercise)))
      notification.success('Exercicio atualizado com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setUpdating(false)
    }
  }

  async function destroy(id: number) {
    try {
      setDestroying(true)
      await exerciseService.destroy(id)
      setExercises(exercises.filter((exercise) => exercise.id !== id))
      notification.success('Exercicio excluído com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setDestroying(false)
    }
  }

  async function replicate(exerciseId: number, classroomId: number) {
    try {
      setCreating(true)
      const newExercise = await exerciseService.replicate(exerciseId, classroomId)
      setExercises([...exercises, newExercise])
      notification.success('Exercicio criado com sucesso.')
      return newExercise
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
      return null
    } finally {
      setCreating(false)
    }
  }

  return (
    <ExerciseContext.Provider
      value={{
        focusedExercise,
        exercises,
        tags,
        isFetching,
        isCreating,
        isUpdating,
        isDestroying,
        isSaving,
        isLoading,
        fetch,
        find,
        create,
        update,
        destroy,
        replicate,
      }}
    >
      {children}
    </ExerciseContext.Provider>
  )
}
