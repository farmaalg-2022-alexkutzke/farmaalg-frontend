import { createContext, ReactNode, useEffect, useState } from 'react'

import { useAlert } from '~/helpers/hooks'
import { institutionService, InstitutionsComboBox } from '~/services'

namespace FormMetaContext {
  export interface Props {
    institutionsCombobox: InstitutionsComboBox
  }
}

namespace FormMetaProvider {
  export interface Props {
    children: ReactNode
  }
}

export const FormMetaContext = createContext({} as FormMetaContext.Props)

export function FormMetaProvider({ children }: FormMetaProvider.Props) {
  const notification = useAlert()
  const [institutionsCombobox, setInstitutionsCombobox] = useState<InstitutionsComboBox>([])

  async function loadInstitutions() {
    try {
      setInstitutionsCombobox(await institutionService.getComboBox())
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    }
  }

  useEffect(() => {
    loadInstitutions()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <FormMetaContext.Provider value={{ institutionsCombobox }}>
      {children}
    </FormMetaContext.Provider>
  )
}
