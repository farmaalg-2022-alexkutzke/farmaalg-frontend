import CssBaseline from '@mui/material/CssBaseline'
import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles'
import { SnackbarProvider } from 'notistack'
import { ReactNode } from 'react'
import { BrowserRouter } from 'react-router-dom'

import { theme } from '~/config'
import { useAuth } from '~/helpers/hooks'

import { AnswerProvider } from './answers'
import { AuthProvider } from './auth'
import { ClassroomProvider } from './classrooms'
import { DrawerProvider } from './drawer'
import { ExerciseProvider } from './exercises'
import { FormMetaProvider } from './formMeta'
import { InstitutionProvider } from './institutions'
import { LanguageProvider } from './languages'
import { TestCaseProvider } from './testCases'
import { UserProvider } from './users'

namespace ContextProvider {
  export interface Props {
    children: ReactNode
  }
}

export * from './auth'
export * from './answers'
export * from './classrooms'
export * from './drawer'
export * from './exercises'
export * from './formMeta'
export * from './institutions'
export * from './languages'
export * from './testCases'
export * from './users'

function PublicContextProvider({ children }: ContextProvider.Props) {
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline /> {/* <== kickstart a simple and consistent baseline to build upon. */}
        <SnackbarProvider maxSnack={5}>
          <AuthProvider>
            <BrowserRouter>
              <DrawerProvider>
                <FormMetaProvider>
                  {children}
                </FormMetaProvider>
              </DrawerProvider>
            </BrowserRouter>
          </AuthProvider>
        </SnackbarProvider>
      </ThemeProvider>
    </StyledEngineProvider>
  )
}

function AuthContextProvider({ children }: ContextProvider.Props) {
  const { isAuthenticated } = useAuth()

  if (!isAuthenticated) {
    return <>{children}</>
  }

  return (
    <InstitutionProvider>
      <UserProvider>
        <LanguageProvider>
          <ClassroomProvider>
            <ExerciseProvider>
              <TestCaseProvider>
                <AnswerProvider>
                  {children}
                </AnswerProvider>
              </TestCaseProvider>
            </ExerciseProvider>
          </ClassroomProvider>
        </LanguageProvider>
      </UserProvider>
    </InstitutionProvider>
  )
}

function ContextProvider({ children }: ContextProvider.Props) {
  return (
    <PublicContextProvider>
      <AuthContextProvider>
        {children}
      </AuthContextProvider>
    </PublicContextProvider>
  )
}

export default ContextProvider
