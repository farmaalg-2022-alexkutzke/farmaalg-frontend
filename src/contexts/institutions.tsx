import { createContext, ReactNode, useState } from 'react'

import { useAlert } from '~/helpers/hooks'
import { Instituicao, InstituicaoDTO, InstituicaoContatoDTO } from '~/models'
import { institutionService } from '~/services'

namespace InstitutionContext {
  export interface Props {
    institutions: Instituicao[]
    isFetching: boolean
    isCreating: boolean
    isUpdating: boolean
    isDestroying: boolean
    isSaving: boolean
    isLoading: boolean
    fetch: () => Promise<void>
    create: (institution: InstituicaoDTO) => Promise<void>
    update: (institutionId: number, institution: InstituicaoDTO) => Promise<void>
    destroy: (institutionId: number) => Promise<void>
    addContact: (institutionId: number, contact: InstituicaoContatoDTO) => Promise<void>
    updateContact: (institutionId: number, contactId: number, contact: InstituicaoContatoDTO) => Promise<void>
    removeContact: (institutionId: number, contactId: number) => Promise<void>
  }
}

namespace InstitutionProvider {
  export interface Props {
    children: ReactNode
  }
}

export const InstitutionContext = createContext({} as InstitutionContext.Props)

export function InstitutionProvider({ children }: InstitutionProvider.Props) {
  const notification = useAlert()
  const [institutions, setInstitutions] = useState<Instituicao[]>([])
  const [isFetching, setFetching] = useState(false)
  const [isCreating, setCreating] = useState(false)
  const [isUpdating, setUpdating] = useState(false)
  const [isDestroying, setDestroying] = useState(false)
  const isSaving = isCreating || isUpdating
  const isLoading = isFetching || isCreating || isUpdating || isDestroying

  async function fetch() {
    try {
      setFetching(true)
      setInstitutions(await institutionService.get())
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setFetching(false)
    }
  }

  async function create(data: InstituicaoDTO) {
    try {
      setCreating(true)
      const newInstitution = await institutionService.create(data)
      setInstitutions([...institutions, newInstitution])
      notification.success('Instituicao criada com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setCreating(false)
    }
  }

  async function update(id: number, data: InstituicaoDTO) {
    try {
      setUpdating(true)
      const newState = await institutionService.update(id, data)
      setInstitutions(institutions.map((institution) => (institution.id === id ? newState : institution)))
      notification.success('Instituicao atualizada com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setUpdating(false)
    }
  }

  async function destroy(id: number) {
    try {
      setDestroying(true)
      await institutionService.destroy(id)
      setInstitutions(institutions.filter((institution) => institution.id !== id))
      notification.success('Instituicao excluída com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setDestroying(false)
    }
  }

  async function addContact(institutionId: number, data: InstituicaoContatoDTO) {
    try {
      const newContact = await institutionService.createContact(institutionId, data)
      setInstitutions(institutions.map((institution) => {
        if (institution.id === institutionId) {
          institution.contatos ??= []
          institution.contatos.push(newContact)
        }

        return institution
      }))
      notification.success('Novo contato salvo com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    }
  }

  async function updateContact(institutionId: number, contactId: number, data: InstituicaoContatoDTO) {
    try {
      const newContactState = await institutionService.updateContact(contactId, data)
      setInstitutions(institutions.map((institution) => {
        if (institution.id === institutionId) {
          institution.contatos ??= []
          institution.contatos = institution.contatos.map((contact) => {
            return contact.id === contactId
              ? newContactState
              : contact
          })
        }

        return institution
      }))
      notification.success('Instituicao atualizada com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    }
  }

  async function removeContact(institutionId: number, contactId: number) {
    try {
      await institutionService.destroyContact(contactId)
      setInstitutions(institutions.map((institution) => {
        if (institution.id === institutionId) {
          institution.contatos ??= []
          institution.contatos = institution.contatos.filter((contact) => {
            return contact.id !== contactId
          })
        }

        return institution
      }))
      notification.success('Instituicao atualizada com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    }
  }

  return (
    <InstitutionContext.Provider
      value={{
        institutions,
        isFetching,
        isCreating,
        isUpdating,
        isDestroying,
        isSaving,
        isLoading,
        fetch,
        create,
        update,
        destroy,
        addContact,
        updateContact,
        removeContact,
      }}
    >
      {children}
    </InstitutionContext.Provider>
  )
}
