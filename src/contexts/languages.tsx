import { createContext, ReactNode, useEffect, useState } from 'react'

import { useAlert, useAuth } from '~/helpers/hooks'
import { Linguagem, LinguagemDTO } from '~/models'
import { languageService } from '~/services'

namespace LanguageContext {
  export interface Props {
    languages: Linguagem[]
    isFetching: boolean
    isCreating: boolean
    isUpdating: boolean
    isDestroying: boolean
    isSaving: boolean
    isLoading: boolean
    fetch: () => Promise<void>
    create: (language: LinguagemDTO) => Promise<void>
    update: (languageId: number, language: LinguagemDTO) => Promise<void>
    destroy: (languageId: number) => Promise<void>
  }
}

namespace LanguageProvider {
  export interface Props {
    children: ReactNode
  }
}

export const LanguageContext = createContext({} as LanguageContext.Props)

export function LanguageProvider({ children }: LanguageProvider.Props) {
  const notification = useAlert()
  const { user } = useAuth()
  const [languages, setLanguages] = useState<Linguagem[]>([])
  const [isFetching, setFetching] = useState(false)
  const [isCreating, setCreating] = useState(false)
  const [isUpdating, setUpdating] = useState(false)
  const [isDestroying, setDestroying] = useState(false)
  const isSaving = isCreating || isUpdating
  const isLoading = isFetching || isCreating || isUpdating || isDestroying

  async function fetch() {
    try {
      setFetching(true)
      setLanguages(await languageService.get())
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setFetching(false)
    }
  }

  async function create(data: LinguagemDTO) {
    try {
      setCreating(true)
      const newLanguage = await languageService.create(data)
      setLanguages([...languages, newLanguage])
      notification.success('Linguagem criada com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setCreating(false)
    }
  }

  async function update(id: number, data: LinguagemDTO) {
    try {
      setUpdating(true)
      const newState = await languageService.update(id, data)
      setLanguages(languages.map((language) => (language.id === id ? newState : language)))
      notification.success('Linguagem atualizada com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setUpdating(false)
    }
  }

  async function destroy(id: number) {
    try {
      setDestroying(true)
      await languageService.destroy(id)
      setLanguages(languages.filter((language) => language.id !== id))
      notification.success('Linguagem excluída com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setDestroying(false)
    }
  }

  useEffect(() => {
    if (user?.isAdmin() || user?.isTeacher()) {
      fetch()
    }
  }, [user]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <LanguageContext.Provider
      value={{
        languages,
        isFetching,
        isCreating,
        isUpdating,
        isDestroying,
        isSaving,
        isLoading,
        fetch,
        create,
        update,
        destroy,
      }}
    >
      {children}
    </LanguageContext.Provider>
  )
}
