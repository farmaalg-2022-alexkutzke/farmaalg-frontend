import { createContext, ReactNode, useEffect, useState } from 'react'

import { useAlert, useExerciseContext } from '~/helpers/hooks'
import { CasoTeste, CasoTesteDTO, ExecucaoCasoTeste } from '~/models'
import { exerciseService } from '~/services'

namespace TestCaseContext {
  export interface Props {
    testCases: CasoTeste[]
    isRunning: boolean
    isFetching: boolean
    isCreating: boolean
    isUpdating: boolean
    isDestroying: boolean
    isSaving: boolean
    isLoading: boolean
    getByFocusedExercise: () => Promise<void>
    create: (testCase: CasoTesteDTO) => Promise<void>
    update: (testCaseId: number, testCase: CasoTesteDTO) => Promise<void>
    destroy: (testCaseId: number) => Promise<void>
    run: (testCaseId: number, languageId: number, code: string) => Promise<ExecucaoCasoTeste>
  }
}

namespace TestCaseProvider {
  export interface Props {
    children: ReactNode
  }
}

export const TestCaseContext = createContext({} as TestCaseContext.Props)

export function TestCaseProvider({ children }: TestCaseProvider.Props) {
  const notification = useAlert()
  const { focusedExercise } = useExerciseContext()
  const [testCases, setTestCases] = useState<CasoTeste[]>([])
  const [isRunning, setRunning] = useState(false)
  const [isFetching, setFetching] = useState(false)
  const [isCreating, setCreating] = useState(false)
  const [isUpdating, setUpdating] = useState(false)
  const [isDestroying, setDestroying] = useState(false)
  const isSaving = isCreating || isUpdating
  const isLoading = isFetching || isCreating || isUpdating || isDestroying

  async function getByFocusedExercise() {
    if (!focusedExercise) {
      return
    }
    try {
      setFetching(true)
      setTestCases(await exerciseService.getTestCases(focusedExercise.id))
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setFetching(false)
    }
  }

  async function create(data: CasoTesteDTO) {
    if (!focusedExercise) {
      return
    }
    try {
      setCreating(true)
      const newTestCase = await exerciseService.createTestCase(focusedExercise.id, data)
      setTestCases([...testCases, newTestCase])
      notification.success('Caso de teste criado com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setCreating(false)
    }
  }

  async function update(id: number, data: CasoTesteDTO) {
    if (!focusedExercise) {
      return
    }
    try {
      setUpdating(true)
      const newState = await exerciseService.updateTestCase(id, data)
      setTestCases(testCases.map((testCase) => (testCase.id === id ? newState : testCase)))
      notification.success('Caso de teste atualizado com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setUpdating(false)
    }
  }

  async function destroy(id: number) {
    if (!focusedExercise) {
      return
    }
    try {
      setDestroying(true)
      await exerciseService.destroyTestCase(id)
      setTestCases(testCases.filter((testCase) => testCase.id !== id))
      notification.success('Caso de teste excluído com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setDestroying(false)
    }
  }

  async function run(testCaseId: number, languageId: number, code: string) {
    setRunning(true)
    const testRun = await exerciseService.runTestCase(testCaseId, languageId, code)
    notification.success('Caso de teste executado.')
    setRunning(false)
    return testRun
  }

  useEffect(() => {
    getByFocusedExercise()
  }, [focusedExercise]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <TestCaseContext.Provider
      value={{
        testCases,
        isRunning,
        isFetching,
        isCreating,
        isUpdating,
        isDestroying,
        isSaving,
        isLoading,
        getByFocusedExercise,
        create,
        update,
        destroy,
        run,
      }}
    >
      {children}
    </TestCaseContext.Provider>
  )
}
