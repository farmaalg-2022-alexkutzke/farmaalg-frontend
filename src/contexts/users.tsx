import { createContext, ReactNode, useMemo, useState } from 'react'

import { useAlert } from '~/helpers/hooks'
import { Admin, Aluno, NovaSenhaDTO, Perfil, Professor, Usuario, UsuarioDTO } from '~/models'
import { userService } from '~/services'

namespace UserContext {
  export interface Props {
    users: Usuario[]
    admins: Admin[]
    students: Aluno[]
    teachers: Professor[]
    isFetching: boolean
    isCreating: boolean
    isUpdating: boolean
    isDestroying: boolean
    isSaving: boolean
    isLoading: boolean
    fetch: () => Promise<void>
    create: <M extends UsuarioDTO>(user: M) => Promise<void>
    update: <M extends UsuarioDTO>(userId: number, user: M) => Promise<void>
    updatePassword: (passwordPayload: NovaSenhaDTO) => Promise<void>
    destroy: (userId: number) => Promise<void>
  }
}

namespace UserProvider {
  export interface Props {
    children: ReactNode
  }
}

export const UserContext = createContext({} as UserContext.Props)

export function UserProvider({ children }: UserProvider.Props) {
  const notification = useAlert()
  const [users, setUsers] = useState<Usuario[]>([])
  const { admins, students, teachers } = useMemo(() => ({
    admins: users.filter((user) => user.perfil === Perfil.ADMIN) as Admin[],
    students: users.filter((user) => user.perfil === Perfil.ALUNO) as Aluno[],
    teachers: users.filter((user) => user.perfil === Perfil.PROFESSOR) as Professor[],
  }), [users.length]) // eslint-disable-line react-hooks/exhaustive-deps

  const [isFetching, setFetching] = useState(false)
  const [isCreating, setCreating] = useState(false)
  const [isUpdating, setUpdating] = useState(false)
  const [isDestroying, setDestroying] = useState(false)
  const isSaving = isCreating || isUpdating
  const isLoading = isFetching || isCreating || isUpdating || isDestroying

  async function fetch() {
    try {
      setFetching(true)
      setUsers(await userService.get())
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setFetching(false)
    }
  }

  async function create<M extends UsuarioDTO>(data: M) {
    try {
      setCreating(true)
      const newUser = await userService.create(data)
      setUsers([...users, newUser])
      notification.success('Usuario criado com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setCreating(false)
    }
  }

  async function update<M extends UsuarioDTO>(id: number, data: M) {
    try {
      setUpdating(true)
      const newState = await userService.update(id, data)
      setUsers(users.map((user) => (user.id === id ? newState : user)))
      notification.success('Usuario atualizado com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setUpdating(false)
    }
  }

  async function updatePassword(payload: NovaSenhaDTO) {
    try {
      setUpdating(true)
      payload.id
        ? await userService.updatePasswordAsAdmin(payload.id, payload.senhaNova)
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        : await userService.updatePassword(payload.senhaAntiga!, payload.senhaNova)
      notification.success('Senha atualizada com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setUpdating(false)
    }
  }

  async function destroy(id: number) {
    try {
      setDestroying(true)
      await userService.destroy(id)
      setUsers(users.filter((user) => user.id !== id))
      notification.success('Usuario excluído com sucesso.')
    } catch (error: any) {
      notification.error(error.response?.data || error.message)
    } finally {
      setDestroying(false)
    }
  }

  return (
    <UserContext.Provider
      value={{
        users,
        admins,
        students,
        teachers,
        isFetching,
        isCreating,
        isUpdating,
        isDestroying,
        isSaving,
        isLoading,
        fetch,
        create,
        update,
        updatePassword,
        destroy,
      }}
    >
      {children}
    </UserContext.Provider>
  )
}
