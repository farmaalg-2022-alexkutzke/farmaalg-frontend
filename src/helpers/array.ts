/* eslint-disable @typescript-eslint/ban-types */

export function uniqueValues(array: number[]): number[]
export function uniqueValues(array: string[]): string[]
export function uniqueValues<T extends Object>(array: T[], keyExtractor: (item: T) => any): T[]
export function uniqueValues(array: any[], keyExtractor?: any): any[] {
  const hashTable: any = {}

  array.forEach((item) => {
    const key = keyExtractor ? keyExtractor(item) : item
    hashTable[key] = item
  })

  return Object.values(hashTable)
}
