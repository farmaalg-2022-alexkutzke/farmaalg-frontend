import { Perfil, Usuario } from '~/models'

export function isAdmin(user?: Perfil | Usuario) {
  return [user, (user as Usuario)?.perfil].includes(Perfil.ADMIN)
}

export function isStudent(user?: Perfil | Usuario) {
  return [user, (user as Usuario)?.perfil].includes(Perfil.ALUNO)
}

export function isTeacher(user?: Perfil | Usuario) {
  return [user, (user as Usuario)?.perfil].includes(Perfil.PROFESSOR)
}
