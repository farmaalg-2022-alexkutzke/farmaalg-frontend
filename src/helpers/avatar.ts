
/**
 * Gets an URL to a random avatar from DICEBEAR API (identicon category)
 */
export function generateAvatar(exactSeed?: number | string) {
  const actualSeed = exactSeed || Math.random().toString().slice(2)

  return `https://avatars.dicebear.com/api/identicon/farmaalg-${actualSeed}.svg?b=%23ffffff`
}
