import { AlertColor } from '@mui/material/Alert'
import { OptionsObject, useSnackbar } from 'notistack'

export function useAlert() {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar()

  function alertFactory(color: AlertColor) {
    return (newMessage: string, options?: OptionsObject) => {
      const alertKey = enqueueSnackbar(newMessage, {
        anchorOrigin: {
          horizontal: 'right',
          vertical: 'bottom',
        },
        preventDuplicate: true,
        variant: color,
        ...options,
      })

      return () => closeSnackbar(alertKey)
    }
  }

  return {
    error: alertFactory('error'),
    info: alertFactory('info'),
    success: alertFactory('success'),
    warning: alertFactory('warning'),
  }
}
