import { useContext } from 'react'
import { DrawerContext } from '~/contexts'

export function useDrawer() {
  return useContext(DrawerContext)
}
