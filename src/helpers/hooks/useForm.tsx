import { yupResolver } from '@hookform/resolvers/yup'
import { InputHTMLAttributes, useCallback } from 'react'
import { DeepPartial, Path, UnpackNestedValue, useForm as useRHForm } from 'react-hook-form'
import { AnyObjectSchema } from 'yup'

namespace useForm {
  export interface UseFormOptions<T> {
    defaultValues?: UnpackNestedValue<DeepPartial<T>>
    valSchema?: AnyObjectSchema
  }

  export interface FormProps extends InputHTMLAttributes<HTMLFormElement> {
    onSubmit: (data: any) => void
  }
}

export function useForm<T>({
  defaultValues,
  valSchema,
}: useForm.UseFormOptions<T> = {}) {
  const formUtils = useRHForm<T>({
    resolver: valSchema && yupResolver(valSchema),
    defaultValues,
  })

  const Form = useCallback(({ onSubmit, ...props }: useForm.FormProps) => (
    <form
      noValidate
      onSubmit={formUtils.handleSubmit(onSubmit)}
      {...props}
    />
  ), [formUtils.handleSubmit]) // eslint-disable-line react-hooks/exhaustive-deps

  return {
    Form,
    getController: (fieldName: Path<T>, defaultValue: any = '') => ({
      control: formUtils.control,
      name: fieldName,
      defaultValue,
      getValue: () => formUtils.getValues(fieldName),
      setValue: (value: any) => formUtils.setValue(fieldName, value),
    }),
    ...formUtils,
  }
}
