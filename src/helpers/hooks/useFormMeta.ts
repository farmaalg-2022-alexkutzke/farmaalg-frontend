import { useContext } from 'react'
import { FormMetaContext } from '~/contexts'

export function useFormMeta() {
  return useContext(FormMetaContext)
}
