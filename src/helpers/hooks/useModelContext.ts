import { useContext } from 'react'

import {
  AnswerContext,
  ClassroomContext,
  ExerciseContext,
  InstitutionContext,
  LanguageContext,
  TestCaseContext,
  UserContext,
} from '~/contexts'

export function useAnswerContext() {
  return useContext(AnswerContext)
}
export function useClassroomContext() {
  return useContext(ClassroomContext)
}

export function useExerciseContext() {
  return useContext(ExerciseContext)
}

export function useInstitutionContext() {
  return useContext(InstitutionContext)
}

export function useLanguageContext() {
  return useContext(LanguageContext)
}

export function useTestCaseContext() {
  return useContext(TestCaseContext)
}

export function useUserContext() {
  return useContext(UserContext)
}
