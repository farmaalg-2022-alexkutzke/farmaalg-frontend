
// Format of model in a form (sent with the request)
export interface DTO {
  id?: number
}

// Reference interface for the model (usually coming from the response)
export interface Model {
  readonly id: number
}
