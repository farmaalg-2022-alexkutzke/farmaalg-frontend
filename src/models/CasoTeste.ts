import { DTO, Model } from './@Model'
import { Exercicio } from './Exercicio'

export interface CasoTesteDTO extends DTO {
  exercicio: Model
  output: string
  titulo: string
  input?: string
}

export interface CasoTeste extends Model {
  exercicio: Exercicio
  input: string | null
  output: string
  titulo: string
}

export interface ExecucaoCasoTeste extends Model {
  id: number
  estahCorreto: boolean
  input: null
  output: string
  outputObtido: string
  titulo: string
}
