import { DTO, Model } from './@Model'
import { Linguagem } from './Linguagem'
import { Turma } from './Turma'

export interface DicaExercicioDTO extends DTO {
  titulo: string
  descricao: string
  numeroTentativas: number
  exercicio?: Model
}

export interface TagExercicioDTO extends DTO {
  nome: string
}

export interface ExercicioDTO extends DTO {
  turma: Model
  titulo: string
  descricao: string
  enunciado: string
  exercicioOriginal?: Model
  ehPublico: boolean
  tags: TagExercicioDTO[]
  linguagens: Model[]
  dicas: DicaExercicioDTO[]
}

export interface DicaExercicio extends Model {
  titulo: string
  descricao: string
  numeroTentativas: number
}

export interface TagExercicio extends Model {
  nome: string
  exercicios?: Exercicio[]
}

export interface Exercicio extends Model {
  turma: Turma
  titulo: string
  descricao: string
  enunciado: string
  exercicioOriginal?: Exercicio
  ehPublico: boolean
  tags: TagExercicio[]
  linguagens: Linguagem[]
  dicas: DicaExercicio[]
}
