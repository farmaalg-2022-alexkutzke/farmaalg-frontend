import { DTO, Model } from './@Model'

export interface InstituicaoContatoDTO extends DTO {
  contato?: string
  tipo?: string
  pessoa?: string
  instituicao: Model
}

export interface InstituicaoDTO extends DTO {
  nome: string
  cnpj: string
  cep: string
  endereco: string
  numero: string
  bairro: string
  cidade: string
  estado: string
  contatos?: Omit<InstituicaoContatoDTO, 'instituicao'>[]
}

export interface InstituicaoContato extends Model {
  contato?: string
  tipo?: string
  pessoa?: string
}

export interface Instituicao extends Model {
  nome: string
  cnpj: string
  cep: string
  endereco: string
  numero: string
  bairro: string
  cidade: string
  estado: string
  contatos: InstituicaoContato[]
}
