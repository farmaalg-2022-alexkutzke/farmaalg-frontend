import { DTO, Model } from './@Model'

export interface LinguagemDTO extends DTO {
  nome: string
  codigoSandbox: string
  exemplo?: string
}

export interface Linguagem extends Model {
  nome: string
  codigoSandbox: string
  exemplo?: string
}
