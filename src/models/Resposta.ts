import { DTO, Model } from './@Model'
import { ExecucaoCasoTeste } from './CasoTeste'
import { Exercicio } from './Exercicio'
import { Aluno } from './Usuario'

export interface RespostaDTO extends DTO {
  aluno: Model
  codigo: string
  ehFinal: boolean
  exercicio: Model
  linguagem: Model
}

export interface Resposta extends Model {
  aluno: Aluno
  casosTeste: ExecucaoCasoTeste[]
  codigo: string
  exercicio: Exercicio
  ehFinal: boolean
  numeroTentativa: number
}
