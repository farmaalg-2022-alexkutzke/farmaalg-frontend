import { DTO, Model } from './@Model'
import { Exercicio } from './Exercicio'
import { Aluno, Professor } from './Usuario'

export enum TurmaStatus { // eslint-disable-line no-shadow
  ATIVA = 'ATIVA',
  ENCERRADA = 'ENCERRADA',
  ARQUIVADA = 'ARQUIVADA',
  PAUSADA = 'PAUSADA'
}

export interface TurmaDTO extends DTO {
  disciplina: string
  nome: string
  descricao?: string
  codigoAcesso: string
  status: TurmaStatus
  professor: Model
}

export interface Turma extends Model {
  disciplina: string
  nome: string
  descricao?: string
  codigoAcesso: string
  status: TurmaStatus
  professor: Professor
  alunos?: Aluno[]
  exercicios?: Exercicio[]
}
