import { DTO, Model } from './@Model'
import { Instituicao } from './Instituicao'

export enum Perfil { // eslint-disable-line no-shadow
  ADMIN = 'ADMIN',
  ALUNO = 'ALUNO',
  PROFESSOR = 'PROFESSOR'
}

export interface UsuarioDTO extends DTO {
  perfil: Perfil
  nome: string
  email: string
  avatar: string
  senha: string
  confirmarSenha?: string
  telefone?: string
  instituicao?: Model
  idInstitucional?: string
}

export interface AdminDTO extends UsuarioDTO {
  perfil: Perfil.ADMIN
  telefone: string
}

export interface AlunoDTO extends UsuarioDTO {
  perfil: Perfil.ALUNO
  instituicao: Model
  idInstitucional: string
}

export interface ProfessorDTO extends UsuarioDTO {
  perfil: Perfil.PROFESSOR
  telefone: string
  instituicao: Model
  idInstitucional: string
}

export type NovaSenhaDTO = {
  senhaNova: string
  senhaAntiga?: string
  confirmarSenha?: string
  id?: number
}

export interface Usuario extends Model {
  perfil: Perfil
  nome: string
  email: string
  avatar: string
  telefone?: string
  instituicao?: Instituicao
  idInstitucional?: string
}

export interface Admin extends Usuario {
  perfil: Perfil.ADMIN
  telefone: string
}

export interface Aluno extends Usuario {
  perfil: Perfil.ALUNO
  instituicao: Instituicao
  idInstitucional: string
}

export interface Professor extends Usuario {
  perfil: Perfil.PROFESSOR
  telefone: string
  instituicao: Instituicao
  idInstitucional: string
}
