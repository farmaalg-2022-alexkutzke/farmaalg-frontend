import { lazy, Suspense } from 'react'
import { Navigate, Routes, Route } from 'react-router-dom'

import LoadingOverlay from '@/common/LoadingOverlay'
import BasicLayout from '@/layouts/BasicLayout'
import PanelLayout from '@/layouts/PanelLayout'
import ClassroomsPage from '@/pages/Classrooms'
import ClassroomDetailsPage from '@/pages/Classrooms/ClassroomDetails'
import ExerciseAnswersSubpage from '@/pages/Classrooms/ClassroomDetails/AnswersPanel'
import ClassroomExercisesSubpage from '@/pages/Classrooms/ClassroomDetails/ExercisesList'
import ClassroomStudentsSubpage from '@/pages/Classrooms/ClassroomDetails/StudentsList'
import DashboardPage from '@/pages/Dashboard'
import EditorPage from '@/pages/Editor'
import ExercisesPage from '@/pages/Exercises'
import { useAuth } from '~/helpers/hooks'

const InstitutionsPage = lazy(() => import('@/pages/Institutions'))
const LanguagesPage = lazy(() => import('@/pages/Languages'))
const UsersPage = lazy(() => import('@/pages/Users'))

export const AUTH_HOME_PATH = import.meta.env.VITE_FARMAALG_AUTH_HOME_PATH
export const PUBLIC_HOME_PATH = import.meta.env.VITE_FARMAALG_PUBLIC_HOME_PATH

function AuthROutes() {
  const { user } = useAuth()

  return (
    <Routes>
      <Route element={<BasicLayout backButton />}>
        <Route path="/editor" element={<EditorPage />} />
      </Route>
      <Route element={<PanelLayout />}>
        <Route path="/" element={<DashboardPage />} />
        {(user?.isAdmin() || user?.isTeacher()) && (
          <Route path="/exercicios" element={<ExercisesPage />} />
        )}
        <Route path="/turmas">
          <Route index element={<ClassroomsPage />} />
          <Route path=":classroomId" element={<ClassroomDetailsPage />}>
            <Route index element={<Navigate replace to="exercicios" />} />
            <Route path="exercicios" element={<ClassroomExercisesSubpage />} />
            {user?.isStudent() && (
              <Route path="exercicios/:exerciseId/respostas" element={<ExerciseAnswersSubpage />} />
            )}
            {(user?.isAdmin() || user?.isTeacher()) && <>
              <Route path="exercicios/:exerciseId/alunos" element={<ClassroomStudentsSubpage />} />
              <Route path="exercicios/:exerciseId/alunos/:studentId/respostas" element={<ExerciseAnswersSubpage />} />
              <Route path="alunos" element={<ClassroomStudentsSubpage />} />
              <Route path="alunos/:studentId/exercicios" element={<ClassroomExercisesSubpage />} />
              <Route path="alunos/:studentId/exercicios/:exerciseId/respostas" element={<ExerciseAnswersSubpage />} />
            </>}
          </Route>
        </Route>
        {user?.isAdmin() && (
          <Route
            path="/instituicoes"
            element={
              <Suspense fallback={<LoadingOverlay />}>
                <InstitutionsPage />
              </Suspense>
            }
          />
        )}
        {user?.isAdmin() && (
          <Route
            path="/linguagens"
            element={
              <Suspense fallback={<LoadingOverlay />}>
                <LanguagesPage />
              </Suspense>
            }
          />
        )}
        {user?.isAdmin() && (
          <Route
            path="/usuarios"
            element={
              <Suspense fallback={<LoadingOverlay />}>
                <UsersPage />
              </Suspense>
            }
          />
        )}
        <Route path="*" element={<Navigate replace to="/" />} />
      </Route>
    </Routes>
  )
}

export default AuthROutes
