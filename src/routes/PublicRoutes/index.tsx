import { Navigate, Routes, Route } from 'react-router-dom'

import BasicLayout from '@/layouts/BasicLayout'
import LandingPage from '@/pages/Landing'
import SignInPage from '@/pages/SignIn'
import SignUpPage from '@/pages/SignUp'

export const PUBLIC_HOME_PATH = import.meta.env.VITE_FARMAALG_PUBLIC_HOME_PATH

function PublicRoutes() {
  return (
    <Routes>
      <Route element={<BasicLayout contained />}>
        <Route path="/" element={<LandingPage />} />
        <Route path="/entrar" element={<SignInPage />} />
        <Route path="/registrar" element={<SignUpPage />} />
        <Route path="*" element={<Navigate replace to="/" />} />
      </Route>
    </Routes>
  )
}

export default PublicRoutes
