import { useAuth } from '~/helpers/hooks'

import AuthRoutes from './AuthRoutes'
import PublicRoutes from './PublicRoutes'

export { AUTH_HOME_PATH } from './AuthRoutes'
export { PUBLIC_HOME_PATH } from './PublicRoutes'

function Router() {
  const { isAuthenticated } = useAuth()

  return isAuthenticated
    ? <AuthRoutes />
    : <PublicRoutes />
}

export default Router
