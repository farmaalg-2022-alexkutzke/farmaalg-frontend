import { DTO, Model } from '~/models'

import httpClient from '../httpClient'
import { HttpConfig, HttpTransformers, UrlParams } from '../types'

class FarmaAlgApiService<T extends Model, D extends DTO> {
  private $httpConfig: HttpConfig<D>
  private $url: string

  constructor(uri: string, transformers?: HttpTransformers) {
    this.$url = uri
    this.$httpConfig = {
      transformRequest: transformers?.request && [
        transformers?.request,
        ...httpClient.defaults.transformRequest as any,
      ],
      transformResponse: transformers?.response && [
        ...httpClient.defaults.transformResponse as any,
        transformers?.response,
      ],
    }
  }

  async request<R = any>({ url, ...config }: HttpConfig<D>): Promise<R> {
    const response = await httpClient.request<R>({
      url: `${this.$url}/${url}`,
      ...this.$httpConfig,
      ...config,
    })

    return response.data
  }

  async get(params?: UrlParams): Promise<T[]> {
    const response = await httpClient.get<T[]>(this.$url, {
      ...this.$httpConfig,
      params,
    })

    return response.data
  }

  async find(id: number): Promise<T | null> {
    const response = await httpClient.get<T>(`${this.$url}/${id}`, {
      ...this.$httpConfig,
    })

    return response.data ?? null
  }

  save(data: D): Promise<T> {
    if (data.id) {
      return this.update(data.id, data)
    }

    return this.create(data)
  }

  async create(data: D): Promise<T> {
    const response = await httpClient.post<T>(this.$url, data, {
      ...this.$httpConfig,
    })

    return response.data
  }

  async update(id: number, data: D): Promise<T> {
    const response = await httpClient.put<T>(`${this.$url}/${id}`, data, {
      ...this.$httpConfig,
    })

    return response.data
  }

  async destroy(id: number): Promise<T | null> {
    const response = await httpClient.delete<T>(`${this.$url}/${id}`, {
      ...this.$httpConfig,
    })

    return response.data ?? null
  }
}

export default FarmaAlgApiService
