import { Resposta, RespostaDTO } from '~/models'

import ApiService from './@ApiService'

class AnswerService extends ApiService<Resposta, RespostaDTO> {
  constructor() {
    super('/resposta')
  }

  test(data: Omit<RespostaDTO, 'ehFinal'>) {
    return this.create({ ...data, ehFinal: false })
  }

  submit(data: Omit<RespostaDTO, 'ehFinal'>) {
    return this.create({ ...data, ehFinal: true })
  }

  findByExerciseAndStudent(exerciseId: number, studentId: number) {
    return this.request({
      method: 'GET',
      url: `aluno/${studentId}/exercicio/${exerciseId}`,
    })
  }
}

export default AnswerService
