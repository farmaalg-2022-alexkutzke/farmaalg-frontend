import jwtDecode from 'jwt-decode'

import { Usuario } from '~/models'

import httpClient from '../httpClient'

class FarmaAlgAuthService {
  private readonly $_STORAGE_KEY = import.meta.env.VITE_FARMAALG_AUTH_STORAGE_KEY
  private readonly $_SIGNIN_URL = '/login'
  private readonly $_USERDATA_URL = '/usuario/mim'

  /**
   * Check if token is saved in browser storage and if it is not expired.
   */
  get isAuthenticated() {
    const localToken = localStorage.getItem(this.$_STORAGE_KEY)
    const sessionToken = sessionStorage.getItem(this.$_STORAGE_KEY)
    const token = localToken || sessionToken

    if (token) {
      const payload = jwtDecode<{ exp: number }>(token)

      if (payload.exp * 1000 > Date.now()) {
        return true
      }
      this.unlink()
    }

    return false
  }

  /**
   * Submit the credentials to the endpoint and, if success, persist
   * the token to the browser storage.
   */
  async authenticate(email: string, password: string, rememberMe = false) {
    const requestPayload = { email, senha: password }
    const response = await httpClient.post<{ token: string }>(
      this.$_SIGNIN_URL,
      requestPayload,
    )

    const storageType = rememberMe ? localStorage : sessionStorage
    storageType.setItem(this.$_STORAGE_KEY, response.data.token)
  }

  /**
   * Remove the token from the browser storage.
   */
  async unlink() { // eslint-disable-line require-await
    localStorage.removeItem(this.$_STORAGE_KEY)
    sessionStorage.removeItem(this.$_STORAGE_KEY)
  }

  async getUserData() {
    const response = await httpClient.get<Usuario>(this.$_USERDATA_URL)

    return response.data
  }
}

export default FarmaAlgAuthService
