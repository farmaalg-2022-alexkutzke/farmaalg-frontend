import { Turma, TurmaDTO } from '~/models'

import ApiService from './@ApiService'

class ClassroomService extends ApiService<Turma, TurmaDTO> {
  constructor() {
    super('/turma')
  }

  // As relationships don't include the reference to the classroom,
  // we need to override the method and include it manually.
  async find(id: number) {
    const classroom = await super.find(id)

    if (classroom) {
      classroom.exercicios = classroom.exercicios?.map((exercise) => ({
        ...exercise,
        turma: classroom,
      }))
    }

    return classroom
  }

  async checkAccessCode(accessCode: string) {
    try {
      return !(accessCode && await this.request<string>({
        method: 'GET',
        url: `codigo/acesso/${accessCode}`,
      }))
    } catch (error: any) {
      return false
    }
  }

  registerWithAccessCode(userId: number, accessCode: string) {
    return this.request({
      method: 'POST',
      url: `aluno/${userId}`,
      data: { codigoAcesso: accessCode } as any,
    })
  }
}

export default ClassroomService
