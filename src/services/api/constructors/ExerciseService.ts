import { CasoTesteDTO, Exercicio, ExercicioDTO, RespostaDTO } from '~/models'

import ApiService from './@ApiService'
import AnswerService from './AnswerService'
import TestCaseService from './TestCaseService'

class ExerciseService extends ApiService<Exercicio, ExercicioDTO> {
  answerService = new AnswerService()
  testCaseService = new TestCaseService()

  constructor() {
    super('/exercicio')
  }

  createFull(exercise: ExercicioDTO) {
    return this.request({
      method: 'POST',
      url: 'completo',
      data: exercise,
    })
  }

  replicate(exerciseId: number, classroomId: number) {
    return this.request<Exercicio>({
      method: 'POST',
      url: `${exerciseId}/turma/${classroomId}`,
    })
  }

  getTestCases(exerciseId: number) {
    return this.testCaseService.getByExercise(exerciseId)
  }

  createTestCase(exerciseId: number, testCase: CasoTesteDTO) {
    return this.testCaseService.create({
      ...testCase,
      exercicio: {
        id: exerciseId,
      },
    })
  }

  updateTestCase(testCaseId: number, testCase: CasoTesteDTO) {
    return this.testCaseService.update(testCaseId, testCase)
  }

  destroyTestCase(testCaseId: number) {
    return this.testCaseService.destroy(testCaseId)
  }

  runTestCase(testCaseId: number, languageId: number, code: string) {
    return this.testCaseService.run(testCaseId, languageId, code)
  }

  testAnswer(data: Omit<RespostaDTO, 'ehFinal'>) {
    return this.answerService.test(data)
  }

  submitAnswer(data: Omit<RespostaDTO, 'ehFinal'>) {
    return this.answerService.submit(data)
  }

  findAnswersByExerciseAndStudent(exerciseId: number, studentId: number) {
    return this.answerService.findByExerciseAndStudent(exerciseId, studentId)
  }
}

export default ExerciseService
