import { TagExercicio, TagExercicioDTO } from '~/models'

import ApiService from './@ApiService'

class ExerciseTagService extends ApiService<TagExercicio, TagExercicioDTO> {
  constructor() {
    super('/tag')
  }
}

export default ExerciseTagService
