import { InstituicaoContato, InstituicaoContatoDTO } from '~/models'
import ApiService from './@ApiService'

class InstitutionContactService extends ApiService<InstituicaoContato, InstituicaoContatoDTO> {
  constructor() {
    super('/instituicao/contato')
  }
}

export default InstitutionContactService
