import { Instituicao, InstituicaoDTO, InstituicaoContatoDTO } from '~/models'

import { InstitutionsComboBox } from '../types'
import ApiService from './@ApiService'
import InstitutionContactService from './InstitutionContactService'

class InstitutionService extends ApiService<Instituicao, InstituicaoDTO> {
  institutionContactService = new InstitutionContactService()

  constructor() {
    super('/instituicao')
  }

  getComboBox() {
    return this.request<InstitutionsComboBox>({
      method: 'GET',
      url: 'combo',
    })
  }

  createContact(institutionId: number, contact: InstituicaoContatoDTO) {
    return this.institutionContactService.create({
      ...contact,
      instituicao: { id: institutionId },
    })
  }

  updateContact(contactId: number, contact: InstituicaoContatoDTO) {
    return this.institutionContactService.update(contactId, contact)
  }

  destroyContact(contactId: number) {
    return this.institutionContactService.destroy(contactId)
  }
}

export default InstitutionService
