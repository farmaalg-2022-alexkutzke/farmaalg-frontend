import { Linguagem, LinguagemDTO } from '~/models'
import ApiService from './@ApiService'

class LanguageService extends ApiService<Linguagem, LinguagemDTO> {
  constructor() {
    super('/linguagem')
  }
}

export default LanguageService
