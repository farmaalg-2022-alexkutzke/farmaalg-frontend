import { CasoTeste, CasoTesteDTO, ExecucaoCasoTeste } from '~/models'

import ApiService from './@ApiService'

class TestCaseService extends ApiService<CasoTeste, CasoTesteDTO> {
  constructor() {
    super('/caso/teste')
  }

  getByExercise(exerciseId: number) {
    return this.request<CasoTeste[]>({
      method: 'GET',
      url: `exercicio/${exerciseId}`,
    })
  }

  run(testCaseId: number, languageId: number, code: string) {
    return this.request<ExecucaoCasoTeste>({
      method: 'POST',
      url: `${testCaseId}`,
      data: {
        codigo: code,
        linguagem: {
          id: languageId,
        },
      } as any,
    })
  }
}

export default TestCaseService
