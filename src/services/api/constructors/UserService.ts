import { Usuario, UsuarioDTO } from '~/models'

import ApiService from './@ApiService'

class UserService extends ApiService<Usuario, UsuarioDTO> {
  constructor() {
    super('/usuario')
  }

  async updatePassword(oldPassword: string, newPassword: string) {
    await this.request({
      method: 'PUT',
      url: 'senha',
      data: {
        senhaAntiga: oldPassword,
        senhaNova: newPassword,
      } as any,
    })
  }

  async updatePasswordAsAdmin(userId: number, newPassword: string) {
    await this.request({
      method: 'PUT',
      url: 'admin/senha',
      data: {
        id: userId,
        senhaNova: newPassword,
      } as any,
    })
  }
}

export default UserService
