import axios from 'axios'

function createHttpClient() {
  const authHeader = import.meta.env.VITE_FARMAALG_AUTH_HEADER
  const storageKey = import.meta.env.VITE_FARMAALG_AUTH_STORAGE_KEY
  const authToken = localStorage.getItem(storageKey) ?? sessionStorage.getItem(storageKey)

  return axios.create({
    baseURL: import.meta.env.VITE_FARMAALG_SERVER,
    headers: {
      'Content-Type': 'application/json',
      [authHeader]: (authToken as string) && `Bearer ${authToken}`,
    },
  })
}

export default createHttpClient()
