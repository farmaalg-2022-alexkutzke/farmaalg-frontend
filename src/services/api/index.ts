import AuthService from './constructors/AuthService'
import ClassroomService from './constructors/ClassroomService'
import ExerciseService from './constructors/ExerciseService'
import ExerciseTagService from './constructors/ExerciseTagService'
import InstitutionService from './constructors/InstitutionService'
import LanguageService from './constructors/LanguageService'
import UserService from './constructors/UserService'

export * from './types'

export const authService = new AuthService()
export const classroomService = new ClassroomService()
export const exerciseService = new ExerciseService()
export const institutionService = new InstitutionService()
export const tagService = new ExerciseTagService()
export const languageService = new LanguageService()
export const userService = new UserService()
