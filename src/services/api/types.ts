import { AxiosRequestConfig, AxiosRequestTransformer, AxiosResponseTransformer } from 'axios'

export type UrlParams = Record<string, string | number>
export type HttpConfig<T> = AxiosRequestConfig<T>
export type RequestTransformer = AxiosRequestTransformer
export type ResponseTransformer = AxiosResponseTransformer

export interface HttpTransformers {
  request?: RequestTransformer
  response?: ResponseTransformer
}

export type InstitutionsComboBox = Array<{
  id: number
  nome: string
}>
